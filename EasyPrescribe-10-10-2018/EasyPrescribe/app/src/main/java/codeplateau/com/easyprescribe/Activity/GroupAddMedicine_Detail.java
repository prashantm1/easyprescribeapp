package codeplateau.com.easyprescribe.Activity;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import codeplateau.com.easyprescribe.R;

public class GroupAddMedicine_Detail extends AppCompatActivity {
    private RecyclerView rc_challenges_list;
    private ProgressDialog pDialog;
    private LinearLayout ll_no_challenges_list;
    private static LinearLayoutManager mLayoutManager;

    @Override
    protected void onCreate (Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_add_medicine__detail);
        initViews();
        setListener();
    }
    private void initViews() {


        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

        // Handle Toolbar
        android.support.v7.widget.Toolbar toolbar = (android.support.v7.widget.Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //set the back arrow in the toolbar
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.title_group_tasklist);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        rc_challenges_list = (RecyclerView) findViewById(R.id.rc_challenges_list);
        ll_no_challenges_list = (LinearLayout) findViewById(R.id.ll_no_challenges_list);

    }
    private void setListener(){

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_add_group_task_detail, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        switch (item.getItemId()) {
            case R.id.item_add_group_task_detail:
                Toast.makeText(GroupAddMedicine_Detail.this, item.getItemId(), Toast.LENGTH_SHORT).show();

               // Intent intent_AddTask= new Intent(getApplicationContext(), Add_Medicine.class);
             /*   intent_AddTask.putExtra("challenge_id", challenge_id);
                intent_AddTask.putExtra("group_id", group_id);
                intent_AddTask.putExtra("group_name", group_name);
                intent_AddTask.putExtra("group_icon", group_icon);*/
               // startActivity(intent_AddTask);
        }

        return super.onOptionsItemSelected(item);
    }


    public class ViewDialogBackButton {

        public void showDialog(Activity activity) {
            final Dialog dialog = new Dialog(activity);
            dialog.setCanceledOnTouchOutside(true);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(false);
            dialog.setContentView(R.layout.dialogbox_medicine_list_success);

            Button dialogButton = (Button) dialog.findViewById(R.id.btnOk1);
            dialogButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

            ImageView img_close = (ImageView) dialog.findViewById(R.id.img_close);
            img_close.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

            dialog.show();
        }
    }

    @Override
    public void onBackPressed() {

        ViewDialogBackButton alert = new ViewDialogBackButton();
        alert.showDialog(GroupAddMedicine_Detail.this);
    }
}
