package codeplateau.com.easyprescribe.Fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import codeplateau.com.easyprescribe.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class patient_listeningFragment extends Fragment {


    public patient_listeningFragment () {
        // Required empty public constructor
    }


    @Override
    public View onCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_patient_listening, container, false);
    }

}
