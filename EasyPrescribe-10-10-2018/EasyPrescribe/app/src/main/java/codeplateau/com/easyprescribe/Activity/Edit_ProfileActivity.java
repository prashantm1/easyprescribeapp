package codeplateau.com.easyprescribe.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import net.gotev.uploadservice.MultipartUploadRequest;
import net.gotev.uploadservice.UploadNotificationConfig;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import codeplateau.com.easyprescribe.R;
import codeplateau.com.easyprescribe.Services.RestInterface;
import de.hdodenhof.circleimageview.CircleImageView;


public class Edit_ProfileActivity extends AppCompatActivity {

    private TextInputLayout input_layout_firstname, input_layout_lastname,input_layout_email,input_layout_address,input_layout_doctortype;
    private EditText  txt_mobileno,txt_firstname,txt_lastname,txt_emaild,txt_address,txt_doctortype,txt_phone;
    private ImageView img_groupicon;
    private TextView txt_gender;
    private CircleImageView img_profileicon;
    private static final String TAG = "Write_Prescription";
    private ProgressDialog pDialog;
    //SharedPreferences sharedPreferences;

    SharedPreferences sharedpreferences;
    private Button btn_updateprofile;
    public static final String MyPREFERENCES = "myprefs";
    private static int PICK_IMAGE_REQUEST = 1;
    Uri imageuri;
    String imageiconselect = "0";
    private static String name,phone;
    public static String id,first_name,last_name,gender,email,address,doctor_type,user_id,picture = "null",mobile;
    @Override
    protected void onCreate (Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.editprofileactivity);

        initViews();
        setListeners();
    }
    private void initViews(){

        //sharedPreferences = getApplicationContext().getSharedPreferences(RestInterface.pref, Context.MODE_PRIVATE);
        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        name = sharedpreferences.getString("name", "");
        email = sharedpreferences.getString("email", "");
        phone = sharedpreferences.getString("phone", "");


        input_layout_firstname = (TextInputLayout) findViewById(R.id.input_layout_firstname);
        input_layout_lastname = (TextInputLayout) findViewById(R.id.input_layout_lastname);
        input_layout_email = (TextInputLayout) findViewById(R.id.input_layout_email);
        input_layout_address = (TextInputLayout) findViewById(R.id.input_layout_address);
        input_layout_doctortype = (TextInputLayout) findViewById(R.id.input_layout_doctortype);
        input_layout_firstname = (TextInputLayout) findViewById(R.id.input_layout_firstname);
        txt_firstname= (EditText ) findViewById(R.id.txt_firstname);
        txt_lastname= (EditText ) findViewById(R.id.txt_lastname);
        txt_emaild= (EditText ) findViewById(R.id.txt_emaild);
        txt_address= (EditText ) findViewById(R.id.txt_address);
        txt_doctortype= (EditText ) findViewById(R.id.txt_doctortype);
        img_groupicon= (ImageView ) findViewById(R.id.img_groupicon);
        img_profileicon= (CircleImageView ) findViewById(R.id.img_profileicon);
        btn_updateprofile= (Button ) findViewById(R.id.btn_updateprofile);
        txt_gender= (TextView ) findViewById(R.id.txt_gender);
        txt_mobileno= (EditText )findViewById(R.id.txt_mobileno);


        txt_firstname.setText(name);
        txt_emaild.setText(email);
        txt_mobileno.setText(mobile);
    }

    private void setListeners(){

        Update_Profile_API();

        btn_updateprofile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick (View v) {

                if (txt_firstname.getText().toString().equals("")) {
                    Toast.makeText(getApplicationContext(), "Enter Firstname", Toast.LENGTH_LONG).show();
                } else if (txt_lastname.getText().toString().equals("")) {
                    Toast.makeText(getApplicationContext(), "Enter lastname", Toast.LENGTH_LONG).show();
                }
                else if (txt_emaild.getText().toString().equals("")) {
                    Toast.makeText(getApplicationContext(), "Enter Email", Toast.LENGTH_LONG).show();
                }
                else if (txt_address.getText().toString().equals("")) {
                    Toast.makeText(getApplicationContext(), "Enter Address", Toast.LENGTH_LONG).show();
                }
                else if (txt_doctortype.getText().toString().equals("")) {
                    Toast.makeText(getApplicationContext(), "Enter Docrtype", Toast.LENGTH_LONG).show();
                }
                else {

                    getUpdateAPI();

                }
            }

        });

        img_profileicon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
            }
        });

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            try {

                imageuri = data.getData();
                final InputStream imageStream = getApplicationContext().getContentResolver().openInputStream(imageuri);
                final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                img_profileicon.setImageBitmap(selectedImage);
                imageiconselect = "1";
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                Toast.makeText(getApplicationContext(), "Something went wrong", Toast.LENGTH_LONG).show();
            }

        } else {
            Toast.makeText(getApplicationContext(), "You haven't picked Image", Toast.LENGTH_LONG).show();
        }
    }

    private void Update_Profile_API () {
        // Tag used to cancel the request
        showpDialog();
        StringRequest strReq = new StringRequest(Request.Method.POST,
                RestInterface.URL_edit_Profile_API, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Login Response: " + response);
                //hideProgressDialog();
                hidepDialog();
                try {
                    JSONObject jObj = new JSONObject(response);
                    String success = jObj.getString("success");

                    if (success.equals("1")) {

                        JSONArray requestarray = jObj.getJSONArray("data");

                        for (int i = 0; i < requestarray.length(); i++) {

                            JSONObject req = requestarray.getJSONObject(i);


                            id = req.optString("id");
                            first_name= req.optString("first_name");
                            last_name = req.optString("last_name");
                            gender = req.optString("gender");
                            email = req.optString("email");
                            address = req.optString("address");
                            doctor_type = req.optString("doctor_type");
                            user_id = req.optString("user_id");

                        }

                        if (imageiconselect.equals("1")) {
                            uploadMultipart();
                        }


                    } else {
                        hidepDialog();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Error: " + error.getMessage());
                //Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
                hidepDialog();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting params to register url
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id",user_id );
                return params;
            }
        };

        strReq.setRetryPolicy(new DefaultRetryPolicy(
                5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // Adding request to request queue
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        strReq.setShouldCache(false);
        requestQueue.add(strReq);
    }


    private void getUpdateAPI() {

        showpDialog();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                RestInterface.URL_UpdateProfile, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Login Response: " + response);
                //hideProgressDialog();
                hidepDialog();
                try {
                    JSONObject jObj = new JSONObject(response);
                    String success = jObj.getString("success");

                    if (success.equals("1")) {

                        String data = jObj.getString("data");
                        Toast.makeText(getApplicationContext(), data, Toast.LENGTH_SHORT).show();

                        Intent intent_MedicineList = new Intent(getApplicationContext(), DrawerLayoutActivity.class);
                        startActivity(intent_MedicineList);

                    } else {

                        String message = jObj.getString("data");
                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();

                        hidepDialog();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Error: " + error.getMessage());
                //Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
                hidepDialog();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting params to register url
                Map<String, String> params = new HashMap<String, String>();
                params.put("first_name", txt_firstname.getText().toString().trim());
                params.put("last_name", txt_lastname.getText().toString().trim());
                params.put("gender", txt_gender.getText().toString().trim());
                params.put("address", txt_address.getText().toString().trim());
                params.put("doctor_type", txt_doctortype.getText().toString().trim());
                params.put("id", "2");

                return params;
            }
        };

        strReq.setRetryPolicy(new DefaultRetryPolicy(
                5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // Adding request to request queue
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        strReq.setShouldCache(false);
        requestQueue.add(strReq);
    }

    public String getFileName(Uri uri) {
        String result = null;
        if (uri.getScheme().equals("content")) {
            Cursor cursor = getApplicationContext().getContentResolver().query(uri, null, null, null, null);
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                }
            } finally {
                cursor.close();
            }
        }
        if (result == null) {
            result = uri.getPath();
            int cut = result.lastIndexOf('/');
            if (cut != -1) {
                result = result.substring(cut + 1);
            }
        }
        return result;
    }
    public void uploadMultipart() {

        //getting the actual path of the image
        String path = getPath(imageuri);

        //Uploading code
        try {
            String uploadId = UUID.randomUUID().toString();

            //Creating a multi part request
            new MultipartUploadRequest(getApplicationContext(), uploadId, RestInterface.URL_IMAGEICONUPLOAD)
                    .addFileToUpload(path, "image") //Adding file
                    .setNotificationConfig(new UploadNotificationConfig())
                    .setMaxRetries(2)
                    .startUpload(); //Starting the upload

            Toast.makeText(this, "Group Updated Successfully", Toast.LENGTH_SHORT).show();

           /* Intent intent_GroupTaskList = new Intent(getApplicationContext(), GroupChallengesList.class);
            intent_GroupTaskList.putExtra("group_id", group_id);
            intent_GroupTaskList.putExtra("group_name", group_name);
            intent_GroupTaskList.putExtra("group_icon", group_icon);
            startActivity(intent_GroupTaskList);*/

        } catch (Exception exc) {
            Toast.makeText(getApplicationContext(), exc.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    //method to get the file path from uri
    public String getPath(Uri uri) {
        Cursor cursor = getApplicationContext().getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        String document_id = cursor.getString(0);
        document_id = document_id.substring(document_id.lastIndexOf(":") + 1);
        cursor.close();

        cursor = getApplicationContext().getContentResolver().query(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                null, MediaStore.Images.Media._ID + " = ? ", new String[]{document_id}, null);
        cursor.moveToFirst();
        String path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
        cursor.close();

        return path;
    }

    private void showpDialog() {

        // Progress Dialog
        pDialog = new ProgressDialog(Edit_ProfileActivity.this);
        pDialog.setMessage("Please wait...");
        pDialog.setCancelable(false);

        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hidepDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
}
