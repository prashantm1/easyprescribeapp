package codeplateau.com.easyprescribe.Activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.res.Resources;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import codeplateau.com.easyprescribe.Fragment.Appointment_Fragment;
import codeplateau.com.easyprescribe.R;

public class Add_Old_PatientsDetail extends AppCompatActivity {

    private static EditText txt_patient_mobileno, txt_patientname;
    private static Button btn_submit;

    private static FragmentManager fragment_manager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_oldpatient_detail);

        initViews();
        setListeners();
    }

    private void initViews() {

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

        // Handle Toolbar
        android.support.v7.widget.Toolbar toolbar = (android.support.v7.widget.Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //set the back arrow in the toolbar
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.title_OldPatient);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        txt_patient_mobileno = (EditText) findViewById(R.id.txt_patient_mobileno);
        txt_patientname = (EditText) findViewById(R.id.txt_patientname);

        btn_submit = (Button) findViewById(R.id.btn_submit);
    }

    private void setListeners() {

        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Add_Old_PatientsDetail.ViewDialog_OldPatient alert = new Add_Old_PatientsDetail.ViewDialog_OldPatient();
                alert.showDialog_OldPatient(Add_Old_PatientsDetail.this);
            }
        });
    }


    public class ViewDialog_OldPatient {

        public void showDialog_OldPatient(Activity activity) {
            final Dialog dialog = new Dialog(activity);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(false);
            dialog.setContentView(R.layout.dialog_old_patients_message);
            // dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
            int width = getWindow().getDecorView().getWidth();
            int width_75 = ((75 * width) / 100);

            LinearLayout.LayoutParams forButtonParams = new LinearLayout.LayoutParams(width_75, LinearLayout.LayoutParams.WRAP_CONTENT);
            LinearLayout.LayoutParams forTextViewParams = new LinearLayout.LayoutParams(width_75, LinearLayout.LayoutParams.WRAP_CONTENT);
            Resources r = activity.getResources();


            int px_bottom = (int) TypedValue.applyDimension(
                    TypedValue.COMPLEX_UNIT_DIP,
                    15,
                    r.getDisplayMetrics()
            );

            forButtonParams.setMargins(0, px_bottom, 0, px_bottom);
            forButtonParams.gravity = Gravity.CENTER;

            TextView dialogButton = (TextView) dialog.findViewById(R.id.lbl_OK);



            dialogButton.setLayoutParams(forButtonParams);
            dialogButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    fragment_manager = getSupportFragmentManager();
                    Fragment appointment_fragment = new Appointment_Fragment();
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.frame_container, appointment_fragment, appointment_fragment.getClass().getSimpleName()).addToBackStack(null).commit();
                    dialog.dismiss();
                }
            });

            dialog.show();
        }
    }

    @Override
    public void onBackPressed() {

        Intent intent_CreateChallengesSelect = new Intent(getApplicationContext(), DrawerLayoutActivity.class);
        startActivity(intent_CreateChallengesSelect);
    }
}
