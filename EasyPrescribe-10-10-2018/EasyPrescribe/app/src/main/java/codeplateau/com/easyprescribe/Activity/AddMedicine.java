package codeplateau.com.easyprescribe.Activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import codeplateau.com.easyprescribe.Model.Medicine_NameModel;
import codeplateau.com.easyprescribe.Services.RestInterface;
import codeplateau.com.easyprescribe.R;

public class AddMedicine extends AppCompatActivity {

    private ProgressDialog pDialog;
    private AutoCompleteTextView ed_medicinename;
    private static final String TAG = AddMedicine.class.getSimpleName().toString();
    private static String med_id, dose, unit, direction, instruction, duration;
    private String appointment_id, user_id,patient_id,id;

    ArrayList<Medicine_NameModel> medicine_nameModelArrayList;
    List<String> responseList = new ArrayList<String>();

    SharedPreferences pref;
    private static String TAG_MEDICINE_ID = "id";
    private static String TAG_MEDICINE_NAME = "name";

    private EditText ed_dose, ed_direction;
    private RadioGroup rd_instruction;
    private RadioButton radio_instructionButton;
    private TextView tv_unit, tv_duration;
    private Button btn_submit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_medicine);

        initViews();
        setListeners();
    }

    private void initViews() {

        pref = getApplicationContext().getSharedPreferences(RestInterface.pref, Context.MODE_PRIVATE);
        user_id = pref.getString("user_id", "");

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            appointment_id = extras.getString("appointment_id");
            user_id= extras.getString("user_id");
            patient_id= extras.getString("patient_id");
            med_id= extras.getString("med_id");
            id= extras.getString("id");
        }

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

        // Handle Toolbar
        android.support.v7.widget.Toolbar toolbar = (android.support.v7.widget.Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //set the back arrow in the toolbar
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.title_Add_Medicine);

        ed_medicinename = (AutoCompleteTextView) findViewById(R.id.ed_medicinename);
        ed_dose = (EditText) findViewById(R.id.ed_dose);

        tv_duration = (TextView) findViewById(R.id.tv_duration);
        tv_unit = (TextView) findViewById(R.id.tv_unit);

        ed_direction = (EditText) findViewById(R.id.ed_direction);
        rd_instruction = (RadioGroup) findViewById(R.id.rd_instruction);

        btn_submit = (Button) findViewById(R.id.btn_submit);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    private void setListeners() {

        MedicineName_List_API();

        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (ed_medicinename.getText().toString().equals("")) {
                    ed_medicinename.requestFocus();
                    Dialog_Alert("Medicine Name");
                } else if (ed_dose.getText().toString().equals("")) {
                    ed_dose.requestFocus();
                    Dialog_Alert("Dose");
                } else if (tv_unit.getText().toString().equals("")) {
                    tv_unit.requestFocus();
                    Dialog_Alert("Unit");
                } else if (tv_duration.getText().toString().equals("")) {
                    tv_duration.requestFocus();
                    Dialog_Alert("Duration");
                } else {

                    dose = ed_dose.getText().toString().trim();
                    unit = tv_unit.getText().toString().trim();
                    duration = tv_duration.getText().toString().trim();
                    direction = ed_direction.getText().toString().trim();

                    // get selected radio button from radioGroup
                    int selectedId = rd_instruction.getCheckedRadioButtonId();
                    radio_instructionButton = (RadioButton) findViewById(selectedId);
                    instruction = radio_instructionButton.getText().toString();

                    Add_MedicineDetail_API();
                }
            }
        });

        tv_unit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ViewDialog_Unit alert = new ViewDialog_Unit();
                alert.showDialog1(AddMedicine.this);

            }
        });
        tv_duration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ViewDialog_Duration alert = new ViewDialog_Duration();
                alert.showDialog1(AddMedicine.this);
            }
        });
    }

    private void MedicineName_List_API() {
        // Tag used to cancel the request
        showpDialog();
        StringRequest strReq = new StringRequest(Request.Method.POST,
                RestInterface.URL_MedicineName, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Add Medicine Response: " + response);
                hidepDialog();
                try {
                    JSONObject jObj = new JSONObject(response);
                    String success = jObj.getString("success");

                    if (success.equals("1")) {

                        JSONArray requestarray = jObj.getJSONArray("data");

                        if (requestarray.equals("[]")) {

                            Toast.makeText(AddMedicine.this, "Not Any Medicine Name Found!", Toast.LENGTH_SHORT).show();

                        } else {

                            medicine_nameModelArrayList = new ArrayList<Medicine_NameModel>();
                            medicine_nameModelArrayList.clear();

                            responseList = new ArrayList<String>();

                            for (int i = 0; i < requestarray.length(); i++) {

                                Medicine_NameModel medicine_nameModels = new Medicine_NameModel();
                                JSONObject req = requestarray.getJSONObject(i);

                                medicine_nameModels.setId(req.optString(TAG_MEDICINE_ID));
                                medicine_nameModels.setName(req.optString(TAG_MEDICINE_NAME));

                                medicine_nameModelArrayList.add(medicine_nameModels);
                                responseList.add(req.optString(TAG_MEDICINE_NAME));
                            }

                            ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_dropdown_item_1line, responseList);
                            ed_medicinename.setThreshold(1);
                            ed_medicinename.setAdapter(adapter);

                            ed_medicinename.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                @Override
                                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                                    for (int i = 0; i < medicine_nameModelArrayList.size(); i++) {

                                        String item_name = ed_medicinename.getText().toString();

                                        if (item_name.equals(medicine_nameModelArrayList.get(i).getName())) {

                                            med_id = medicine_nameModelArrayList.get(i).getId();
                                        }
                                    }
                                }
                            });
                        }

                    } else {
                        hidepDialog();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
                hidepDialog();
            }
        }) {

            @Override
            protected java.util.Map<String, String> getParams() {
                // Posting params to register url
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", user_id);
                return params;
            }
        };

        strReq.setRetryPolicy(new DefaultRetryPolicy(
                5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // Adding request to request queue
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        strReq.setShouldCache(false);
        requestQueue.add(strReq);
    }

    // Add Medicine
    private void Add_MedicineDetail_API() {

        showpDialog();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                RestInterface.URL_AddPrescription, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Login Response: " + response);
                hidepDialog();
                try {
                    JSONObject jObj = new JSONObject(response);
                    String success = jObj.getString("success");

                    if (success.equals("1")) {

                        String message = jObj.getString("message");
                        Dialog_Success(message);

                        Intent intent_MedicineList = new Intent(getApplicationContext(), Write_Prescription.class);

                        intent_MedicineList.putExtra("appointment_id", appointment_id);
                        intent_MedicineList.putExtra("patient_id",patient_id);
                        intent_MedicineList.putExtra("user_id",user_id);
                        intent_MedicineList.putExtra("med_id",med_id);
                        intent_MedicineList.putExtra("id",id);
                        startActivity(intent_MedicineList);

                    } else {

                        String message = jObj.getString("message");
                        Dialog_Failed(message);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Error: " + error.getMessage());
                hidepDialog();
            }
        }) {

            @Override
            protected java.util.Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("med_id", med_id);
                params.put("dose", dose);
                params.put("unit", unit);
                params.put("direction", instruction);
                params.put("duration", duration);
                params.put("user_id",user_id);
                params.put("appointment_id", appointment_id);
                params.put("patient_id", patient_id);
                return params;
            }
        };

        strReq.setRetryPolicy(new DefaultRetryPolicy(
                5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // Adding request to request queue
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        strReq.setShouldCache(false);
        requestQueue.add(strReq);
    }

    private void showpDialog() {

        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Please wait...");
        pDialog.setCancelable(false);

        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hidepDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    public class ViewDialog_Unit {

        public void showDialog1(Context context) {
            final Dialog dialog = new Dialog(context);
            dialog.setCanceledOnTouchOutside(true);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(true);
            dialog.setContentView(R.layout.dialog_unit);

            TextView dialogButton_gram = (TextView) dialog.findViewById(R.id.lbl_gram);
            dialogButton_gram.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    tv_unit.setText("gram");
                    dialog.dismiss();
                }
            });

            TextView dialogButton_kg = (TextView) dialog.findViewById(R.id.lbl_Kg);
            dialogButton_kg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    tv_unit.setText("kg");
                    dialog.dismiss();
                }
            });


            TextView dialogButton_pounds = (TextView) dialog.findViewById(R.id.lbl_pounds);
            dialogButton_pounds.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    tv_unit.setText("pounds");
                    dialog.dismiss();

                }
            });


            ImageView img_close = (ImageView) dialog.findViewById(R.id.img_close);
            img_close.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

            dialog.show();
        }
    }

    public class ViewDialog_Duration {

        public void showDialog1(Context context) {
            final Dialog dialog = new Dialog(context);
            dialog.setCanceledOnTouchOutside(true);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(true);
            dialog.setContentView(R.layout.dialog_duration);

            TextView dialogButton_one = (TextView) dialog.findViewById(R.id.lbl_one_time);
            dialogButton_one.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    tv_duration.setText("1 Times");
                    dialog.dismiss();
                }
            });

            TextView dialogButton_two = (TextView) dialog.findViewById(R.id.lbl_two_time);
            dialogButton_two.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    tv_duration.setText("2 Times");
                    dialog.dismiss();
                }
            });


            TextView dialogButton_three = (TextView) dialog.findViewById(R.id.lbl_three_time);
            dialogButton_three.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    tv_duration.setText("3 Times");
                    dialog.dismiss();

                }
            });

            TextView dialogButton_four = (TextView) dialog.findViewById(R.id.lbl_four_time);
            dialogButton_four.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    tv_duration.setText("4 Times");
                    dialog.dismiss();
                }
            });


            ImageView img_close = (ImageView) dialog.findViewById(R.id.img_close);
            img_close.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

            dialog.show();
        }
    }

    private void Dialog_Alert(String value) {

        // dailog_alert dialog
        final Dialog dialog = new Dialog(AddMedicine.this);
        dialog.setContentView(R.layout.dailog_box);

        // set the dailog_alert dialog components - text, image and button
        ImageView image = (ImageView) dialog.findViewById(R.id.image);
        image.setImageDrawable(getResources().getDrawable(R.drawable.ic_warning_yellow_24dp));
        image.setMaxHeight(50);
        image.setMaxWidth(50);

        TextView tv_title = (TextView) dialog.findViewById(R.id.tv_title);
        tv_title.setText("Add Medicine");

        // set the dailog_alert dialog components - text, image and button
        TextView text = (TextView) dialog.findViewById(R.id.tv_content);
        text.setText("Enter the " + value);

        Button dialogButton = (Button) dialog.findViewById(R.id.dialogButtonOK);
        // if button is clicked, close the dailog_alert dialog
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private void Dialog_Success(String value) {

        // dailog_alert dialog
        final Dialog dialog = new Dialog(AddMedicine.this);
        dialog.setContentView(R.layout.dailog_box);

        // set the dailog_alert dialog components - text, image and button
        ImageView image = (ImageView) dialog.findViewById(R.id.image);
        image.setImageDrawable(getResources().getDrawable(R.drawable.donemark));
        image.setMaxHeight(50);
        image.setMaxWidth(50);

        TextView tv_title = (TextView) dialog.findViewById(R.id.tv_title);
        tv_title.setText("Add Medicine");

        TextView text = (TextView) dialog.findViewById(R.id.tv_content);
        text.setText(value);

        Button dialogButton = (Button) dialog.findViewById(R.id.dialogButtonOK);
        // if button is clicked, close the dailog_alert dialog
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();

                Intent intent_Write_Prescription = new Intent(getApplicationContext(), Write_Prescription.class);
                intent_Write_Prescription.putExtra("appointment_id", appointment_id);
                startActivity(intent_Write_Prescription);
            }
        });

        dialog.show();
    }

    private void Dialog_Failed(String value) {

        // dailog_alert dialog
        final Dialog dialog = new Dialog(AddMedicine.this);
        dialog.setContentView(R.layout.dailog_box);

        // set the dailog_alert dialog components - text, image and button
        ImageView image = (ImageView) dialog.findViewById(R.id.image);
        image.setImageDrawable(getResources().getDrawable(R.drawable.ic_error_red_24dp));
        image.setMaxHeight(50);
        image.setMaxWidth(50);

        TextView tv_title = (TextView) dialog.findViewById(R.id.tv_title);
        tv_title.setText("Add Medicine");

        TextView text = (TextView) dialog.findViewById(R.id.tv_content);
        text.setText(value);

        Button dialogButton = (Button) dialog.findViewById(R.id.dialogButtonOK);
        // if button is clicked, close the dailog_alert dialog
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });

        dialog.show();
    }

    @Override
    public void onBackPressed() {

        Intent intent_Write_Prescription = new Intent(getApplicationContext(), Write_Prescription.class);
        intent_Write_Prescription.putExtra("appointment_id", appointment_id);
        intent_Write_Prescription.putExtra("patient_id",patient_id);
        intent_Write_Prescription.putExtra("med_id",med_id);
        intent_Write_Prescription.putExtra("user_id",user_id);
        startActivity(intent_Write_Prescription);
    }
}
