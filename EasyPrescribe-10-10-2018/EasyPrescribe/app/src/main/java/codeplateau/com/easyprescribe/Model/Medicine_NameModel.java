package codeplateau.com.easyprescribe.Model;

public class Medicine_NameModel {

    String id,name,brandname,strength,brandsfx,content,manftr,created_date,updated_date;

    public String getId () {
        return id;
    }

    public void setId (String id) {
        this.id = id;
    }

    public String getName () {
        return name;
    }

    public void setName (String name) {
        this.name = name;
    }

    public String getBrandname () {
        return brandname;
    }

    public void setBrandname (String brandname) {
        this.brandname = brandname;
    }

    public String getStrength () {
        return strength;
    }

    public void setStrength (String strength) {
        this.strength = strength;
    }

    public String getBrandsfx () {
        return brandsfx;
    }

    public void setBrandsfx (String brandsfx) {
        this.brandsfx = brandsfx;
    }

    public String getContent () {
        return content;
    }

    public void setContent (String content) {
        this.content = content;
    }

    public String getManftr () {
        return manftr;
    }

    public void setManftr (String manftr) {
        this.manftr = manftr;
    }

    public String getCreated_date () {
        return created_date;
    }

    public void setCreated_date (String created_date) {
        this.created_date = created_date;
    }

    public String getUpdated_date () {
        return updated_date;
    }

    public void setUpdated_date (String updated_date) {
        this.updated_date = updated_date;
    }
}
