package codeplateau.com.easyprescribe.Activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import codeplateau.com.easyprescribe.Model.Medicine_ListModel;
import codeplateau.com.easyprescribe.R;
import codeplateau.com.easyprescribe.Services.RestInterface;

public class Write_Prescription extends AppCompatActivity {

    private Button btn_print;
    private ImageView img_addmedicine, img_editmedicine;
    private ProgressDialog pDialog;

    private static LinearLayoutManager mLayoutManager;
    private RecyclerView rc_patients_list;
    private LinearLayout ll_no_medicine_list;

    private String appointment_id, user_id, med_id;

    private static final String TAG = Write_Prescription.class.getSimpleName().toString();

    ArrayList<Medicine_ListModel> medicine_listModelArrayList;
    writePrescription_Adapter writePrescription_adapter;

    SharedPreferences pref;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.write_prescription);

        initViews();
        setListeners();
    }

    private void initViews() {

        pref = getApplicationContext().getSharedPreferences(RestInterface.pref, Context.MODE_PRIVATE);
        user_id = pref.getString("user_id", "");

        Bundle extras = getIntent().getExtras();
        if (extras != null) {

            appointment_id = extras.getString("appointment_id");
        }

        // Handle Toolbar
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //set the back arrow in the toolbar
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.title_writePrescription);

        rc_patients_list = (RecyclerView) findViewById(R.id.rc_patients_list);
        ll_no_medicine_list = (LinearLayout) findViewById(R.id.ll_no_medicine_list);

        img_addmedicine = (ImageView) findViewById(R.id.img_addmedicine);
        img_editmedicine = (ImageView) findViewById(R.id.img_editmedicine);
        btn_print = (Button) findViewById(R.id.btn_print);

    }

    private void setListeners() {

        mLayoutManager = new LinearLayoutManager(getApplicationContext());
        rc_patients_list.setHasFixedSize(false);
        rc_patients_list.setLayoutManager(mLayoutManager);

        MedicineList_API();

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        btn_print.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent_MedicineList = new Intent(getApplicationContext(), PrintActivity.class);
                intent_MedicineList.putExtra("appointment_id", appointment_id);
                startActivity(intent_MedicineList);
            }
        });

        img_editmedicine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                StringBuilder pres_id = new StringBuilder();

                if (!medicine_listModelArrayList.isEmpty()) {

                    for(int i = 0; i < medicine_listModelArrayList.size(); i++){

                        pres_id.append(medicine_listModelArrayList.get(i).getId());
                        pres_id.append(",");
                    }

                    String id = pres_id.toString();
                    id = id.replaceAll(",$", "");

                    Intent intent_MedicineList = new Intent(getApplicationContext(), tempEditMedicine.class);
                    intent_MedicineList.putExtra("appointment_id", appointment_id);
                    intent_MedicineList.putExtra("id", id);
                    startActivity(intent_MedicineList);
                }
            }
        });

        img_addmedicine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent_AddMedicine = new Intent(getApplicationContext(), tempDynamicFieldDemo.class);
                intent_AddMedicine.putExtra("appointment_id", appointment_id);
                startActivity(intent_AddMedicine);
            }
        });
    }

    private void MedicineList_API() {

        showpDialog();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                RestInterface.URL_MedicineList, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Service Response: " + response.toString());
                hidepDialog();

                try {
                    JSONObject jObj = new JSONObject(response);
                    String success = jObj.getString("success");


                    if (success.equals("1")) {

                        medicine_listModelArrayList = new ArrayList<>();
                        medicine_listModelArrayList.clear();

                        JSONArray requestarray = jObj.getJSONArray("message");


                        for (int i = 0; i < requestarray.length(); i++) {

                            JSONObject req = requestarray.getJSONObject(i);
                            Medicine_ListModel medicine_listModels = new Medicine_ListModel();

                            medicine_listModels.setId(req.optString("user_id"));
                            medicine_listModels.setId(req.optString("id"));
                            medicine_listModels.setMed_id(req.optString("med_id"));
                            medicine_listModels.setPatient_id(req.optString("patient_id"));
                            medicine_listModels.setAppointment_id(req.optString("appointment_id"));
                            medicine_listModels.setDose(req.optString("dose"));
                            medicine_listModels.setInstruction(req.optString("instruction"));
                            medicine_listModels.setDuration(req.optString("duration"));
                            medicine_listModels.setDirections(req.optString("directions"));
                            medicine_listModels.setDescription(req.optString("description"));
                            medicine_listModels.setDays(req.optString("days"));
                            medicine_listModels.setCreated_date(req.optString("created_date"));
                            medicine_listModels.setUpdated_date(req.optString("updated_date"));
                            medicine_listModels.setMedicine(req.optString("medicine"));

                            medicine_listModelArrayList.add(medicine_listModels);
                        }

                        if (!medicine_listModelArrayList.isEmpty()) {

                            rc_patients_list.setVisibility(View.VISIBLE);
                            ll_no_medicine_list.setVisibility(View.GONE);

                            writePrescription_adapter = new writePrescription_Adapter(getApplicationContext(), medicine_listModelArrayList);
                            rc_patients_list.setAdapter(writePrescription_adapter);
                            writePrescription_adapter.notifyDataSetChanged();

                        } else {
                            ll_no_medicine_list.setVisibility(View.VISIBLE);
                            rc_patients_list.setVisibility(View.GONE);
                        }

                    } else {

                        ll_no_medicine_list.setVisibility(View.VISIBLE);
                        rc_patients_list.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Cust req Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();
                hidepDialog();
            }
        }) {

            @Override
            protected java.util.Map<String, String> getParams() {
                // Posting params to register url
                java.util.Map<String, String> params = new HashMap<String, String>();
                params.put("appointment_id", appointment_id);
                params.put("user_id", user_id);
                return params;
            }
        };

        // Adding request to request queue
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        strReq.setShouldCache(false);
        requestQueue.add(strReq);

    }

    private void showpDialog() {

        // Progress Dialog
        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Please wait...");
        pDialog.setCancelable(false);

        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hidepDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    public class writePrescription_Adapter extends RecyclerView.Adapter<writePrescription_Adapter.ViewHolder> {

        private ArrayList<Medicine_ListModel> medicine_listModelArrayList;
        private Context context;
        private ProgressDialog pDialog;

        public writePrescription_Adapter(Context context, ArrayList<Medicine_ListModel> medicine_listModels) {
            this.medicine_listModelArrayList = medicine_listModels;
            this.context = context;
        }

        @Override
        public writePrescription_Adapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {

            View view;
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_write_prescription, viewGroup, false);

            return new writePrescription_Adapter.ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(final writePrescription_Adapter.ViewHolder viewHolder, final int i) {

            String iconurl = "https://www.frenzybet.com/uploads/icons/" + medicine_listModelArrayList.get(i).getMedicine();

            viewHolder.tv_medicinename.setText(medicine_listModelArrayList.get(i).getMedicine());

            String dose = medicine_listModelArrayList.get(i).getDose();
            String duration = medicine_listModelArrayList.get(i).getDuration();

            viewHolder.tv_description.setText(dose + " " + duration + " per day");

            viewHolder.img_edit.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {

                    String appointment_id = medicine_listModelArrayList.get(i).getAppointment_id();
                    String id = medicine_listModelArrayList.get(i).getId();

                    Intent intent_MedicineList = new Intent(context.getApplicationContext(), tempEditMedicine.class);
                    intent_MedicineList.putExtra("id",id);
                    intent_MedicineList.putExtra("appointment_id", appointment_id);
                    context.startActivity(intent_MedicineList);
                }
            });

            viewHolder.img_delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    med_id = medicine_listModelArrayList.get(i).getId();
                    appointment_id = medicine_listModelArrayList.get(i).getAppointment_id();
                    Dialog_Delete(med_id, appointment_id);
                }
            });

            viewHolder.img_edit.setOnClickListener(new View.OnClickListener() {

                String id = medicine_listModelArrayList.get(i).getId();
                String med_id = medicine_listModelArrayList.get(i).getMed_id();
                String patient_id = medicine_listModelArrayList.get(i).getPatient_id();
                String appointment_id = medicine_listModelArrayList.get(i).getAppointment_id();
                String dose = medicine_listModelArrayList.get(i).getDose();
                String instruction = medicine_listModelArrayList.get(i).getInstruction();
                String duration = medicine_listModelArrayList.get(i).getDuration();
                String directions = medicine_listModelArrayList.get(i).getDirections();
                String medicine = medicine_listModelArrayList.get(i).getMedicine();
                String user_id = medicine_listModelArrayList.get(i).getId();

                @Override
                public void onClick(View v) {

                    Intent intent_MedicineList = new Intent(context.getApplicationContext(), EditMedicine.class);
                    intent_MedicineList.putExtra("id", id);
                    intent_MedicineList.putExtra("med_id", med_id);
                    intent_MedicineList.putExtra("patient_id", patient_id);
                    intent_MedicineList.putExtra("appointment_id", appointment_id);
                    intent_MedicineList.putExtra("dose", dose);
                    intent_MedicineList.putExtra("instruction", instruction);
                    intent_MedicineList.putExtra("duration", duration);
                    intent_MedicineList.putExtra("directions", directions);
                    intent_MedicineList.putExtra("medicine", medicine);
                    intent_MedicineList.putExtra("user_id", user_id);
                    context.startActivity(intent_MedicineList);

                }
            });
        }

        @Override
        public int getItemCount() {
            return medicine_listModelArrayList.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            TextView tv_medicinename, tv_description;
            ImageView img_edit, img_delete;
            LinearLayout ll_write_prescription;

            public ViewHolder(View view) {
                super(view);

                tv_medicinename = (TextView) view.findViewById(R.id.tv_medicinename);
                tv_description = (TextView) view.findViewById(R.id.tv_description);

                img_edit = (ImageView) view.findViewById(R.id.img_edit);
                img_delete = (ImageView) view.findViewById(R.id.img_delete);

                ll_write_prescription = (LinearLayout) view.findViewById(R.id.ll_write_prescription);

            }
        }

        private void Delete_PrescriptionAPI(final String med_id, final String Appointment_id) {

            showpDialog();
            StringRequest strReq = new StringRequest(Request.Method.POST,
                    RestInterface.URL_DeletePrescription, new Response.Listener<String>() {

                @Override
                public void onResponse(String response) {
                    Log.d(TAG, "Login Response: " + response);
                    //hideProgressDialog();
                    hidepDialog();
                    try {
                        JSONObject jObj = new JSONObject(response);
                        String success = jObj.getString("success");

                        if (success.equals("1")) {

                            String message = jObj.getString("message");
                            //Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                            Dialog_Success(message, Appointment_id);

                        } else {

                            String message = jObj.getString("message");
                            //Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                            Dialog_Failed(message);
                            hidepDialog();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e(TAG, "Error: " + error.getMessage());
                    //Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
                    hidepDialog();
                }
            }) {

                @Override
                protected java.util.Map<String, String> getParams() {
                    // Posting params to register url
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("id", med_id);
                    return params;
                }
            };

            strReq.setRetryPolicy(new DefaultRetryPolicy(
                    5000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            // Adding request to request queue
            RequestQueue requestQueue = Volley.newRequestQueue(context);
            strReq.setShouldCache(false);
            requestQueue.add(strReq);
        }

        private void Dialog_Delete(final String id, final String appointment_id) {

            // dailog_alert dialog
            final Dialog dialog = new Dialog(Write_Prescription.this);
            dialog.setContentView(R.layout.dailog_box_delete);

            // set the dailog_alert dialog components - text, image and button
            ImageView image = (ImageView) dialog.findViewById(R.id.image);
            image.setImageDrawable(getResources().getDrawable(R.drawable.ic_warning_yellow_24dp));
            image.setMaxHeight(50);
            image.setMaxWidth(50);

            TextView tv_title = (TextView) dialog.findViewById(R.id.tv_title);
            tv_title.setText("Delete");

            // set the dailog_alert dialog components - text, image and button
            TextView text = (TextView) dialog.findViewById(R.id.tv_content);
            text.setText("Are you sure to delete this prescription ?");

            Button dialogButton_ok = (Button) dialog.findViewById(R.id.dialogButtonOK);
            Button dialogButton_Cancel = (Button) dialog.findViewById(R.id.dialogButtonCancel);

            // if button is clicked, close the dailog_alert dialog
            dialogButton_ok.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    dialog.dismiss();
                    Delete_PrescriptionAPI(med_id, appointment_id);
                }
            });

            dialogButton_Cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    dialog.dismiss();
                }
            });

            dialog.show();
        }

        private void Dialog_Success(final String value, final String appointment_id) {

            // dailog_alert dialog
            final Dialog dialog = new Dialog(Write_Prescription.this);
            dialog.setContentView(R.layout.dailog_box);

            // set the dailog_alert dialog components - text, image and button
            ImageView image = (ImageView) dialog.findViewById(R.id.image);
            image.setImageDrawable(getResources().getDrawable(R.drawable.donemark));
            image.setMaxHeight(50);
            image.setMaxWidth(50);

            TextView tv_title = (TextView) dialog.findViewById(R.id.tv_title);
            tv_title.setText("Prescription List");

            TextView text = (TextView) dialog.findViewById(R.id.tv_content);
            text.setText(value);

            Button dialogButton = (Button) dialog.findViewById(R.id.dialogButtonOK);
            // if button is clicked, close the dailog_alert dialog
            dialogButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    dialog.dismiss();

                    Intent intent_Write_Prescription = new Intent(context.getApplicationContext(), Write_Prescription.class);
                    intent_Write_Prescription.putExtra("appointment_id", appointment_id);
                    context.startActivity(intent_Write_Prescription);
                }
            });

            dialog.show();
        }

        private void Dialog_Failed(String value) {

            // dailog_alert dialog
            final Dialog dialog = new Dialog(Write_Prescription.this);
            dialog.setContentView(R.layout.dailog_box);

            // set the dailog_alert dialog components - text, image and button
            ImageView image = (ImageView) dialog.findViewById(R.id.image);
            image.setImageDrawable(getResources().getDrawable(R.drawable.ic_error_red_24dp));
            image.setMaxHeight(50);
            image.setMaxWidth(50);

            TextView tv_title = (TextView) dialog.findViewById(R.id.tv_title);
            tv_title.setText("Prescription List");

            TextView text = (TextView) dialog.findViewById(R.id.tv_content);
            text.setText(value);

            Button dialogButton = (Button) dialog.findViewById(R.id.dialogButtonOK);
            // if button is clicked, close the dailog_alert dialog
            dialogButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    dialog.dismiss();
                }
            });

            dialog.show();
        }

        private void showpDialog() {

            // Progress Dialog
            pDialog = new ProgressDialog(Write_Prescription.this);
            pDialog.setMessage("Please wait...");
            pDialog.setCancelable(false);

            if (!pDialog.isShowing())
                pDialog.show();
        }

        private void hidepDialog() {
            if (pDialog.isShowing())
                pDialog.dismiss();
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent_DrawerLayoutActivity = new Intent(getApplicationContext(), DrawerLayoutActivity.class);
        startActivity(intent_DrawerLayoutActivity);
    }
}
