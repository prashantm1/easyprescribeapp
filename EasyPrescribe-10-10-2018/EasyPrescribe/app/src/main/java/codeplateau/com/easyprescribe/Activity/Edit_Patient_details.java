package codeplateau.com.easyprescribe.Activity;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import codeplateau.com.easyprescribe.Services.RestInterface;
import codeplateau.com.easyprescribe.R;

public class Edit_Patient_details extends AppCompatActivity {
    private EditText txt_patientname,txt_MoNo,txt_date,txt_insruction;
    private RadioGroup txt_radioGroup;
    private Button btn_addpresc,btn_subpresc,btn_AddMedicine;
    private TextView tv_blood,txt_weight,txt_height;
    Calendar myCalendar = null;
    private static String id,name,phone,dateofbirth,bloodgrp,gender,height,weight,description,created_date;

    private int myear;
    private int mmonth;
    private int mday;
    static final int DATE_DIALOG_START = 999;
    static final int DATE_DIALOG_END = 888;
    private ProgressDialog pDialog;
    private static final String TAG = "Edit Patient_detial";
    private String appointment_id,patient_id,med_id,user_id;


    @Override
    protected void onCreate (Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit__patient_details);
        initViews();
        setListeners();

    }
    private void initViews(){


        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            appointment_id = extras.getString("appointment_id");
            patient_id= extras.getString("patient_id");
            med_id= extras.getString("med_id");
            user_id= extras.getString("user_id");
        }

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

        // Handle Toolbar
        android.support.v7.widget.Toolbar toolbar = (android.support.v7.widget.Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //set the back arrow in the toolbar
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.title_EditPatientDetail);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        txt_patientname=(EditText)findViewById(R.id.txt_patientname);
        txt_MoNo=(EditText)findViewById(R.id.txt_MoNo);
        txt_date=(EditText )findViewById(R.id.txt_date);
        tv_blood = (TextView ) findViewById(R.id.tv_blood);
        txt_radioGroup = (RadioGroup) findViewById(R.id.txt_radioGroup);
        txt_height = (TextView) findViewById(R.id.txt_height);
        txt_weight = (TextView ) findViewById(R.id.txt_weight);
        txt_insruction = (EditText) findViewById(R.id.txt_insruction);
        btn_addpresc= (Button)findViewById(R.id.btn_addpresc);
        btn_subpresc=(Button)findViewById(R.id.btn_submit);
        btn_AddMedicine=(Button)findViewById(R.id.btn_WritePrescription);
    }

    private void setListeners(){

        getEditPatientDetail_API();

        myCalendar = Calendar.getInstance();
        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                updateLabel();
            }
        };

        txt_date.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                /*new DatePickerDialog(UpdateChallengeDetail.this, date1, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();*/
                setCurrentDateOnView_StartDate();
                addListenerOnButton_StartDate();
            }
        });

        txt_height.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               Edit_Patient_details.ViewDialog alert = new Edit_Patient_details.ViewDialog();
                alert.showDialog(Edit_Patient_details.this);
            }
        });
        txt_weight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick (View view) {

                Edit_Patient_details.ViewDialog1 alert = new Edit_Patient_details.ViewDialog1();
                alert.showDialog1(Edit_Patient_details.this);

            }
        });
        tv_blood.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick (View view) {

                Edit_Patient_details.ViewDialog2 alert = new Edit_Patient_details.ViewDialog2();
                alert.showDialog2(Edit_Patient_details.this);

            }
        });

        btn_AddMedicine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick (View view) {

                Intent intent = new Intent(Edit_Patient_details.this, Write_Prescription.class);
                intent.putExtra("appointment_id", appointment_id);
                intent.putExtra("patient_id",patient_id);
                intent.putExtra("med_id",med_id);
                intent.putExtra("user_id",user_id);
                startActivity(intent);

                getUpdateAPI();
            }
        });

    }

    private void getEditPatientDetail_API () {
        // Tag used to cancel the request
        showpDialog();
        StringRequest strReq = new StringRequest(Request.Method.POST,
                RestInterface.URL_EditPatient_detail, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Login Response: " + response);
                //hideProgressDialog();
                hidepDialog();
                try {
                    JSONObject jObj = new JSONObject(response);
                    String success = jObj.getString("success");

                    if (success.equals("1")) {

                        JSONArray requestarray = jObj.getJSONArray("data");

                        for (int i = 0; i < requestarray.length(); i++) {

                            JSONObject req = requestarray.getJSONObject(i);
                        id = req.optString("userid");
                        name= req.optString("name");
                        phone = req.optString("phone");
                        dateofbirth = req.optString("dateofbirth");
                        bloodgrp = req.optString("bloodgrp");
                        gender = req.optString("gender");
                        height = req.optString("height");
                        weight = req.optString("weight");
                        description = req.optString("description");

                        txt_patientname.setText(name);
                        txt_MoNo.setText(phone);
                        txt_date.setText(dateofbirth);
                        tv_blood.setText(bloodgrp);
                        txt_height.setText(height);
                        txt_weight.setText(weight);
                        txt_insruction.setText(description);
                        }


                    } else {
                        hidepDialog();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Error: " + error.getMessage());
                //Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
                hidepDialog();
            }
        }) {

            @Override
            protected java.util.Map<String, String> getParams() {
                // Posting params to register url
                Map<String, String> params = new HashMap<String, String>();
                params.put("name","Test1" );
                params.put("phone", "123456789011");

                return params;
            }
        };

        strReq.setRetryPolicy(new DefaultRetryPolicy(
                5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // Adding request to request queue
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        strReq.setShouldCache(false);
        requestQueue.add(strReq);
    }


    private void getUpdateAPI() {

        showpDialog();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                RestInterface.URL_UpdateApI, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Login Response: " + response);
                //hideProgressDialog();
                hidepDialog();
                try {
                    JSONObject jObj = new JSONObject(response);
                    String success = jObj.getString("success");

                    if (success.equals("1")) {

                        //      JSONObject jsonObject = jObj.getJSONObject("data");

                        String data = jObj.getString("data");
                        Toast.makeText(getApplicationContext(), data, Toast.LENGTH_SHORT).show();

                        Intent intent_MedicineList = new Intent(getApplicationContext(), Write_Prescription.class);

                        intent_MedicineList.putExtra("appointment_id", appointment_id);
                        intent_MedicineList.putExtra("patient_id",patient_id);
                        intent_MedicineList.putExtra("med_id",med_id);
                        intent_MedicineList.putExtra("user_id",user_id);
                        startActivity(intent_MedicineList);

                    } else {

                        String message = jObj.getString("data");
                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();

                        hidepDialog();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Error: " + error.getMessage());
                //Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
                hidepDialog();
            }
        }) {

            @Override
            protected java.util.Map<String, String> getParams() {
                // Posting params to register url
                Map<String, String> params = new HashMap<String, String>();
                params.put("name", txt_patientname.getText().toString().trim());
                params.put("phone", txt_MoNo.getText().toString().trim());
                params.put("dateofbirth", txt_date.getText().toString().trim());
                params.put("userid", "4");
                params.put("bloodgrp", tv_blood.getText().toString().trim());
                params.put("gender", "Male");
                params.put("weight", txt_weight.getText().toString().trim());
                params.put("height", txt_height.getText().toString().trim());
                params.put("description", txt_insruction.getText().toString().trim());

                return params;
            }
        };

        strReq.setRetryPolicy(new DefaultRetryPolicy(
                5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // Adding request to request queue
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        strReq.setShouldCache(false);
        requestQueue.add(strReq);
    }


    public void setCurrentDateOnView_StartDate() {

        final Calendar c = Calendar.getInstance();
        myear = c.get(Calendar.YEAR);
        mmonth = c.get(Calendar.MONTH);
        mday = c.get(Calendar.DAY_OF_MONTH);

        // set current date into textview
        txt_date.setText(new StringBuilder()
                // Month is 0 based, just add 1
                .append(myear + 1).append("/").append(mmonth).append("/")
                .append(mday).append(" "));
    }
    public void addListenerOnButton_StartDate() {

        txt_date.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                showDialog(DATE_DIALOG_START);
            }
        });

    }

    private void updateLabel() {
        //String myFormat = "MM/dd/yy"; //In which you need put here
        String myFormat = "yy/MM/dd"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        txt_date.setText(sdf.format(myCalendar.getTime()));
    }

    private void showpDialog() {

        // Progress Dialog
        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Please wait...");
        pDialog.setCancelable(false);

        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hidepDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }


    public class ViewDialog {

        public void showDialog(Context context) {
            final Dialog dialog = new Dialog(context);
            dialog.setCanceledOnTouchOutside(true);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(false);
            dialog.setContentView(R.layout.item_heights);

            TextView dialogButton_all = (TextView) dialog.findViewById(R.id.lbl_all);
            dialogButton_all.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    txt_height.setText("All");
                   // getEditPatientDetail_API("Height", "");
                    dialog.dismiss();
                }
            });

            TextView dialogButton_jan = (TextView) dialog.findViewById(R.id.lbl_ft);
            dialogButton_jan.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    txt_height.setText("ft");
                  //  getEditPatientDetail_API("Height", "01");
                    dialog.dismiss();
                }
            });

            TextView dialogButton_feb = (TextView) dialog.findViewById(R.id.lbl_inches);
            dialogButton_feb.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    txt_height.setText("inches");
                  //  getEditPatientDetail_API("Height", "02");
                    dialog.dismiss();
                }
            });


            ImageView img_close = (ImageView) dialog.findViewById(R.id.img_close);
            img_close.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

            dialog.show();
        }
    }


    public class ViewDialog1 {

        public void showDialog1(Context context) {
            final Dialog dialog = new Dialog(context);
            dialog.setCanceledOnTouchOutside(true);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(false);
            dialog.setContentView(R.layout.item_weights);

            TextView dialogButton_all = (TextView) dialog.findViewById(R.id.lbl_all);
            dialogButton_all.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    txt_weight.setText("All");
                  //  getEditPatientDetail_API("Weight", "01");
                    dialog.dismiss();
                }
            });

            TextView dialogButton_jan = (TextView) dialog.findViewById(R.id.lbl_Kg);
            dialogButton_jan.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    txt_weight.setText("Kg");
                    //getEditPatientDetail_API("Weight", "02");
                    dialog.dismiss();
                }
            });

            TextView dialogButton_feb = (TextView) dialog.findViewById(R.id.lbl_gram);
            dialogButton_feb.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    txt_weight.setText("gram");
                  //  getEditPatientDetail_API("Weight", "03");
                    dialog.dismiss();
                }
            });

            // lbl_july
            TextView dialogButton_march = (TextView) dialog.findViewById(R.id.lbl_pounds);
            dialogButton_march.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    txt_weight.setText("pounds");
                    // AddPatient_detail_API("Weight", "03");
                    dialog.dismiss();
                }
            });


            ImageView img_close = (ImageView) dialog.findViewById(R.id.img_close);
            img_close.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

            dialog.show();
        }
    }

    public class ViewDialog2 {

        public void showDialog2(Context context) {
            final Dialog dialog = new Dialog(context);
            dialog.setCanceledOnTouchOutside(true);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(false);
            dialog.setContentView(R.layout.item_bloodgroup);

            TextView dialogButton_all = (TextView) dialog.findViewById(R.id.lbl_all);
            dialogButton_all.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    tv_blood.setText("All");
                   // getEditPatientDetail_API("Blood Group", "");
                    dialog.dismiss();
                }
            });
            TextView dialogButton_Apositive = (TextView) dialog.findViewById(R.id.lbl_APositive);
            dialogButton_Apositive.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    tv_blood.setText("A+");
                    //getEditPatientDetail_API("Blood Group", "01");
                    dialog.dismiss();
                }
            });

            TextView dialogButton_ANegative = (TextView) dialog.findViewById(R.id.lbl_ANegative);
            dialogButton_ANegative.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    tv_blood.setText("A-");
                   // getEditPatientDetail_API("Blood Group", "02");
                    dialog.dismiss();
                }
            });

            // lbl_july
            TextView dialogButton_Bpositive = (TextView) dialog.findViewById(R.id.lbl_Bpositive);
            dialogButton_Bpositive.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    tv_blood.setText("B+");
                   // getEditPatientDetail_API("Blood Group", "03");
                    dialog.dismiss();
                }
            });
            TextView dialogButton_BNegative = (TextView) dialog.findViewById(R.id.lbl_BNegative);
            dialogButton_BNegative.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    tv_blood.setText("B-");
                   // getEditPatientDetail_API("Blood Group", "04");
                    dialog.dismiss();
                }
            });
            TextView dialogButton_AB = (TextView) dialog.findViewById(R.id.lbl_ABpositive);
            dialogButton_AB.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    tv_blood.setText("AB+");
                   // getEditPatientDetail_API("Blood Group", "05");
                    dialog.dismiss();
                }
            });
            TextView dialogButton_ABNegative = (TextView) dialog.findViewById(R.id.lbl_ABNegative);
            dialogButton_ABNegative.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    tv_blood.setText("AB-");
                    //getEditPatientDetail_API("Blood Group", "06");
                    dialog.dismiss();
                }
            });
            TextView dialogButton_Opositive = (TextView) dialog.findViewById(R.id.lbl_Opositive);
            dialogButton_Opositive.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    tv_blood.setText("O+");
                   // getEditPatientDetail_API("Blood Group", "07");
                    dialog.dismiss();
                }
            });
            TextView dialogButton_ONegative = (TextView) dialog.findViewById(R.id.lbl_ONegative);
            dialogButton_ONegative.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    tv_blood.setText("O-");
                    //getEditPatientDetail_API("Blood Group", "08");
                    dialog.dismiss();
                }
            });


            ImageView img_close = (ImageView) dialog.findViewById(R.id.img_close);
            img_close.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

            dialog.show();
        }
    }


}
