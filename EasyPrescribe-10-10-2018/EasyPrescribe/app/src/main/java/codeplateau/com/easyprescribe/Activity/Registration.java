package codeplateau.com.easyprescribe.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import codeplateau.com.easyprescribe.Services.RestInterface;
import codeplateau.com.easyprescribe.R;

public class Registration extends AppCompatActivity {
    private EditText txt_doctorname, txt_emaild, txt_mobileno, txt_password;
    private Button btn_registration;

    private ProgressDialog pDialog;

    private  String name, email, phone, password;
    private static final String TAG = Registration.class.getSimpleName();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registarion);

        initViews();
        setListeners();
    }

    private void initViews() {

        txt_doctorname = (EditText) findViewById(R.id.txt_doctorname);
        txt_emaild = (EditText) findViewById(R.id.txt_emaild);
        txt_mobileno = (EditText) findViewById(R.id.txt_mobileno);
        txt_password = (EditText) findViewById(R.id.txt_password);

        btn_registration = (Button) findViewById(R.id.btn_registration);

    }

    private void setListeners() {


        btn_registration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (txt_doctorname.getText().toString().equals("")) {
                    Toast.makeText(getApplicationContext(), "Enter Doctor Name!", Toast.LENGTH_SHORT).show();
                    txt_doctorname.requestFocus();
                } else if (txt_emaild.getText().toString().equals("")) {
                    Toast.makeText(getApplicationContext(), "Enter Email", Toast.LENGTH_SHORT).show();
                    txt_emaild.requestFocus();
                } else if (txt_mobileno.getText().toString().equals("")) {
                    Toast.makeText(getApplicationContext(), "Enter Mobileno", Toast.LENGTH_SHORT).show();
                    txt_mobileno.requestFocus();
                } else if (txt_password.getText().toString().equals("")) {
                    Toast.makeText(getApplicationContext(), "Enter password", Toast.LENGTH_SHORT).show();
                    txt_password.requestFocus();
                } else {

                    boolean emailid = isEmailValid(txt_emaild.getText().toString().trim());

                    if (!emailid) {

                        Toast.makeText(getApplicationContext(), "Email is not valid", Toast.LENGTH_SHORT).show();
                        txt_emaild.requestFocus();

                    } else {

                        name = txt_doctorname.getText().toString().trim();
                        email = txt_emaild.getText().toString().trim();
                        password = txt_password.getText().toString().trim();
                        phone = txt_mobileno.getText().toString().trim();

                        Registration_API();
                    }
                }
            }
        });

    }

    @Override
    public void onBackPressed() {
        // code here to show dialog

        Intent intent_GroupChallengesList = new Intent(getApplicationContext(), MainScreen.class);
        startActivity(intent_GroupChallengesList);
    }


    private void Registration_API() {

        showpDialog();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                RestInterface.URL_Registration, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Login Response: " + response);

                hidepDialog();
                try {
                    JSONObject jObj = new JSONObject(response);
                    String success = jObj.getString("success");

                    if (success.equals("1")) {

                        String message = jObj.getString("message");
                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();

                        Intent intent_Login= new Intent(getApplicationContext(), Login.class);
                        startActivity(intent_Login);

                    } else {

                        String message = jObj.getString("message");
                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();

                        hidepDialog();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Error: " + error.getMessage());
                //Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
                hidepDialog();
            }
        }) {

            @Override
            protected java.util.Map<String, String> getParams() {
                // Posting params to register url
                Map<String, String> params = new HashMap<String, String>();
                params.put("name", name);
                params.put("email", email);
                params.put("phone", phone);
                params.put("password", password);

                return params;
            }
        };

        strReq.setRetryPolicy(new DefaultRetryPolicy(
                5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // Adding request to request queue
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        strReq.setShouldCache(false);
        requestQueue.add(strReq);
    }

    public static boolean isEmailValid(String email) {
        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    private void showpDialog() {

        // Progress Dialog
        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Please wait...");
        pDialog.setCancelable(false);

        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hidepDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
}
