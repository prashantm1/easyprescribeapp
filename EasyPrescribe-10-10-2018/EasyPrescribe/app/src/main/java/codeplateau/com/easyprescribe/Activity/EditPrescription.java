package codeplateau.com.easyprescribe.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import codeplateau.com.easyprescribe.R;
import codeplateau.com.easyprescribe.Services.RestInterface;

public class EditPrescription extends AppCompatActivity {

    private EditText txt_manufacter,txt_patientname1;
    private Button btn_updateMedicine;
    private ProgressDialog pDialog;
    private static View view;
    private TextView txt_dose,txt_dur,txt_direction,txt_unit,lbl_unit;
    private static final String TAG = "Add_Medicine";
    private static String id,med_id,dose,instruction,unit,directions,duration,patient_id,appointment_id,created_date,updated_date;
   // private String patient_id;
    private RadioGroup txt_radioGroup;


    @Override
    protected void onCreate (Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_prescriptionactivity);

        initViews();
        setListeners();
    }
    private void  initViews(){

        Bundle extras = getIntent().getExtras();

        if (extras != null) {
            appointment_id = extras.getString("appointment_id");
            patient_id= extras.getString("patient_id");
            med_id= extras.getString("med_id");
        }

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

        // Handle Toolbar
        android.support.v7.widget.Toolbar toolbar = (android.support.v7.widget.Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //set the back arrow in the toolbar
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.title_Edit_Medicine);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });


        txt_patientname1=(EditText)findViewById(R.id.txt_patientname1);
      //  txt_info=(EditText )findViewById(R.userid.txt_info);
        txt_dose=(TextView )findViewById(R.id.txt_dose);
        txt_dur=(TextView )findViewById(R.id.txt_dururation);
        txt_unit=(TextView )findViewById(R.id.txt_unit);
        txt_direction=(TextView )findViewById(R.id.txt_direction);
        txt_manufacter=(EditText)findViewById(R.id.txt_manufacter);
        btn_updateMedicine=(Button)findViewById(R.id.btn_updateMedicine);
        txt_radioGroup=(RadioGroup )findViewById(R.id.txt_radioGroup);
        lbl_unit=(TextView )findViewById(R.id.lbl_unit);

    }
    private void  setListeners(){

        EditPrescription_API();
        btn_updateMedicine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick (View view) {

                Update_PrescriptionAPI();
            }
        });

    }
    private void EditPrescription_API () {
        // Tag used to cancel the request
        showpDialog();
        StringRequest strReq = new StringRequest(Request.Method.POST,
                RestInterface.URL_EditPrescription, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Login Response: " + response);

                hidepDialog();
                try {
                    JSONObject jObj = new JSONObject(response);
                    String success = jObj.getString("success");

                    if (success.equals("1")) {

                        JSONArray requestarray = jObj.getJSONArray("data");

                        for (int i = 0; i < requestarray.length(); i++) {

                            JSONObject req = requestarray.getJSONObject(i);
                            id = req.optString("userid");
                            med_id= req.optString("med_id");
                            patient_id= req.optString("patient_id");
                            appointment_id=req.optString("appointment_id");
                            dose=req.optString("dose");
                            instruction= req.optString("instruction");
                            duration= req.optString("duration");
                            directions= req.optString("directions");
                            created_date = req.optString("created_date");
                            updated_date = req.optString("updated_date");




                            txt_patientname1.setText("hellow");
                            txt_dose.setText(dose);
                            txt_direction.setText(directions);
                            txt_dur.setText(duration);
                            txt_dur.setText(duration);


                        }




                    } else {
                        hidepDialog();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Error: " + error.getMessage());
                //Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
                hidepDialog();
            }
        }) {

            @Override
            protected java.util.Map<String, String> getParams() {
                // Posting params to register url
                Map<String, String> params = new HashMap<String, String>();

                params.put("userid","1");


                return params;
            }
        };

        strReq.setRetryPolicy(new DefaultRetryPolicy(
                5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // Adding request to request queue
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        strReq.setShouldCache(false);
        requestQueue.add(strReq);
    }


    private void Update_PrescriptionAPI() {

        showpDialog();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                RestInterface.URL_UpdatePrescription, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Edit Prescription Response: " + response);
                //hideProgressDialog();
                hidepDialog();
                try {
                    JSONObject jObj = new JSONObject(response);
                    String success = jObj.getString("success");

                    if (success.equals("1")) {

                        String message = jObj.getString("message");
                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();

                   } else {

                        String message = jObj.getString("message");
                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Error: " + error.getMessage());
                //Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
                hidepDialog();
            }
        }) {

            @Override
            protected java.util.Map<String, String> getParams() {
                // Posting params to register url
                Map<String, String> params = new HashMap<String, String>();

                params.put("med_id","1");
                params.put("dose", txt_dose.getText().toString().trim());
                //   params.put("instruction", txt_radioGroup.getText().toString().trim());
                params.put("unit",txt_unit.getText().toString().trim());
                params.put("directions",txt_direction.getText().toString().trim());
                params.put("duration", txt_dur.getText().toString().trim());
                params.put("appointment_id",appointment_id);
                params.put("user_id","1");
                params.put("userid", "1");

                return params;
            }
        };

        strReq.setRetryPolicy(new DefaultRetryPolicy(
                5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // Adding request to request queue
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        strReq.setShouldCache(false);
        requestQueue.add(strReq);
    }

    private void showpDialog() {

        // Progress Dialog
        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Please wait...");
        pDialog.setCancelable(false);

        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hidepDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    @Override
    public void onBackPressed() {
        Intent intent_CreateChallengesSelect = new Intent(getApplicationContext(),Write_Prescription.class);
        startActivity(intent_CreateChallengesSelect);
    }
}
