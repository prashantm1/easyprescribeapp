package codeplateau.com.easyprescribe.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import codeplateau.com.easyprescribe.Activity.EditPrescription;
import codeplateau.com.easyprescribe.Model.Medicine_ListModel;
import codeplateau.com.easyprescribe.Model.Medicine_NameModel;
import codeplateau.com.easyprescribe.R;

public class MediciNameAdapter extends RecyclerView.Adapter<MediciNameAdapter.ViewHolder> {

    private ArrayList<Medicine_NameModel> medicine_nameModelArrayList ;
    private Context context;

    public MediciNameAdapter(Context context, ArrayList<Medicine_NameModel> medicine_nameModels) {
        this.medicine_nameModelArrayList = medicine_nameModels;
        this.context = context;
    }

    @Override
    public MediciNameAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {

        View view;
        view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.support_simple_spinner_dropdown_item, viewGroup, false);

        return new MediciNameAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final MediciNameAdapter.ViewHolder viewHolder, final int i) {

        String iconurl = "https://www.frenzybet.com/uploads/icons/"+medicine_nameModelArrayList.get(i).getName();


        viewHolder.text1.setText(medicine_nameModelArrayList.get(i).getName());



    }

    @Override
    public int getItemCount() {
        return medicine_nameModelArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView text1;


        public ViewHolder(View view) {
            super(view);

            //  img_challenges_icon = (ImageView) view.findViewById(R.id.img_challenges_icon);
           // text1 = (TextView) view.findViewById(R.id.text1);



        }
    }
}
