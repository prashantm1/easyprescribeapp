package codeplateau.com.easyprescribe.Activity;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import codeplateau.com.easyprescribe.Fragment.Appointment_Fragment;
import codeplateau.com.easyprescribe.Adapter.Navigation_Drawer_Adapter;
import codeplateau.com.easyprescribe.Fragment.Logout_Fragment;
import codeplateau.com.easyprescribe.Fragment.MyProfile;
import codeplateau.com.easyprescribe.Model.Navigation_Items;
import codeplateau.com.easyprescribe.R;
import codeplateau.com.easyprescribe.Fragment.patient_listeningFragment;
import codeplateau.com.easyprescribe.Services.RestInterface;


public class DrawerLayoutActivity extends AppCompatActivity {

    private static DrawerLayout drawer;
    private static ActionBarDrawerToggle actionbarToggle;

    private static ArrayList<Navigation_Items> arrayList;
    private static Navigation_Drawer_Adapter adapter;

    private static ListView listview;

    private static FragmentManager fragment_manager;
    private static Toolbar toolbar;
    private static RelativeLayout left_slider;
    private Boolean exit = false;

    //private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;

    public SharedPreferences sharedPreferences;

    private String name, email, phone;
    private static TextView txt_profilename, txt_profileemail;

    public DrawerLayoutActivity activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        init();
        populateListItems();

        // Replace the default/home fragment if savedinstance is null
        if (savedInstanceState == null) {
            selectItem(0);
        }
    }

    private void setupViewPager(ViewPager viewPager) {

        DrawerLayoutActivity.ViewPagerAdapter adapter = new DrawerLayoutActivity.ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new Appointment_Fragment(), "Appointment List");
        adapter.addFragment(new patient_listeningFragment(), "Patient Listing");
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    void init() {

        sharedPreferences = getApplicationContext().getSharedPreferences(RestInterface.pref, Context.MODE_PRIVATE);
        name = sharedPreferences.getString("name", "");
        email = sharedPreferences.getString("email", "");
        phone = sharedPreferences.getString("phone", "");

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        listview = (ListView) findViewById(R.id.list_slidermenu);
        toolbar = (Toolbar) findViewById(R.id.toolbar);

        txt_profilename = (TextView) findViewById(R.id.txt_profilename);
        txt_profileemail = (TextView) findViewById(R.id.txt_profileemail);

        txt_profilename.setText(name);
        txt_profileemail.setText(email);

        left_slider = (RelativeLayout) findViewById(R.id.slider);

        // Fragment manager to manage fragment
        fragment_manager = getSupportFragmentManager();
        arrayList = new ArrayList<Navigation_Items>();

        // Setting actionbar toggle
        actionbarToggle = new ActionBarDrawerToggle(this, drawer, toolbar,
                R.string.drawer_open, R.string.drawer_close) {

            @Override
            public void onDrawerClosed(View drawerView) {
               //Toast.makeText(DrawerLayoutActivity.this, "Drawer Close", Toast.LENGTH_SHORT).show();
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                //Toast.makeText(DrawerLayoutActivity.this, "Drawer Open", Toast.LENGTH_SHORT).show();
                super.onDrawerOpened(drawerView);
            }
        };

        // Setting drawer listener
        drawer.setDrawerListener(actionbarToggle);

    }

    // Populate navigation drawer listitems
    void populateListItems() {
        Integer Icons[] = {
                R.drawable.ic_account_circle_black_24dp,
                R.drawable.ic_account_circle_black_24dp,
                R.drawable.ic_settings_backup_restore_black_24dp};
        String title[] = getResources().getStringArray(R.array.list_items);
        for (int i = 0; i < Icons.length; i++) {
            arrayList.add(new Navigation_Items(title[i], Icons[i]));
        }

        adapter = new Navigation_Drawer_Adapter(DrawerLayoutActivity.this, arrayList);

        // Setting adapter
        listview.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    // Select item method for replacing fragments
    public void selectItem(int position) {

        // Setting toolbar title
        //toolbar.setTitle(arrayList.get(position).getTitle());

        // Close drawer method
        closeDrawer();
        switch (position) {
            case 0:
                replaceFragment(new Appointment_Fragment(), "Appointment");
                break;

            case 1:
                replaceFragment(new MyProfile(),"MyProfile");
                break;

            case 2:
                replaceFragment(new Logout_Fragment(), "Logout_Fragment");
                break;
        }
    }

    // Replace fragment method
    void replaceFragment(Fragment fragment, String tag) {

        // First find the fragment by TAG and if it null then replace the
        // fragment else do nothing
        Fragment fr = fragment_manager.findFragmentByTag(tag);
        if (fr == null) {
            fragment_manager.beginTransaction()
                    .replace(R.id.frame_container, fragment, tag).commit();
        }

    }

    // close the open drawer
    void closeDrawer() {
        if (drawer.isDrawerOpen(left_slider)) {
            drawer.closeDrawer(left_slider);
        }
    }

    @Override
    public void onBackPressed() {

        // Call whenBackpressed method to do task
        whenBackPressed();
    }

    // Method to be execute on back pressed
    void whenBackPressed() {
        Fragment fr = fragment_manager.findFragmentByTag("Home");
        // First close the drawer if open
        if (drawer.isDrawerOpen(left_slider)) {
            drawer.closeDrawer(left_slider);

        }
        // else replace the home fragment if TAG is null
        else {
            if (fr == null) {
                selectItem(0);
            }

            // finally finish activity
            else {

                if (exit) {
                    finish(); // finish activity
                } else {
                    Toast.makeText(this, "Press Back again to Exit.", Toast.LENGTH_SHORT).show();
                    exit = true;
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            exit = false;
                        }
                    }, 3 * 1000);
                }
            }
        }

    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync state for actionbar toggle
        actionbarToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        actionbarToggle.onConfigurationChanged(newConfig);
    }
}
