package codeplateau.com.easyprescribe.Fragment;


import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import codeplateau.com.easyprescribe.Activity.Add_New_PatientDetail;
import codeplateau.com.easyprescribe.Activity.Add_Old_PatientsDetail;
import codeplateau.com.easyprescribe.Activity.Write_Prescription;
import codeplateau.com.easyprescribe.Services.RestInterface;
import codeplateau.com.easyprescribe.Model.Appointement_model;
import codeplateau.com.easyprescribe.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class Appointment_Fragment extends Fragment {

    private ProgressDialog pDialog;
    private static View view;

    private static final String TAG = Appointment_Fragment.class.getSimpleName().toString();

    Appointement_Adapter appointement_adapter;

    ArrayList<Appointement_model> appointement_today_modelArrayList;
    ArrayList<Appointement_model> appointement_yesterday_modelArrayList;
    ArrayList<Appointement_model> appointement_old_modelArrayList;

    SharedPreferences pref;
    private static LinearLayoutManager mLayoutManager_Today, mLayoutManager_Yesterday, mLayoutManager_Old;

    private static RecyclerView rc_today_patient_list, rc_yesterday_patient_list, rc_old_patient_list;
    private static LinearLayout ll_today_no_record, ll_yesterday_no_record, ll_old_no_record;

    private static FloatingActionButton fab_addrecord;

    private String user_id,id;

    public Appointment_Fragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.appointment_list, container, false);

        initViews();
        setListeners();
        return view;
    }

    private void initViews() {

        pref = getActivity().getSharedPreferences(RestInterface.pref, Context.MODE_PRIVATE);
        user_id = pref.getString("user_id", "");

        rc_today_patient_list = (RecyclerView) view.findViewById(R.id.rc_today_patient_list);
        rc_yesterday_patient_list = (RecyclerView) view.findViewById(R.id.rc_yesterday_patient_list);
        rc_old_patient_list = (RecyclerView) view.findViewById(R.id.rc_old_patient_list);

        ll_today_no_record = (LinearLayout) view.findViewById(R.id.ll_today_no_record);
        ll_yesterday_no_record = (LinearLayout) view.findViewById(R.id.ll_yesterday_no_record);
        ll_old_no_record = (LinearLayout) view.findViewById(R.id.ll_old_no_record);

        fab_addrecord = (FloatingActionButton) view.findViewById(R.id.fab_addrecord);
    }

    private void setListeners() {

        mLayoutManager_Today = new LinearLayoutManager(getContext());
        rc_today_patient_list.setHasFixedSize(false);
        mLayoutManager_Today.setOrientation(LinearLayoutManager.VERTICAL);
        rc_today_patient_list.setLayoutManager(mLayoutManager_Today);

        mLayoutManager_Yesterday = new LinearLayoutManager(getContext());
        rc_yesterday_patient_list.setHasFixedSize(false);
        mLayoutManager_Yesterday.setOrientation(LinearLayoutManager.VERTICAL);
        rc_yesterday_patient_list.setLayoutManager(mLayoutManager_Yesterday);

        mLayoutManager_Old = new LinearLayoutManager(getContext());
        rc_old_patient_list.setHasFixedSize(false);
        mLayoutManager_Old.setOrientation(LinearLayoutManager.VERTICAL);
        rc_old_patient_list.setLayoutManager(mLayoutManager_Old);

        AppointmentList_API();

        fab_addrecord.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Appointment_Fragment.ViewDialog alert = new Appointment_Fragment.ViewDialog();
                alert.showDialog(Appointment_Fragment.this);
            }
        });
    }

    private void AppointmentList_API() {

        showpDialog();
        StringRequest strReq = new StringRequest(Request.Method.POST,
                RestInterface.URL_AppointmentList, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Response : " + response);

                hidepDialog();

                try {

                    JSONObject jObj = new JSONObject(response);
                    String success = jObj.getString("success");

                    if (success.equals("1")) {

                        appointement_today_modelArrayList = new ArrayList<Appointement_model>();
                        appointement_today_modelArrayList.clear();

                        JSONArray jsonArray_today = jObj.getJSONArray("today");

                        if (jsonArray_today.equals("[]")) {

                            ll_today_no_record.setVisibility(View.VISIBLE);
                            rc_today_patient_list.setVisibility(View.GONE);

                        } else {

                            for (int i = 0; i < jsonArray_today.length(); i++) {

                                JSONObject req = jsonArray_today.getJSONObject(i);

                                Appointement_model appointement_model = new Appointement_model();

                                appointement_model.setId(req.optString("id"));
                                appointement_model.setName(req.optString("name"));
                                appointement_model.setPhone(req.optString("phone"));
                                appointement_model.setDateofbirth(req.optString("dateofbirth"));
                                appointement_model.setBloodgrp(req.optString("bloodgrp"));
                                appointement_model.setGender(req.optString("gender"));
                                appointement_model.setHeight(req.optString("height"));
                                appointement_model.setWeight(req.optString("weight"));
                                appointement_model.setDescription(req.optString("description"));
                                appointement_model.setCreated_date(req.optString("created_date"));

                                appointement_today_modelArrayList.add(appointement_model);
                            }

                            if (!appointement_today_modelArrayList.isEmpty()) {

                                rc_today_patient_list.setVisibility(View.VISIBLE);
                                ll_today_no_record.setVisibility(View.GONE);

                                appointement_adapter = new Appointement_Adapter(getContext(), appointement_today_modelArrayList);
                                rc_today_patient_list.setAdapter(appointement_adapter);

                            } else {

                                ll_today_no_record.setVisibility(View.VISIBLE);
                                rc_today_patient_list.setVisibility(View.GONE);
                            }
                        }

                        appointement_yesterday_modelArrayList = new ArrayList<Appointement_model>();
                        appointement_yesterday_modelArrayList.clear();

                        JSONArray jsonArray_yesterday = jObj.getJSONArray("yesterday");

                        if (jsonArray_yesterday.equals("[]")) {

                            ll_yesterday_no_record.setVisibility(View.VISIBLE);
                            rc_yesterday_patient_list.setVisibility(View.GONE);

                        } else {

                            for (int i = 0; i < jsonArray_yesterday.length(); i++) {

                                JSONObject req = jsonArray_yesterday.getJSONObject(i);

                                Appointement_model appointement_model = new Appointement_model();

                                appointement_model.setId(req.optString("id"));
                                appointement_model.setName(req.optString("name"));
                                appointement_model.setPhone(req.optString("phone"));
                                appointement_model.setDateofbirth(req.optString("dateofbirth"));
                                appointement_model.setBloodgrp(req.optString("bloodgrp"));
                                appointement_model.setGender(req.optString("gender"));
                                appointement_model.setHeight(req.optString("height"));
                                appointement_model.setWeight(req.optString("weight"));
                                appointement_model.setDescription(req.optString("description"));
                                appointement_model.setCreated_date(req.optString("created_date"));

                                appointement_yesterday_modelArrayList.add(appointement_model);

                            }

                            if (!appointement_yesterday_modelArrayList.isEmpty()) {

                                rc_yesterday_patient_list.setVisibility(View.VISIBLE);
                                ll_yesterday_no_record.setVisibility(View.GONE);

                                appointement_adapter = new Appointement_Adapter(getContext(), appointement_yesterday_modelArrayList);
                                rc_yesterday_patient_list.setAdapter(appointement_adapter);

                            } else {

                                ll_yesterday_no_record.setVisibility(View.VISIBLE);
                                rc_yesterday_patient_list.setVisibility(View.GONE);
                            }
                        }

                        appointement_old_modelArrayList = new ArrayList<Appointement_model>();
                        appointement_old_modelArrayList.clear();

                        JSONArray jsonArray_old = jObj.getJSONArray("old");

                        if (jsonArray_old.equals("[]")) {

                            ll_old_no_record.setVisibility(View.VISIBLE);
                            rc_old_patient_list.setVisibility(View.GONE);

                        } else {

                            for (int i = 0; i < jsonArray_old.length(); i++) {

                                JSONObject req = jsonArray_old.getJSONObject(i);

                                Appointement_model appointement_model = new Appointement_model();

                                appointement_model.setId(req.optString("id"));
                                appointement_model.setName(req.optString("name"));
                                appointement_model.setPhone(req.optString("phone"));
                                appointement_model.setDateofbirth(req.optString("dateofbirth"));
                                appointement_model.setBloodgrp(req.optString("bloodgrp"));
                                appointement_model.setGender(req.optString("gender"));
                                appointement_model.setHeight(req.optString("height"));
                                appointement_model.setWeight(req.optString("weight"));
                                appointement_model.setDescription(req.optString("description"));
                                appointement_model.setCreated_date(req.optString("created_date"));

                                appointement_old_modelArrayList.add(appointement_model);
                            }

                            if (!appointement_old_modelArrayList.isEmpty()) {

                                rc_old_patient_list.setVisibility(View.VISIBLE);
                                ll_old_no_record.setVisibility(View.GONE);

                                appointement_adapter = new Appointement_Adapter(getContext(), appointement_old_modelArrayList);
                                rc_old_patient_list.setAdapter(appointement_adapter);

                            } else {

                                ll_old_no_record.setVisibility(View.VISIBLE);
                                rc_old_patient_list.setVisibility(View.GONE);
                            }
                        }

                    } else {
                        hidepDialog();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Error: " + error.getMessage());
                //Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
                hidepDialog();
            }
        }) {

            @Override
            protected java.util.Map<String, String> getParams() {
                // Posting params to register url
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", user_id);
                return params;
            }
        };

        strReq.setRetryPolicy(new DefaultRetryPolicy(
                5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // Adding request to request queue
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        strReq.setShouldCache(false);
        requestQueue.add(strReq);
    }

    private void showpDialog() {

        // Progress Dialog
        pDialog = new ProgressDialog(getContext());
        pDialog.setMessage("Please wait...");
        pDialog.setCancelable(false);

        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hidepDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    public class ViewDialog {

        public void showDialog(Appointment_Fragment activity) {
            final Dialog dialog = new Dialog(getContext());
            dialog.setCanceledOnTouchOutside(true);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(false);
            dialog.setContentView(R.layout.dialog_patient_category);

            TextView lbl_New_Patient = (TextView) dialog.findViewById(R.id.lbl_New_Patient);
            lbl_New_Patient.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent_Add_New_PatientDetail = new Intent(getContext(), Add_New_PatientDetail.class);
                    startActivity(intent_Add_New_PatientDetail);
                    dialog.dismiss();
                }
            });

            TextView lbl_Old_Patient = (TextView) dialog.findViewById(R.id.lbl_Old_Patient);
            lbl_Old_Patient.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent_Add_Old_PatientsDetail = new Intent(getContext(), Add_Old_PatientsDetail.class);
                    startActivity(intent_Add_Old_PatientsDetail);
                    dialog.dismiss();
                }
            });

            ImageView img_close = (ImageView) dialog.findViewById(R.id.img_close);
            img_close.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    dialog.dismiss();
                }
            });

            dialog.show();
        }
    }

    public static class Appointement_Adapter extends RecyclerView.Adapter<Appointement_Adapter.ViewHolder> {
        private ArrayList<Appointement_model> appointement_modelArrayList;
        private Context context;
        public String TAG = Appointement_Adapter.class.getSimpleName();
        public String email;

        public Appointement_Adapter(Context context, ArrayList<Appointement_model> appointement_models) {
            this.appointement_modelArrayList = appointement_models;
            this.context = context;
        }

        @Override
        public Appointement_Adapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {

            View view;
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_appointment_list, viewGroup, false);

            return new Appointement_Adapter.ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(final Appointement_Adapter.ViewHolder viewHolder, final int i) {

            String title = appointement_modelArrayList.get(i).getName();
            String first_letter = String.valueOf(title.charAt(0)).toUpperCase();

            Random rnd = new Random();
            int color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));

            TextDrawable drawable = TextDrawable.builder().buildRound(first_letter, color);
            viewHolder.img_patient_pic.setImageDrawable(drawable);

            viewHolder.lbl_patient_name.setText(appointement_modelArrayList.get(i).getName());

            viewHolder.ll_patient_detail.setOnClickListener(new View.OnClickListener() {

                String appointment_id = appointement_modelArrayList.get(i).getId();
                String user_id = appointement_modelArrayList.get(i).getId();
                String name = appointement_modelArrayList.get(i).getName();
                String phone = appointement_modelArrayList.get(i).getPhone();
                String dateofbirth = appointement_modelArrayList.get(i).getDateofbirth();
                String bloodgrp = appointement_modelArrayList.get(i).getBloodgrp();
                String gender = appointement_modelArrayList.get(i).getGender();
                String height = appointement_modelArrayList.get(i).getHeight();
                String weight = appointement_modelArrayList.get(i).getWeight();
                String description = appointement_modelArrayList.get(i).getDescription();
                String created_date = appointement_modelArrayList.get(i).getCreated_date();
                String med_id = appointement_modelArrayList.get(i).getId();
                String patient_id = appointement_modelArrayList.get(i).getId();
                String id= appointement_modelArrayList.get(i).getId();
                @Override
                public void onClick(View v) {

                    Intent intent_Write_Prescription = new Intent(context.getApplicationContext(), Write_Prescription.class);
                    intent_Write_Prescription.putExtra("appointment_id", appointment_id);
                    intent_Write_Prescription.putExtra("user_id", user_id);
                    intent_Write_Prescription.putExtra("name", name);
                    intent_Write_Prescription.putExtra("phone", phone);
                    intent_Write_Prescription.putExtra("dateofbirth", dateofbirth);
                    intent_Write_Prescription.putExtra("bloodgrp", bloodgrp);
                    intent_Write_Prescription.putExtra("gender", gender);
                    intent_Write_Prescription.putExtra("height", height);
                    intent_Write_Prescription.putExtra("weight", weight);
                    intent_Write_Prescription.putExtra("description", description);
                    intent_Write_Prescription.putExtra("created_date", created_date);
                    intent_Write_Prescription.putExtra("med_id",med_id);
                    intent_Write_Prescription.putExtra("patient_id",patient_id);
                    intent_Write_Prescription.putExtra("id",id);

                    context.startActivity(intent_Write_Prescription);
                }
            });

            /*viewHolder.img_patient_status.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick (View v) {

                    Intent intent_MedicineList = new Intent(context.getApplicationContext(), PrintActivity.class);
                    intent_MedicineList.putExtra("id", "2");
                    context.startActivity(intent_MedicineList);
                }
            });*/
        }

        @Override
        public int getItemCount() {

            return appointement_modelArrayList.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {

            TextView lbl_patient_name;
            LinearLayout ll_patient_detail;
            ImageView img_patient_pic, img_edit_patient_detail, img_patient_status;

            public ViewHolder(View view) {
                super(view);

                lbl_patient_name = (TextView) view.findViewById(R.id.lbl_patient_name);

                img_patient_pic = (ImageView) view.findViewById(R.id.img_patient_pic);
                img_edit_patient_detail = (ImageView) view.findViewById(R.id.img_edit_patient_detail);
                img_patient_status = (ImageView) view.findViewById(R.id.img_patient_status);
                ll_patient_detail = (LinearLayout) view.findViewById(R.id.ll_patient_detail);
            }
        }
    }
}
