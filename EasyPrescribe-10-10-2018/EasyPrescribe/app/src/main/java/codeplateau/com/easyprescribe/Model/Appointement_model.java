package codeplateau.com.easyprescribe.Model;

public class Appointement_model {

    String id, name, phone, dateofbirth, bloodgrp, gender, height, weight, description, created_date;

    public Appointement_model(){};

    public String getId () {
        return id;
    }

    public void setId (String id) {
        this.id = id;
    }

    public String getDateofbirth () {
        return dateofbirth;
    }

    public void setDateofbirth (String dateofbirth) {
        this.dateofbirth = dateofbirth;
    }

    public String getBloodgrp () {
        return bloodgrp;
    }

    public void setBloodgrp (String bloodgrp) {
        this.bloodgrp = bloodgrp;
    }

    public String getGender () {
        return gender;
    }

    public void setGender (String gender) {
        this.gender = gender;
    }

    public String getHeight () {
        return height;
    }

    public void setHeight (String height) {
        this.height = height;
    }

    public String getWeight () {
        return weight;
    }

    public void setWeight (String weight) {
        this.weight = weight;
    }

    public String getDescription () {
        return description;
    }

    public void setDescription (String description) {
        this.description = description;
    }

    public String getCreated_date () {
        return created_date;
    }

    public void setCreated_date (String created_date) {
        this.created_date = created_date;
    }

    public String getPhone () {
        return phone;
    }

    public void setPhone (String phone) {
        this.phone = phone;
    }

    public String getName () {
        return name;
    }

    public void setName (String name) {
        this.name = name;
    }
}


