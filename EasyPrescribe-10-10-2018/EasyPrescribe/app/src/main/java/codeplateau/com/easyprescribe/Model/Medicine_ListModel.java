package codeplateau.com.easyprescribe.Model;

public class Medicine_ListModel {

    String id,med_id,patient_id,appointment_id,dose,instruction,duration,directions,created_date,updated_date,medicine,description,days;

    public String getId () {
        return id;
    }

    public void setId (String id) {
        this.id = id;
    }

    public String getMed_id () {
        return med_id;
    }

    public void setMed_id (String med_id) {
        this.med_id = med_id;
    }

    public String getPatient_id () {
        return patient_id;
    }

    public void setPatient_id (String patient_id) {
        this.patient_id = patient_id;
    }

    public String getAppointment_id () {
        return appointment_id;
    }

    public void setAppointment_id (String appointment_id) {
        this.appointment_id = appointment_id;
    }

    public String getDose () {
        return dose;
    }

    public void setDose (String dose) {
        this.dose = dose;
    }
    public String getInstruction () {
        return instruction;
    }

    public void setInstruction (String instruction) {
        this.instruction = instruction;
    }

    public String getDuration () {
        return duration;
    }

    public void setDuration (String duration) {
        this.duration = duration;
    }

    public String getDirections () {
        return directions;
    }

    public void setDirections (String directions) {
        this.directions = directions;
    }

    public String getCreated_date () {
        return created_date;
    }

    public void setCreated_date (String created_date) {
        this.created_date = created_date;
    }

    public String getUpdated_date () {
        return updated_date;
    }

    public void setUpdated_date (String updated_date) {
        this.updated_date = updated_date;
    }

    public String getMedicine () {
        return medicine;
    }

    public void setMedicine (String medicine) {
        this.medicine = medicine;
    }

    public String getDescription () {
        return description;
    }

    public void setDescription (String description) {
        this.description = description;
    }

    public String getDays () {
        return days;
    }

    public void setDays (String days) {
        this.days = days;
    }
}
