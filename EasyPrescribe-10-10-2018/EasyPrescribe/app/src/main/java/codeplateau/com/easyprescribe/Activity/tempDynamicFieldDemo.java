package codeplateau.com.easyprescribe.Activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import codeplateau.com.easyprescribe.Model.Medicine_NameModel;
import codeplateau.com.easyprescribe.R;
import codeplateau.com.easyprescribe.Services.RestInterface;

public class tempDynamicFieldDemo extends AppCompatActivity {

    String TAG = tempDynamicFieldDemo.class.getSimpleName();

    Button btnAdd;
    Button btnSubmit;
    LinearLayout container;
    ProgressDialog pDialog;

    AutoCompleteTextView txt_item_name;
    RadioGroup rd_dose;
    RadioGroup rd_duration;
    RadioGroup rd_instruction;
    RadioGroup rd_days;
    EditText txt_distribution;

    Button btn_medicinecancel;

    ArrayList<AutoCompleteTextView> list_txt_item_name = new ArrayList<>();
    ArrayList<RadioGroup> list_rd_dose = new ArrayList<>();
    ArrayList<RadioGroup> list_rd_duration = new ArrayList<>();
    ArrayList<RadioGroup> list_rd_instruction = new ArrayList<>();
    ArrayList<RadioGroup> list_rd_days = new ArrayList<>();
    ArrayList<EditText> list_txt_distribution = new ArrayList<>();

    ArrayList<Button> list_btn_medicinecancel = new ArrayList<>();

    private static String TAG_MEDICINE_ID = "id";
    private static String TAG_MEDICINE_NAME = "name";

    final static ArrayList<Medicine_NameModel> medicine_nameModelArrayList = new ArrayList<Medicine_NameModel>();
    List<String> responseList = new ArrayList<String>();
    ArrayAdapter adapter;
    SharedPreferences pref;

    String user_id;
    String appointment_id;

    int count = 0;
    int i = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_temp_dynamic_field_demo);

        pref = getApplicationContext().getSharedPreferences(RestInterface.pref, Context.MODE_PRIVATE);
        user_id = pref.getString("user_id", "");

        Bundle extras = getIntent().getExtras();
        if (extras != null) {

            appointment_id = extras.getString("appointment_id");
        }

        btnAdd = (Button) findViewById(R.id.btnAddField);
        btnSubmit = (Button) findViewById(R.id.btnSubmit);
        container = (LinearLayout) findViewById(R.id.container);

        MedicineName_List_API();

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AddField();
            }
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                StringBuilder strItemName = new StringBuilder();
                StringBuilder strMedicineId = new StringBuilder();
                StringBuilder strDose = new StringBuilder();
                StringBuilder strDuration = new StringBuilder();
                StringBuilder strInstruction = new StringBuilder();
                StringBuilder strDays = new StringBuilder();
                StringBuilder strDistribution = new StringBuilder();

                // Medicine Id
                for (int i = 0; i < list_txt_item_name.size(); i++) {

                    // Medicine Id
                    for (int j = 0; j < medicine_nameModelArrayList.size(); j++) {

                        String item_name = list_txt_item_name.get(i).getText().toString();

                        if (item_name.equals(medicine_nameModelArrayList.get(j).getName())) {

                            strMedicineId.append(medicine_nameModelArrayList.get(j).getId());
                            strMedicineId.append(",");
                        }
                    }
                }

                String medicineId = strMedicineId.toString();
                medicineId = medicineId.replaceAll(",$", "");

                // Dose
                if (list_rd_dose.size() > 0) {

                    for (int i = 0; i < list_rd_dose.size(); i++) {

                        int selectedId = list_rd_dose.get(i).getCheckedRadioButtonId();
                        RadioButton radioButton = (RadioButton) findViewById(selectedId);
                        strDose.append(radioButton.getText());
                        strDose.append(",");
                    }
                }
                String dose = strDose.toString();
                dose = dose.replaceAll(",$", "");

                // Duration
                if (list_rd_duration.size() > 0) {

                    for (int i = 0; i < list_rd_duration.size(); i++) {

                        int selectedId = list_rd_duration.get(i).getCheckedRadioButtonId();
                        RadioButton radioButton = (RadioButton) findViewById(selectedId);
                        strDuration.append(radioButton.getText());
                        strDuration.append(",");
                    }
                }
                String duration = strDuration.toString();
                duration = duration.replaceAll(",$", "");

                // Instruction
                if (list_rd_instruction.size() > 0) {

                    for (int i = 0; i < list_rd_instruction.size(); i++) {

                        int selectedId = list_rd_instruction.get(i).getCheckedRadioButtonId();
                        RadioButton radioButton = (RadioButton) findViewById(selectedId);
                        strInstruction.append(radioButton.getText());
                        strInstruction.append(",");
                    }
                }
                String instruction = strInstruction.toString();
                instruction = instruction.replaceAll(",$", "");

                // Days
                if (list_rd_days.size() > 0) {

                    for (int i = 0; i < list_rd_days.size(); i++) {

                        int selectedId = list_rd_days.get(i).getCheckedRadioButtonId();
                        RadioButton radioButton = (RadioButton) findViewById(selectedId);
                        strDays.append(radioButton.getText());
                        strDays.append(",");
                    }
                }
                String days = strDays.toString();
                days = days.replaceAll(",$", "");

                // Distribution
                if (list_txt_distribution.size() > 0) {

                    for (int i = 0; i < list_txt_distribution.size(); i++) {

                        strDistribution.append(list_txt_distribution.get(i).getText().toString());
                        strDistribution.append(",");
                    }
                }
                String distribution = strDistribution.toString();
                distribution = distribution.replaceAll(",$", "");

                AddPrescriptionCodeAPI(medicineId, instruction, dose, duration, days, distribution);
            }
        });
    }

    public void AddField() {

        LayoutInflater layoutInflater = (LayoutInflater) getApplicationContext().getSystemService(getApplicationContext().LAYOUT_INFLATER_SERVICE);
        final View addView = layoutInflater.inflate(R.layout.field, null);

        txt_item_name = (AutoCompleteTextView) addView.findViewById(R.id.txt_item_name);
        rd_dose = (RadioGroup) addView.findViewById(R.id.rd_dose);
        rd_duration = (RadioGroup) addView.findViewById(R.id.rd_Duration);
        rd_instruction = (RadioGroup) addView.findViewById(R.id.rd_instruction);
        rd_days = (RadioGroup) addView.findViewById(R.id.rd_days);
        txt_distribution = (EditText) addView.findViewById(R.id.txt_distribution);

        btn_medicinecancel = (Button) addView.findViewById(R.id.btn_medicinecancel);

        list_txt_item_name.add(txt_item_name);
        list_rd_dose.add(rd_dose);
        list_rd_duration.add(rd_duration);
        list_rd_instruction.add(rd_instruction);
        list_rd_days.add(rd_days);
        list_txt_distribution.add(txt_distribution);
        list_btn_medicinecancel.add(btn_medicinecancel);

        if (!medicine_nameModelArrayList.isEmpty()) {

            txt_item_name.setThreshold(1);
            txt_item_name.setAdapter(adapter);
        }

        btn_medicinecancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                list_txt_item_name.remove(txt_item_name);
                list_rd_dose.remove(rd_dose);
                list_rd_duration.remove(rd_duration);
                list_rd_instruction.remove(rd_instruction);
                list_rd_days.remove(rd_days);
                list_txt_distribution.remove(txt_distribution);

                container.removeView(addView);
            }
        });
        container.addView(addView);
    }

    public void AddField1() {

        for (i = count; i <= count; i++) {

            LayoutInflater layoutInflater = (LayoutInflater) getApplicationContext().getSystemService(getApplicationContext().LAYOUT_INFLATER_SERVICE);
            final View addView = layoutInflater.inflate(R.layout.field, null);

            txt_item_name = (AutoCompleteTextView) addView.findViewById(R.id.txt_item_name);
            rd_dose = (RadioGroup) addView.findViewById(R.id.rd_dose);
            rd_duration = (RadioGroup) addView.findViewById(R.id.rd_Duration);
            rd_instruction = (RadioGroup) addView.findViewById(R.id.rd_instruction);
            rd_days = (RadioGroup) addView.findViewById(R.id.rd_days);
            txt_distribution = (EditText) addView.findViewById(R.id.txt_distribution);

            btn_medicinecancel = (Button) addView.findViewById(R.id.btn_medicinecancel);

            list_txt_item_name.add(i, txt_item_name);
            list_rd_dose.add(i, rd_dose);
            list_btn_medicinecancel.add(i, btn_medicinecancel);

            btn_medicinecancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Log.d("Index :", String.valueOf(i));

                    list_txt_item_name.remove(txt_item_name);
                    list_rd_dose.remove(rd_dose);

                    container.removeView(addView);
                    count = count - 1;
                }
            });

            container.addView(addView);
        }
        count = count + 1;
    }

    private void MedicineName_List_API() {
        // Tag used to cancel the request
        showpDialog();
        StringRequest strReq = new StringRequest(Request.Method.POST, RestInterface.URL_MedicineName, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Response: " + response);
                hidepDialog();
                try {
                    JSONObject jObj = new JSONObject(response);
                    String success = jObj.getString("success");

                    if (success.equals("1")) {

                        JSONArray requestarray = jObj.getJSONArray("data");

                        if (requestarray.equals("[]")) {

                            Toast.makeText(tempDynamicFieldDemo.this, "Not Any Medicine Name Found!", Toast.LENGTH_SHORT).show();

                        } else {

                            medicine_nameModelArrayList.clear();
                            responseList = new ArrayList<String>();

                            for (int i = 0; i < requestarray.length(); i++) {

                                Medicine_NameModel medicine_nameModels = new Medicine_NameModel();
                                JSONObject req = requestarray.getJSONObject(i);

                                medicine_nameModels.setId(req.optString(TAG_MEDICINE_ID));
                                medicine_nameModels.setName(req.optString(TAG_MEDICINE_NAME));

                                medicine_nameModelArrayList.add(medicine_nameModels);
                                responseList.add(req.optString(TAG_MEDICINE_NAME));
                            }

                            adapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_dropdown_item_1line, responseList);
                        }

                    } else {
                        hidepDialog();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
                hidepDialog();
            }
        }) {

            @Override
            protected java.util.Map<String, String> getParams() {
                // Posting params to register url
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", user_id);
                return params;
            }
        };

        strReq.setRetryPolicy(new DefaultRetryPolicy(5000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // Adding request to request queue
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        strReq.setShouldCache(false);
        requestQueue.add(strReq);
    }

    private void AddPrescriptionCodeAPI(final String strMedicineId, final String strInstruction, final String strDose, final String strDuration, final String strDays, final String strDistribution) {

        showpDialog();

        StringRequest strReq = new StringRequest(Request.Method.POST, RestInterface.URL_AddPrescription, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Login Response: " + response);
                hidepDialog();
                try {
                    JSONObject jObj = new JSONObject(response);
                    String success = jObj.getString("success");

                    if (success.equals("1")) {

                        String message = jObj.getString("message");
                        Dialog_Success(message);

                    } else {

                        String message = jObj.getString("message");
                        Dialog_Failed(message);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Error: " + error.getMessage());
                hidepDialog();
            }
        }) {
            @Override
            protected java.util.Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("med_id", strMedicineId);
                params.put("instruction", strInstruction);
                params.put("dose", strDose);
                params.put("unit", "");
                params.put("direction", "");
                params.put("duration", strDuration);
                params.put("user_id", user_id);
                params.put("appointment_id", appointment_id);
                params.put("patient_id", user_id);
                params.put("days", strDays);
                params.put("description", strDistribution);
                return params;
            }
        };

        strReq.setRetryPolicy(new DefaultRetryPolicy(5000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // Adding request to request queue
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        strReq.setShouldCache(false);
        requestQueue.add(strReq);
    }

    private void showpDialog() {

        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Please wait...");
        pDialog.setCancelable(false);

        if (!pDialog.isShowing()) pDialog.show();
    }

    private void hidepDialog() {
        if (pDialog.isShowing()) pDialog.dismiss();
    }

    private void Dialog_Success(String value) {

        // dailog_alert dialog
        final Dialog dialog = new Dialog(tempDynamicFieldDemo.this);
        dialog.setContentView(R.layout.dailog_box);

        // set the dailog_alert dialog components - text, image and button
        ImageView image = (ImageView) dialog.findViewById(R.id.image);
        image.setImageDrawable(getResources().getDrawable(R.drawable.donemark));
        image.setMaxHeight(50);
        image.setMaxWidth(50);

        TextView tv_title = (TextView) dialog.findViewById(R.id.tv_title);
        tv_title.setText("Add Medicine");

        TextView text = (TextView) dialog.findViewById(R.id.tv_content);
        text.setText(value);

        Button dialogButton = (Button) dialog.findViewById(R.id.dialogButtonOK);
        // if button is clicked, close the dailog_alert dialog
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();

                Intent intent_Write_Prescription = new Intent(getApplicationContext(), Write_Prescription.class);
                intent_Write_Prescription.putExtra("appointment_id", appointment_id);
                startActivity(intent_Write_Prescription);
            }
        });

        dialog.show();
    }

    private void Dialog_Failed(String value) {
        // dailog_alert dialog
        final Dialog dialog = new Dialog(tempDynamicFieldDemo.this);
        dialog.setContentView(R.layout.dailog_box);

        // set the dailog_alert dialog components - text, image and button
        ImageView image = (ImageView) dialog.findViewById(R.id.image);
        image.setImageDrawable(getResources().getDrawable(R.drawable.ic_error_red_24dp));
        image.setMaxHeight(50);
        image.setMaxWidth(50);

        TextView tv_title = (TextView) dialog.findViewById(R.id.tv_title);
        tv_title.setText("Add Medicine");

        TextView text = (TextView) dialog.findViewById(R.id.tv_content);
        text.setText(value);

        Button dialogButton = (Button) dialog.findViewById(R.id.dialogButtonOK);
        // if button is clicked, close the dailog_alert dialog
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });

        dialog.show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        Intent intent_Write_Prescription = new Intent(getApplicationContext(), Write_Prescription.class);
        intent_Write_Prescription.putExtra("appointment_id", appointment_id);
        startActivity(intent_Write_Prescription);
    }
}
