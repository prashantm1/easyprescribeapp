package codeplateau.com.easyprescribe.Services;

/**
 * Created by Eminence Solution on 22-10-2016.
 */

public class Utils {

    //Email Validation pattern
    public static final String regEx = "\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+.[A-Za-z]{2,4}\b";

    //login
    public static final String Login_Fragment = "Login_Fragment";
    public static final String Registration = "Registration";
}
