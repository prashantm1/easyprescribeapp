package codeplateau.com.easyprescribe.Activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;

import codeplateau.com.easyprescribe.R;
import codeplateau.com.easyprescribe.Services.RestInterface;

public class SplashScreen extends Activity {

    SharedPreferences sharedPreferences;
    String email = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen);

        sharedPreferences = getApplicationContext().getSharedPreferences(RestInterface.pref, Context.MODE_PRIVATE);
        email = sharedPreferences.getString("email", "");

        new Handler().postDelayed(new Runnable() {
            /*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */
            @Override
            public void run() {
                // This method will be executed once the timer is over
                // Start your app main activity

                if(email.equals("")){

                    Intent intent_MainScreen = new Intent(SplashScreen.this, MainScreen.class);
                    startActivity(intent_MainScreen);

                } else {

                    Intent intent_DrawerLayoutActivity= new Intent(SplashScreen.this, DrawerLayoutActivity.class);
                    startActivity(intent_DrawerLayoutActivity);
                }
                // close this activity
                finish();
            }
        }, 3000);

    }
}