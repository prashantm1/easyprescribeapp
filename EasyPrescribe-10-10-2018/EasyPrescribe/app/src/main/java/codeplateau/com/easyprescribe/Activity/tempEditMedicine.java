package codeplateau.com.easyprescribe.Activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import codeplateau.com.easyprescribe.Model.Medicine_ListModel;
import codeplateau.com.easyprescribe.Model.Medicine_NameModel;
import codeplateau.com.easyprescribe.R;
import codeplateau.com.easyprescribe.Services.RestInterface;

public class tempEditMedicine extends AppCompatActivity {

    String TAG = tempEditMedicine.class.getSimpleName();

    private static Button btnAdd;
    private static Button btnSubmit;
    private static LinearLayout container;
    ProgressDialog pDialog;

    private static AutoCompleteTextView txt_item_name;
    private static RadioGroup rd_dose;
    private static RadioGroup rd_duration;
    private static RadioGroup rd_instruction;
    private static RadioGroup rd_days;
    private static EditText txt_distribution;

    private static Button btn_medicinecancel;

    ArrayList<AutoCompleteTextView> list_txt_item_name = new ArrayList<>();
    ArrayList<RadioGroup> list_rd_dose = new ArrayList<>();
    ArrayList<RadioGroup> list_rd_duration = new ArrayList<>();
    ArrayList<RadioGroup> list_rd_instruction = new ArrayList<>();
    ArrayList<RadioGroup> list_rd_days = new ArrayList<>();
    ArrayList<EditText> list_txt_distribution = new ArrayList<>();

    ArrayList<Button> list_btn_medicinecancel = new ArrayList<>();

    ArrayList<Medicine_ListModel> medicine_listModelArrayList = new ArrayList<>();

    private static String TAG_MEDICINE_ID = "id";
    private static String TAG_MEDICINE_NAME = "name";

    HashMap<String, String> map = new HashMap<>();

    final static ArrayList<Medicine_NameModel> medicine_nameModelArrayList = new ArrayList<Medicine_NameModel>();
    List<String> responseList = new ArrayList<String>();
    ArrayAdapter adapter;
    SharedPreferences pref;

    String user_id;
    String id, appointment_id;

    int i = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_temp_edit_medicine);

        pref = getApplicationContext().getSharedPreferences(RestInterface.pref, Context.MODE_PRIVATE);
        user_id = pref.getString("user_id", "");

        Bundle extras = getIntent().getExtras();
        if (extras != null) {

            id = extras.getString("id");
            appointment_id = extras.getString("appointment_id");
        }

        btnAdd = (Button) findViewById(R.id.btnAddField);
        btnSubmit = (Button) findViewById(R.id.btnSubmit);
        container = (LinearLayout) findViewById(R.id.container);

        MedicineName_List_API();

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AddField();
            }
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                StringBuilder strItemName = new StringBuilder();
                StringBuilder strMedicineId = new StringBuilder();
                StringBuilder strDose = new StringBuilder();
                StringBuilder strDuration = new StringBuilder();
                StringBuilder strInstruction = new StringBuilder();
                StringBuilder strDays = new StringBuilder();
                StringBuilder strDistribution = new StringBuilder();

                // Medicine Id
                for (int i = 0; i < list_txt_item_name.size(); i++) {

                    // Medicine Id
                    for (int j = 0; j < medicine_nameModelArrayList.size(); j++) {

                        String item_name = list_txt_item_name.get(i).getText().toString();

                        if (item_name.equals(medicine_nameModelArrayList.get(j).getName())) {

                            strMedicineId.append(medicine_nameModelArrayList.get(j).getId());
                            strMedicineId.append(",");
                        }
                    }
                }

                String medicineId = strMedicineId.toString();
                medicineId = medicineId.replaceAll(",$", "");

                // Dose
                if (list_rd_dose.size() > 0) {

                    for (int i = 0; i < list_rd_dose.size(); i++) {

                        int selectedId = list_rd_dose.get(i).getCheckedRadioButtonId();
                        RadioButton radioButton = (RadioButton) findViewById(selectedId);
                        strDose.append(radioButton.getText());
                        strDose.append(",");
                    }
                }
                String dose = strDose.toString();
                dose = dose.replaceAll(",$", "");

                // Duration
                if (list_rd_duration.size() > 0) {

                    for (int i = 0; i < list_rd_duration.size(); i++) {

                        int selectedId = list_rd_duration.get(i).getCheckedRadioButtonId();
                        RadioButton radioButton = (RadioButton) findViewById(selectedId);
                        strDuration.append(radioButton.getText());
                        strDuration.append(",");
                    }
                }
                String duration = strDuration.toString();
                duration = duration.replaceAll(",$", "");

                // Instruction
                if (list_rd_instruction.size() > 0) {

                    for (int i = 0; i < list_rd_instruction.size(); i++) {

                        int selectedId = list_rd_instruction.get(i).getCheckedRadioButtonId();
                        RadioButton radioButton = (RadioButton) findViewById(selectedId);
                        strInstruction.append(radioButton.getText());
                        strInstruction.append(",");
                    }
                }
                String instruction = strInstruction.toString();
                instruction = instruction.replaceAll(",$", "");

                // Days
                if (list_rd_days.size() > 0) {

                    for (int i = 0; i < list_rd_days.size(); i++) {

                        int selectedId = list_rd_days.get(i).getCheckedRadioButtonId();
                        RadioButton radioButton = (RadioButton) findViewById(selectedId);
                        strDays.append(radioButton.getText());
                        strDays.append(",");
                    }
                }
                String days = strDays.toString();
                days = days.replaceAll(",$", "");

                // Distribution
                if (list_txt_distribution.size() > 0) {

                    for (int i = 0; i < list_txt_distribution.size(); i++) {

                        strDistribution.append(list_txt_distribution.get(i).getText().toString());
                        strDistribution.append(",");
                    }
                }
                String distribution = strDistribution.toString();
                distribution = distribution.replaceAll(",$", "");

                AddPrescriptionCodeAPI(medicineId, instruction, dose, duration, days, distribution, id);
            }
        });
    }

    public void AddOldField(String med_id, String dose, String instruction, String duration, String days, String description) {

        LayoutInflater layoutInflater = (LayoutInflater) getApplicationContext().getSystemService(getApplicationContext().LAYOUT_INFLATER_SERVICE);
        final View addView = layoutInflater.inflate(R.layout.field, null);

        txt_item_name = (AutoCompleteTextView) addView.findViewById(R.id.txt_item_name);
        rd_dose = (RadioGroup) addView.findViewById(R.id.rd_dose);
        rd_duration = (RadioGroup) addView.findViewById(R.id.rd_Duration);
        rd_instruction = (RadioGroup) addView.findViewById(R.id.rd_instruction);
        rd_days = (RadioGroup) addView.findViewById(R.id.rd_days);
        txt_distribution = (EditText) addView.findViewById(R.id.txt_distribution);

        btn_medicinecancel = (Button) addView.findViewById(R.id.btn_medicinecancel);

        // Medicine Name
        String item_name = "";
        if (map.containsKey(med_id)) {
            item_name = map.get(med_id);
        }

        // Medicine Id
        for (int j = 0; j < medicine_nameModelArrayList.size(); j++) {

            if (item_name.equals(medicine_nameModelArrayList.get(j).getName())) {

                txt_item_name.setText(item_name);
            }
        }

        // Dose
        if (dose.equals("Tablet")) {

            rd_dose.check(R.id.radio_tablet);

        } else {

            rd_dose.check(R.id.radio_syrup);
        }

        int selectedId = rd_dose.getCheckedRadioButtonId();
        rd_dose.check(selectedId);

        // Duration
        if (duration.equals("1 times")) {

            rd_duration.check(R.id.radio_onetime);
        }

        if (duration.equals("2 times")) {

            rd_duration.check(R.id.radio_twotime);
        }

        if (duration.equals("3 times")) {

            rd_duration.check(R.id.radio_threetime);
        }

        int selectedId1 = rd_duration.getCheckedRadioButtonId();
        rd_duration.check(selectedId1);

        // Instruction
        if (instruction.equals("After food")) {

            rd_instruction.check(R.id.radio_afterfood);
        }

        if (instruction.equals("Before food")) {

            rd_instruction.check(R.id.radio_beforefood);
        }

        int selectedId2 = rd_instruction.getCheckedRadioButtonId();
        rd_instruction.check(selectedId2);

        // Days
        if (days.equals("1")) {

            rd_days.check(R.id.rd_one);
        }

        if (days.equals("3")) {

            rd_days.check(R.id.rd_three);
        }

        if (days.equals("5")) {

            rd_days.check(R.id.rd_five);
        }

        if (days.equals("7")) {

            rd_days.check(R.id.rd_seven);
        }

        if (days.equals("10")) {

            rd_days.check(R.id.rd_ten);
        }

        if (days.equals("12")) {

            rd_days.check(R.id.rd_twelve);
        }

        if (days.equals("15")) {

            rd_days.check(R.id.rd_fifteen);
        }

        int selectedId3 = rd_days.getCheckedRadioButtonId();
        rd_days.check(selectedId3);

        txt_distribution.setText(description);

        list_txt_item_name.add(txt_item_name);
        list_rd_dose.add(rd_dose);
        list_rd_duration.add(rd_duration);
        list_rd_instruction.add(rd_instruction);
        list_rd_days.add(rd_days);
        list_txt_distribution.add(txt_distribution);
        list_btn_medicinecancel.add(btn_medicinecancel);

        if (!medicine_nameModelArrayList.isEmpty()) {

            txt_item_name.setThreshold(1);
            txt_item_name.setAdapter(adapter);
        }

        btn_medicinecancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                list_txt_item_name.remove(txt_item_name);
                list_rd_dose.remove(rd_dose);
                list_rd_duration.remove(rd_duration);
                list_rd_instruction.remove(rd_instruction);
                list_rd_days.remove(rd_days);
                list_txt_distribution.remove(txt_distribution);

                container.removeView(addView);
            }
        });
        container.addView(addView);
    }

    public void AddField() {

        LayoutInflater layoutInflater = (LayoutInflater) getApplicationContext().getSystemService(getApplicationContext().LAYOUT_INFLATER_SERVICE);
        final View addView = layoutInflater.inflate(R.layout.field, null);

        txt_item_name = (AutoCompleteTextView) addView.findViewById(R.id.txt_item_name);
        rd_dose = (RadioGroup) addView.findViewById(R.id.rd_dose);
        rd_duration = (RadioGroup) addView.findViewById(R.id.rd_Duration);
        rd_instruction = (RadioGroup) addView.findViewById(R.id.rd_instruction);
        rd_days = (RadioGroup) addView.findViewById(R.id.rd_days);
        txt_distribution = (EditText) addView.findViewById(R.id.txt_distribution);

        btn_medicinecancel = (Button) addView.findViewById(R.id.btn_medicinecancel);

        list_txt_item_name.add(txt_item_name);
        list_rd_dose.add(rd_dose);
        list_rd_duration.add(rd_duration);
        list_rd_instruction.add(rd_instruction);
        list_rd_days.add(rd_days);
        list_txt_distribution.add(txt_distribution);
        list_btn_medicinecancel.add(btn_medicinecancel);


        if (!medicine_nameModelArrayList.isEmpty()) {

            txt_item_name.setThreshold(1);
            txt_item_name.setAdapter(adapter);
        }

        btn_medicinecancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                list_txt_item_name.remove(txt_item_name);
                list_rd_dose.remove(rd_dose);
                list_rd_duration.remove(rd_duration);
                list_rd_instruction.remove(rd_instruction);
                list_rd_days.remove(rd_days);
                list_txt_distribution.remove(txt_distribution);

                container.removeView(addView);
            }
        });
        container.addView(addView);
    }

    private void MedicineName_List_API() {
        // Tag used to cancel the request
        showpDialog();
        StringRequest strReq = new StringRequest(Request.Method.POST, RestInterface.URL_MedicineName, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Response: " + response);
                hidepDialog();
                try {
                    JSONObject jObj = new JSONObject(response);
                    String success = jObj.getString("success");

                    if (success.equals("1")) {

                        JSONArray requestarray = jObj.getJSONArray("data");

                        if (requestarray.equals("[]")) {

                            Toast.makeText(tempEditMedicine.this, "Not Any Medicine Name Found!", Toast.LENGTH_SHORT).show();

                        } else {

                            medicine_nameModelArrayList.clear();
                            responseList = new ArrayList<String>();

                            for (int i = 0; i < requestarray.length(); i++) {

                                Medicine_NameModel medicine_nameModels = new Medicine_NameModel();
                                JSONObject req = requestarray.getJSONObject(i);

                                medicine_nameModels.setId(req.optString(TAG_MEDICINE_ID));
                                medicine_nameModels.setName(req.optString(TAG_MEDICINE_NAME));

                                medicine_nameModelArrayList.add(medicine_nameModels);
                                responseList.add(req.optString(TAG_MEDICINE_NAME));

                                String id = req.optString(TAG_MEDICINE_ID);
                                String itemName = req.optString(TAG_MEDICINE_NAME);

                                map.put(id, itemName);
                            }

                            adapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_dropdown_item_1line, responseList);
                        }

                        MedicineList_API();

                    } else {
                        hidepDialog();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
                hidepDialog();
            }
        }) {

            @Override
            protected java.util.Map<String, String> getParams() {
                // Posting params to register url
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", user_id);
                return params;
            }
        };

        strReq.setRetryPolicy(new DefaultRetryPolicy(5000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // Adding request to request queue
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        strReq.setShouldCache(false);
        requestQueue.add(strReq);
    }

    private void AddPrescriptionCodeAPI(final String strMedicineId, final String strInstruction,
                                        final String strDose, final String strDuration,
                                        final String strDays, final String strDistribution, final String id) {

        showpDialog();

        StringRequest strReq = new StringRequest(Request.Method.POST, RestInterface.URL_UpdatePrescription, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Login Response: " + response);
                hidepDialog();
                try {
                    JSONObject jObj = new JSONObject(response);
                    String success = jObj.getString("success");

                    if (success.equals("1")) {

                        String message = jObj.getString("message");
                        Dialog_Success(message);

                    } else {

                        String message = jObj.getString("message");
                        Dialog_Failed(message);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Error: " + error.getMessage());
                hidepDialog();
            }
        }) {
            @Override
            protected java.util.Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("med_id", strMedicineId);
                params.put("instruction", strInstruction);
                params.put("dose", strDose);
                params.put("unit", "");
                params.put("direction", "");
                params.put("duration", strDuration);
                params.put("user_id", user_id);
                params.put("appointment_id", appointment_id);
                params.put("patient_id", user_id);
                params.put("days", strDays);
                params.put("description", strDistribution);
                params.put("id", id);
                return params;
            }
        };

        strReq.setRetryPolicy(new DefaultRetryPolicy(5000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // Adding request to request queue
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        strReq.setShouldCache(false);
        requestQueue.add(strReq);
    }

    private void MedicineList_API() {

        showpDialog();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                RestInterface.URL_MedicineList, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Service Response: " + response.toString());
                hidepDialog();

                try {
                    JSONObject jObj = new JSONObject(response);
                    String success = jObj.getString("success");


                    if (success.equals("1")) {

                        medicine_listModelArrayList = new ArrayList<>();
                        medicine_listModelArrayList.clear();

                        JSONArray requestarray = jObj.getJSONArray("message");

                        for (int i = 0; i < requestarray.length(); i++) {

                            JSONObject req = requestarray.getJSONObject(i);
                            Medicine_ListModel medicine_listModels = new Medicine_ListModel();

                            medicine_listModels.setId(req.optString("user_id"));
                            medicine_listModels.setId(req.optString("id"));
                            medicine_listModels.setMed_id(req.optString("med_id"));
                            medicine_listModels.setPatient_id(req.optString("patient_id"));
                            medicine_listModels.setAppointment_id(req.optString("appointment_id"));
                            medicine_listModels.setDose(req.optString("dose"));
                            medicine_listModels.setInstruction(req.optString("instruction"));
                            medicine_listModels.setDuration(req.optString("duration"));
                            medicine_listModels.setDirections(req.optString("directions"));
                            medicine_listModels.setDescription(req.optString("description"));
                            medicine_listModels.setDays(req.optString("days"));
                            medicine_listModels.setCreated_date(req.optString("created_date"));
                            medicine_listModels.setUpdated_date(req.optString("updated_date"));
                            medicine_listModels.setMedicine(req.optString("medicine"));

                            medicine_listModelArrayList.add(medicine_listModels);
                        }

                        int count = medicine_listModelArrayList.size();

                        if (count > 0) {

                            for (int i = 0; i < count; i++) {

                                String med_id = medicine_listModelArrayList.get(i).getMed_id();
                                String dose = medicine_listModelArrayList.get(i).getDose();
                                String instruction = medicine_listModelArrayList.get(i).getInstruction();
                                String duration = medicine_listModelArrayList.get(i).getDuration();
                                String days = medicine_listModelArrayList.get(i).getDays();
                                String description = medicine_listModelArrayList.get(i).getDescription();

                                AddOldField(med_id, dose, instruction, duration, days, description);
                            }
                        }

                    } else {

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Cust req Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();
                hidepDialog();
            }
        }) {

            @Override
            protected java.util.Map<String, String> getParams() {
                // Posting params to register url
                java.util.Map<String, String> params = new HashMap<String, String>();
                params.put("appointment_id", appointment_id);
                params.put("user_id", user_id);
                return params;
            }
        };

        // Adding request to request queue
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        strReq.setShouldCache(false);
        requestQueue.add(strReq);

    }

    private void showpDialog() {

        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Please wait...");
        pDialog.setCancelable(false);

        if (!pDialog.isShowing()) pDialog.show();
    }

    private void hidepDialog() {
        if (pDialog.isShowing()) pDialog.dismiss();
    }

    private void Dialog_Success(String value) {

        // dailog_alert dialog
        final Dialog dialog = new Dialog(tempEditMedicine.this);
        dialog.setContentView(R.layout.dailog_box);

        // set the dailog_alert dialog components - text, image and button
        ImageView image = (ImageView) dialog.findViewById(R.id.image);
        image.setImageDrawable(getResources().getDrawable(R.drawable.donemark));
        image.setMaxHeight(50);
        image.setMaxWidth(50);

        TextView tv_title = (TextView) dialog.findViewById(R.id.tv_title);
        tv_title.setText("Add Medicine");

        TextView text = (TextView) dialog.findViewById(R.id.tv_content);
        text.setText(value);

        Button dialogButton = (Button) dialog.findViewById(R.id.dialogButtonOK);
        // if button is clicked, close the dailog_alert dialog
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();

                Intent intent_MedicineList = new Intent(getApplicationContext(), Write_Prescription.class);
                intent_MedicineList.putExtra("appointment_id", appointment_id);
                startActivity(intent_MedicineList);
            }
        });

        dialog.show();
    }

    private void Dialog_Failed(String value) {
        // dailog_alert dialog
        final Dialog dialog = new Dialog(tempEditMedicine.this);
        dialog.setContentView(R.layout.dailog_box);

        // set the dailog_alert dialog components - text, image and button
        ImageView image = (ImageView) dialog.findViewById(R.id.image);
        image.setImageDrawable(getResources().getDrawable(R.drawable.ic_error_red_24dp));
        image.setMaxHeight(50);
        image.setMaxWidth(50);

        TextView tv_title = (TextView) dialog.findViewById(R.id.tv_title);
        tv_title.setText("Add Medicine");

        TextView text = (TextView) dialog.findViewById(R.id.tv_content);
        text.setText(value);

        Button dialogButton = (Button) dialog.findViewById(R.id.dialogButtonOK);
        // if button is clicked, close the dailog_alert dialog
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });

        dialog.show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        Intent intent_Write_Prescription = new Intent(getApplicationContext(), Write_Prescription.class);
        intent_Write_Prescription.putExtra("appointment_id", appointment_id);
        startActivity(intent_Write_Prescription);
    }
}
