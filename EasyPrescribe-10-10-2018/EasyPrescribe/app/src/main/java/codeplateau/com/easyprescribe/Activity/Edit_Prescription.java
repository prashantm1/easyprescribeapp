package codeplateau.com.easyprescribe.Activity;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import codeplateau.com.easyprescribe.Services.RestInterface;
import codeplateau.com.easyprescribe.R;

public class Edit_Prescription extends AppCompatActivity {

    private EditText txt_medicinename,txt_dur,txt_direction,txt_dose,txt_unit;
    private Button btn_addprescription;
    private ProgressDialog pDialog;
    private RadioGroup radio_instructions;
    private static final String TAG = "Write_Prescription";
    private static String id, med_id,patient_id,appointment_id,dose,instruction,duration,directions,created_date,updated_date;

    @Override
    protected void onCreate (Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit__prescription);

        initViews();
        setListeners();
    }


    private void initViews(){

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

        // Handle Toolbar
        android.support.v7.widget.Toolbar toolbar = (android.support.v7.widget.Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //set the back arrow in the toolbar
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        getSupportActionBar().setTitle(R.string.title_writePrescription);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });


        btn_addprescription= (Button)findViewById(R.id.btn_addprescription);
        txt_medicinename= (EditText )findViewById(R.id.tv_medicinename);
        txt_dur= (EditText )findViewById(R.id.txt_dururation);
        txt_dose= (EditText )findViewById(R.id.txt_dose);
        txt_direction=(EditText )findViewById(R.id.txt_direction);
        txt_unit=(EditText )findViewById(R.id.txt_unit);
        radio_instructions=(RadioGroup )findViewById(R.id.radio_instructions);
    }
    private void setListeners(){

        EditPrescription_API();
        btn_addprescription.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick (View view) {

                //Update_MedicineAPI();
            }
        });


    }

    private void EditPrescription_API () {
        // Tag used to cancel the request
        showpDialog();
        StringRequest strReq = new StringRequest(Request.Method.POST,
                RestInterface.URL_EditPrescription, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Login Response: " + response);

                hidepDialog();
                try {
                    JSONObject jObj = new JSONObject(response);
                    String success = jObj.getString("success");

                    if (success.equals("1")) {

                        JSONArray requestarray = jObj.getJSONArray("data");

                        for (int i = 0; i < requestarray.length(); i++) {

                            JSONObject req = requestarray.getJSONObject(i);
                            id = req.optString("userid");
                            med_id= req.optString("med_id");
                            patient_id= req.optString("patient_id");
                            appointment_id=req.optString("appointment_id");
                            dose=req.optString("dose");
                            instruction= req.optString("instruction");
                            duration= req.optString("duration");
                            directions= req.optString("directions");
                            created_date = req.optString("created_date");
                            updated_date = req.optString("updated_date");



                            txt_medicinename.setText("hellow");
                            txt_dose.setText(dose);
                            txt_direction.setText(directions);
                            txt_dur.setText(duration);
                          //  txt_unit.setText(content);
                            //txt_manufacter.setText(manftr);
                        }


                        /*if (!picture.equals("null")) {

                            String iconurl = "https://www.frenzybet.com/uploads/icons/" + picture;

                            Picasso.with(getApplicationContext())
                                    .load(iconurl)
                                    .placeholder(R.mipmap.ic_launcher) //this is optional the image to display while the url image is downloading
                                    .error(R.drawable.ic_error)
                                    .into(img_groupicon);
                        }*/

                    } else {
                        hidepDialog();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Error: " + error.getMessage());
                //Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
                hidepDialog();
            }
        }) {

            @Override
            protected java.util.Map<String, String> getParams() {
                // Posting params to register url
                Map<String, String> params = new HashMap<String, String>();
              /*  params.put("name",txt_patientname1.getText().toString().trim() );
                params.put("brandname",txt_brandname.getText().toString().trim() );
                params.put("strength",txt_strength.getText().toString().trim() );
                params.put("units","test");
                params.put("brandsfx",txt_brandsuf.getText().toString().trim() );
                params.put("content",txt_info.getText().toString().trim());
                params.put("manftr",txt_manufacter.getText().toString().trim());*/
                params.put("userid","2");


                return params;
            }
        };

        strReq.setRetryPolicy(new DefaultRetryPolicy(
                5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // Adding request to request queue
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        strReq.setShouldCache(false);
        requestQueue.add(strReq);
    }

    private void showpDialog() {

        // Progress Dialog
        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Please wait...");
        pDialog.setCancelable(false);

        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hidepDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
}
