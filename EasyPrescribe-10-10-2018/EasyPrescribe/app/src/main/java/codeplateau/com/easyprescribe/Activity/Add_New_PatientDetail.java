package codeplateau.com.easyprescribe.Activity;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import codeplateau.com.easyprescribe.Services.RestInterface;
import codeplateau.com.easyprescribe.R;

public class Add_New_PatientDetail extends AppCompatActivity {

    private RadioGroup radioGender;
    private RadioButton radioGenderButton;

    private Button btn_submit;

    private ProgressDialog pDialog;
    private TextView txt_bloodgroup, txt_weight, txt_weight_type, txt_height, txt_height_type;
    private EditText txt_patientname, txt_mobileno, txt_dob, txt_instruction;

    private String name, phone, dateofbirth, password, bloodgroup, gender, weight, height, description;

    private static final String TAG = Add_New_PatientDetail.class.getSimpleName().toString();
    Calendar myCalendar = null;

    SharedPreferences pref;

    private LinearLayout item_header;
    private FrameLayout frame_container;
    private String appointment_id,patient_id,med_id,user_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_newpatientdetail);
        initViews();
        setListener();
    }

    private void initViews() {

        pref = getApplicationContext().getSharedPreferences(RestInterface.pref, Context.MODE_PRIVATE);
        user_id = pref.getString("user_id","");

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

        // Handle Toolbar
        android.support.v7.widget.Toolbar toolbar = (android.support.v7.widget.Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //set the back arrow in the toolbar
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.title_Add_Patient_Detail);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        item_header = (LinearLayout) findViewById(R.id.item_header);
        frame_container = (FrameLayout) findViewById(R.id.frame_container);

        txt_patientname = (EditText) findViewById(R.id.txt_patientname);
        txt_mobileno = (EditText) findViewById(R.id.txt_mobileno);
        txt_dob = (EditText) findViewById(R.id.txt_dob);
        txt_bloodgroup = (TextView) findViewById(R.id.txt_bloodgroup);

        radioGender = (RadioGroup) findViewById(R.id.radioGender);

        txt_height = (TextView) findViewById(R.id.txt_height);
        txt_height_type = (TextView) findViewById(R.id.txt_height_type);

        txt_weight = (TextView) findViewById(R.id.txt_weight);
        txt_weight_type = (TextView) findViewById(R.id.txt_weight_type);

        txt_instruction = (EditText) findViewById(R.id.txt_insruction);
        btn_submit = (Button) findViewById(R.id.btn_submit);
    }

    private void setListener() {

        item_header.setVisibility(View.VISIBLE);
        myCalendar = Calendar.getInstance();

        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                update_date_dob();
            }
        };

        txt_dob.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new DatePickerDialog(Add_New_PatientDetail.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (txt_patientname.getText().toString().equals("")) {
                    txt_patientname.requestFocus();
                    Dialog_Alert("Patient Name");
                } else if (txt_mobileno.getText().toString().equals("")) {
                    txt_mobileno.requestFocus();
                    Dialog_Alert("Mobile No");
                } else if (txt_dob.getText().toString().equals("")) {
                    txt_dob.requestFocus();
                    Dialog_Alert("Date Of Birth");
                } else if (txt_bloodgroup.getText().toString().equals("")) {
                    txt_bloodgroup.requestFocus();
                    Dialog_Alert("Blood Group");
                } else if (txt_height.getText().toString().equals("")) {
                    txt_height.requestFocus();
                    Dialog_Alert("Height");
                } else if (txt_weight.getText().toString().equals("")) {
                    txt_weight.requestFocus();
                    Dialog_Alert("Weight");
                } else if (txt_instruction.getText().toString().equals("")) {
                    txt_instruction.requestFocus();
                    Dialog_Alert("Description");
                } else {

                    name = txt_patientname.getText().toString().trim();
                    phone = txt_mobileno.getText().toString().trim();
                    dateofbirth = txt_dob.getText().toString().trim();

                    bloodgroup = txt_bloodgroup.getText().toString().trim();

                    // get selected radio button from radioGroup
                    int selectedId = radioGender.getCheckedRadioButtonId();
                    radioGenderButton = (RadioButton) findViewById(selectedId);

                    gender = radioGenderButton.getText().toString();

                    height = txt_height.getText().toString().trim() + " " + txt_height_type.getText().toString().trim();
                    weight = txt_weight.getText().toString().trim() + " " + txt_weight_type.getText().toString().trim();

                    description = txt_instruction.getText().toString().trim();

                    AddPatient_detail_API();
                }
            }
        });

        txt_height_type.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ViewDialog_Height alert = new ViewDialog_Height();
                alert.showDialog_Height(Add_New_PatientDetail.this);
            }
        });

        txt_weight_type.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ViewDialog_weight alert = new ViewDialog_weight();
                alert.showDialog_weight(Add_New_PatientDetail.this);
            }
        });
        txt_bloodgroup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ViewDialog_BloodGroup alert = new ViewDialog_BloodGroup();
                alert.showDialog_bloodgroup(Add_New_PatientDetail.this);
            }
        });
    }
    private void update_date_dob() {

        String myFormat = "dd/MM/yy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        txt_dob.setText(sdf.format(myCalendar.getTime()));
    }

    private void AddPatient_detail_API() {

        showpDialog();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                RestInterface.URL_AddPatient_detail, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "New Patient Response: " + response);
                hidepDialog();
                try {
                    JSONObject jObj = new JSONObject(response);
                    String success = jObj.getString("success");

                    if (success.equals("1")) {

                        String data = jObj.getString("data");
                        Dialog_Success(data);

                    } else {

                        String data = jObj.getString("data");
                        Dialog_Failed(data);
                    }

                    hidepDialog();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Error: " + error.getMessage());
                //Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
                hidepDialog();
            }
        }) {

            @Override
            protected java.util.Map<String, String> getParams() {
                // Posting params to register url
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id",user_id);
                params.put("name", name);
                params.put("phone", phone);
                params.put("dateofbirth", dateofbirth);
                params.put("bloodgrp", bloodgroup);
                params.put("gender", gender);
                params.put("weight", weight);
                params.put("height", height);
                params.put("description", description);

                return params;
            }
        };

        strReq.setRetryPolicy(new DefaultRetryPolicy(
                5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // Adding request to request queue
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        strReq.setShouldCache(false);
        requestQueue.add(strReq);
    }


    private void showpDialog() {

        // Progress Dialog
        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Please wait...");
        pDialog.setCancelable(false);

        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hidepDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    public class ViewDialog_Height {

        public void showDialog_Height(Context context) {
            final Dialog dialog = new Dialog(context);
            dialog.setCanceledOnTouchOutside(true);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(false);
            dialog.setContentView(R.layout.item_heights);

            TextView dialogButton_jan = (TextView) dialog.findViewById(R.id.lbl_ft);
            dialogButton_jan.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    txt_height_type.setText("ft");
                    dialog.dismiss();
                }
            });

            TextView dialogButton_feb = (TextView) dialog.findViewById(R.id.lbl_inches);
            dialogButton_feb.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    txt_height_type.setText("inches");
                    dialog.dismiss();
                }
            });

            ImageView img_close = (ImageView) dialog.findViewById(R.id.img_close);
            img_close.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

            dialog.show();
        }
    }

    public class ViewDialog_weight {

        public void showDialog_weight(Context context) {
            final Dialog dialog = new Dialog(context);
            dialog.setCanceledOnTouchOutside(true);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(false);
            dialog.setContentView(R.layout.item_weights);

            TextView dialogButton_kg = (TextView) dialog.findViewById(R.id.lbl_Kg);
            dialogButton_kg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    txt_weight_type.setText("kg");
                    dialog.dismiss();
                }
            });

            TextView dialogButton_gram = (TextView) dialog.findViewById(R.id.lbl_gram);
            dialogButton_gram.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    txt_weight_type.setText("gram");
                    dialog.dismiss();
                }
            });

            TextView dialogButton_pounds = (TextView) dialog.findViewById(R.id.lbl_pounds);
            dialogButton_pounds.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    txt_weight_type.setText("pounds");
                    dialog.dismiss();
                }
            });


            ImageView img_close = (ImageView) dialog.findViewById(R.id.img_close);
            img_close.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

            dialog.show();
        }
    }

    public class ViewDialog_BloodGroup {

        public void showDialog_bloodgroup(Context context) {
            final Dialog dialog = new Dialog(context);
            dialog.setCanceledOnTouchOutside(true);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(false);
            dialog.setContentView(R.layout.item_bloodgroup);

            TextView dialogButton_Apositive = (TextView) dialog.findViewById(R.id.lbl_APositive);
            dialogButton_Apositive.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    txt_bloodgroup.setText("A+");
                    dialog.dismiss();
                }
            });

            TextView dialogButton_ANegative = (TextView) dialog.findViewById(R.id.lbl_ANegative);
            dialogButton_ANegative.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    txt_bloodgroup.setText("A-");
                    dialog.dismiss();
                }
            });

            TextView dialogButton_Bpositive = (TextView) dialog.findViewById(R.id.lbl_Bpositive);
            dialogButton_Bpositive.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    txt_bloodgroup.setText("B+");
                    dialog.dismiss();
                }
            });
            TextView dialogButton_BNegative = (TextView) dialog.findViewById(R.id.lbl_BNegative);
            dialogButton_BNegative.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    txt_bloodgroup.setText("B-");
                    dialog.dismiss();
                }
            });
            TextView dialogButton_AB = (TextView) dialog.findViewById(R.id.lbl_ABpositive);
            dialogButton_AB.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    txt_bloodgroup.setText("AB+");
                    dialog.dismiss();
                }
            });
            TextView dialogButton_ABNegative = (TextView) dialog.findViewById(R.id.lbl_ABNegative);
            dialogButton_ABNegative.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    txt_bloodgroup.setText("AB-");
                    dialog.dismiss();
                }
            });
            TextView dialogButton_Opositive = (TextView) dialog.findViewById(R.id.lbl_Opositive);
            dialogButton_Opositive.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    txt_bloodgroup.setText("O+");
                    dialog.dismiss();
                }
            });
            TextView dialogButton_ONegative = (TextView) dialog.findViewById(R.id.lbl_ONegative);
            dialogButton_ONegative.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    txt_bloodgroup.setText("O-");
                    dialog.dismiss();
                }
            });


            ImageView img_close = (ImageView) dialog.findViewById(R.id.img_close);
            img_close.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

            dialog.show();
        }
    }

    public class ViewDialog_old_Patientmsg {

        public void showDialog_oldmsg(Activity activity) {
            final Dialog dialog = new Dialog(activity);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(false);
            dialog.setContentView(R.layout.dialog_new_patients_message);
            int width = getWindow().getDecorView().getWidth();
            int width_75 = ((75 * width) / 100);

            LinearLayout.LayoutParams forButtonParams = new LinearLayout.LayoutParams(width_75, LinearLayout.LayoutParams.WRAP_CONTENT);
            LinearLayout.LayoutParams forTextViewParams = new LinearLayout.LayoutParams(width_75, LinearLayout.LayoutParams.WRAP_CONTENT);
            Resources r = activity.getResources();


            int px_bottom = (int) TypedValue.applyDimension(
                    TypedValue.COMPLEX_UNIT_DIP,
                    15,
                    r.getDisplayMetrics()
            );

            forButtonParams.setMargins(0, px_bottom, 0, px_bottom);
            forButtonParams.gravity = Gravity.CENTER;


            TextView dialogButton = (TextView) dialog.findViewById(R.id.lbl_OK);
            dialogButton.setLayoutParams(forButtonParams);
            dialogButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    item_header.setVisibility(View.GONE);
                    frame_container.setVisibility(View.VISIBLE);

                    Intent intent_MainActivity = new Intent(getApplicationContext(), DrawerLayoutActivity.class);
                    startActivity(intent_MainActivity);
                    dialog.dismiss();
                }
            });

            dialog.show();
        }
    }

    private void Dialog_Alert(String value) {

        // dailog_alert dialog
        final Dialog dialog = new Dialog(Add_New_PatientDetail.this);
        dialog.setContentView(R.layout.dailog_box);

        // set the dailog_alert dialog components - text, image and button
        ImageView image = (ImageView) dialog.findViewById(R.id.image);
        image.setImageDrawable(getResources().getDrawable(R.drawable.ic_warning_yellow_24dp));
        image.setMaxHeight(50);
        image.setMaxWidth(50);

        TextView tv_title = (TextView) dialog.findViewById(R.id.tv_title);
        tv_title.setText("Add New Patient");

        // set the dailog_alert dialog components - text, image and button
        TextView text = (TextView) dialog.findViewById(R.id.tv_content);
        text.setText("Enter the " + value);

        Button dialogButton = (Button) dialog.findViewById(R.id.dialogButtonOK);
        // if button is clicked, close the dailog_alert dialog
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private void Dialog_Success(String value) {

        // dailog_alert dialog
        final Dialog dialog = new Dialog(Add_New_PatientDetail.this);
        dialog.setContentView(R.layout.dailog_box);

        // set the dailog_alert dialog components - text, image and button
        ImageView image = (ImageView) dialog.findViewById(R.id.image);
        image.setImageDrawable(getResources().getDrawable(R.drawable.donemark));
        image.setMaxHeight(50);
        image.setMaxWidth(50);

        TextView tv_title = (TextView) dialog.findViewById(R.id.tv_title);
        tv_title.setText("Add New Patient");

        TextView text = (TextView) dialog.findViewById(R.id.tv_content);
        text.setText(value);

        Button dialogButton = (Button) dialog.findViewById(R.id.dialogButtonOK);
        // if button is clicked, close the dailog_alert dialog
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();

                Intent intent_DrawerLayoutActivity= new Intent(getApplicationContext(), DrawerLayoutActivity.class);
                startActivity(intent_DrawerLayoutActivity);
            }
        });

        dialog.show();
    }

    private void Dialog_Failed(String value) {

        // dailog_alert dialog
        final Dialog dialog = new Dialog(Add_New_PatientDetail.this);
        dialog.setContentView(R.layout.dailog_box);

        // set the dailog_alert dialog components - text, image and button
        ImageView image = (ImageView) dialog.findViewById(R.id.image);
        image.setImageDrawable(getResources().getDrawable(R.drawable.ic_error_red_24dp));
        image.setMaxHeight(50);
        image.setMaxWidth(50);

        TextView tv_title = (TextView) dialog.findViewById(R.id.tv_title);
        tv_title.setText("Add New Patient");

        TextView text = (TextView) dialog.findViewById(R.id.tv_content);
        text.setText(value);

        Button dialogButton = (Button) dialog.findViewById(R.id.dialogButtonOK);
        // if button is clicked, close the dailog_alert dialog
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });

        dialog.show();
    }


    @Override
    public void onBackPressed() {
        Intent intent_DrawerLayoutActivity = new Intent(getApplicationContext(), DrawerLayoutActivity.class);
        startActivity(intent_DrawerLayoutActivity);
    }

}
