package codeplateau.com.easyprescribe.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import codeplateau.com.easyprescribe.Model.Medicine_NameModel;
import codeplateau.com.easyprescribe.R;
import codeplateau.com.easyprescribe.Services.RestInterface;

public class Edit_MedicineActivity extends AppCompatActivity {

    private EditText txt_brandname, txt_strength, txt_brandsuf, txt_info, txt_manufacter;
    private Button btn_AddeditMedicine;
    private ProgressDialog pDialog;
    private static View view;
    private AutoCompleteTextView txt_patientname1;
    private static String TAG_ID = "userid";
    private static String TAG_NAME = "name";
    private TextView txt_dose, txt_dur, txt_direction, txt_unit, lbl_unit;
    private static final String TAG = "Add_Medicine";
    private static String med_id, dose, instruction, unit, directions, duration, user_id, appointment_id, patient_id, id, medicine;
    private RadioGroup txt_radioGroup;
    //private static String userid,name,created_date,updated_date,brandname,strength,brandsfx,content,manftr,units;

    ArrayList<Medicine_NameModel> medicine_nameModelArrayList;
    List<String> responseList = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit__medicine);

        initViews();
        setListeners();
    }

    private void initViews() {

        Bundle extras = getIntent().getExtras();
        if (extras != null) {

            patient_id = extras.getString("patient_id");
            med_id = extras.getString("med_id");
            user_id = extras.getString("user_id");
            appointment_id = extras.getString("appointment_id");
            dose = extras.getString("dose");
            instruction = extras.getString("instruction");
            duration = extras.getString("duration");
            directions = extras.getString("directions");
            medicine = extras.getString("medicine");
        }

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

        // Handle Toolbar
        android.support.v7.widget.Toolbar toolbar = (android.support.v7.widget.Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //set the back arrow in the toolbar
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.title_Edit_Medicine);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();
            }
        });

        txt_patientname1 = (AutoCompleteTextView) findViewById(R.id.txt_patientname1);
        txt_info = (EditText) findViewById(R.id.txt_info);
        txt_dose = (EditText) findViewById(R.id.txt_dose);
        txt_dur = (TextView) findViewById(R.id.txt_dururation);
        txt_unit = (TextView) findViewById(R.id.txt_unit);
        txt_direction = (TextView) findViewById(R.id.txt_direction);
        txt_manufacter = (EditText) findViewById(R.id.txt_manufacter);
        btn_AddeditMedicine = (Button) findViewById(R.id.btn_AddeditMedicine);
        txt_radioGroup = (RadioGroup) findViewById(R.id.txt_radioGroup);
        lbl_unit = (TextView) findViewById(R.id.lbl_unit);
    }

    private void setListeners() {

        MedicineName_API();

        btn_AddeditMedicine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Update_PrescriptionAPI();
            }
        });
    }

    private void MedicineName_API() {
        // Tag used to cancel the request
        showpDialog();
        StringRequest strReq = new StringRequest(Request.Method.POST,
                RestInterface.URL_MedicineName, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Login Response: " + response);
                hidepDialog();
                try {
                    JSONObject jObj = new JSONObject(response);
                    String success = jObj.getString("success");

                    if (success.equals("1")) {

                        JSONArray requestarray = jObj.getJSONArray("data");

                        medicine_nameModelArrayList = new ArrayList<Medicine_NameModel>();
                        medicine_nameModelArrayList.clear();

                        responseList = new ArrayList<String>();

                        for (int i = 0; i < requestarray.length(); i++) {

                            Medicine_NameModel medicine_nameModels = new Medicine_NameModel();
                            JSONObject req = requestarray.getJSONObject(i);

                            medicine_nameModels.setId(req.getString(TAG_ID));
                            medicine_nameModels.setName(req.getString(TAG_NAME));

                            medicine_nameModelArrayList.add(medicine_nameModels);
                            responseList.add(req.getString(TAG_NAME));
                        }

                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_dropdown_item_1line, responseList);
                        txt_patientname1.setThreshold(1);
                        txt_patientname1.setAdapter(adapter);

                        txt_patientname1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                                for (int i = 0; i < medicine_nameModelArrayList.size(); i++) {

                                    String item_name = txt_patientname1.getText().toString();

                                    if (item_name.equals(medicine_nameModelArrayList.get(i).getName())) {

                                        med_id = medicine_nameModelArrayList.get(i).getId();
                                    }
                                }
                            }
                        });

                    } else {
                        hidepDialog();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Error: " + error.getMessage());
                //Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
                hidepDialog();
            }
        }) {

            @Override
            protected java.util.Map<String, String> getParams() {
                // Posting params to register url
                Map<String, String> params = new HashMap<String, String>();

                return params;
            }
        };

        strReq.setRetryPolicy(new DefaultRetryPolicy(
                5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // Adding request to request queue
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        strReq.setShouldCache(false);
        requestQueue.add(strReq);
    }


    private void Update_PrescriptionAPI() {

        showpDialog();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                RestInterface.URL_UpdatePrescription, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Login Response: " + response);
                //hideProgressDialog();
                hidepDialog();
                try {
                    JSONObject jObj = new JSONObject(response);
                    String success = jObj.getString("success");

                    if (success.equals("1")) {

                        String message = jObj.getString("message");
                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();

                        Intent intent_GroupTaskList = new Intent(getApplicationContext(), Write_Prescription.class);
                        intent_GroupTaskList.putExtra("appointment_id", appointment_id);
                        startActivity(intent_GroupTaskList);

                    } else {

                        String message = jObj.getString("message");
                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();

                        hidepDialog();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Error: " + error.getMessage());
                //Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
                hidepDialog();
            }
        }) {

            @Override
            protected java.util.Map<String, String> getParams() {
                // Posting params to register url
                Map<String, String> params = new HashMap<String, String>();

                params.put("med_id", med_id);
                params.put("dose", txt_dose.getText().toString().trim());
                //   params.put("instruction", txt_radioGroup.getText().toString().trim());
                params.put("unit", txt_unit.getText().toString().trim());
                params.put("directions", txt_direction.getText().toString().trim());
                params.put("duration", txt_dur.getText().toString().trim());
                //  params.put("appointment_id",appointment_id);
                params.put("user_id", user_id);
                params.put("userid", id);

                return params;
            }
        };

        strReq.setRetryPolicy(new DefaultRetryPolicy(
                5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // Adding request to request queue
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        strReq.setShouldCache(false);
        requestQueue.add(strReq);
    }

    private void showpDialog() {

        // Progress Dialog
        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Please wait...");
        pDialog.setCancelable(false);

        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hidepDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
}
