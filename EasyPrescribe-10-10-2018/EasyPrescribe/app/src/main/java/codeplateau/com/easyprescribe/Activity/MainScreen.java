package codeplateau.com.easyprescribe.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import codeplateau.com.easyprescribe.R;

public class MainScreen extends AppCompatActivity {

    private static Button btn_login, btn_Registration;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_screen);

        initViews();
        setListeners();
    }

    private void initViews() {

        btn_login = (Button) findViewById(R.id.btn_login);
        btn_Registration = (Button) findViewById(R.id.btn_Registration);
    }

    private void setListeners() {

        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(MainScreen.this, Login.class);
                startActivity(intent);
            }
        });

        btn_Registration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(MainScreen.this, Registration.class);
                startActivity(intent);
            }
        });


    }
}
