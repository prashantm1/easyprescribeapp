package codeplateau.com.easyprescribe.Model;

public class Navigation_Items {

	String title;
	Integer icon;

	public Navigation_Items (String title, Integer icon) {
		this.title = title;
		this.icon = icon;
	}

	public String getTitle() {
		return title;
	}

	public Integer getIcon() {
		return icon;
	}

}
