package codeplateau.com.easyprescribe.Services;

public class RestInterface {

    // Global Variable
    public static String pref = "EasyPrescribe123456";

    //  domain name
    public static String domain_name = "http://frenzybet.com/prescribe/";

    //Login
    public static String URL_Login = domain_name + "api/posts/login";

    // group icon image upload path
    public static String URL_IMAGEICONUPLOAD = "http://www.frenzybet.com/fileUpload.php";

    //Registration
    public static String URL_Registration = domain_name + "api/posts/register";

    //AddPatient_detail
    public static String URL_AddPatient_detail = domain_name + "api/posts/add_appointment";

    //EditPatient_detail
    public static String URL_EditPatient_detail = domain_name + "api/posts/edit-patient";

    //update-patient
    public static String URL_UpdateApI = domain_name + "api/posts/update-patient";

    // Edit Prescription
    public static String URL_EditPrescription = domain_name + "api/posts/edit-prescription";

    //add-prescription
    public static String URL_AddPrescription = domain_name + "api/posts/add-prescription";

    //update-prescription
    public static String URL_UpdatePrescription = domain_name + "api/posts/update_prescription";

    //add-medicines
    public static String URL_AddMedicine = domain_name + "api/posts/add-medicines";

    //edit-medicines
    public static String URL_EditMedicine = domain_name + "api/posts/edit-prescription";

    // delete prescription
    public static String  URL_DeletePrescription= domain_name + "api/posts/delete-prescription";

    //update-medicines
    public static String URL_UpdateMedicine = domain_name + "api/posts/update-medicines";

    // Add Medicine
    public static String URL_MedicineList = domain_name + "api/posts/app-medicine-list";

    // Appointment
    public static String URL_AppointmentList = domain_name + "api/posts/appointment-listing";

    // medicines list
    public static String URL_MedicineName = domain_name + "api/posts/medicines-listing";

    // update profile
    public static String URL_Update_Profile_API= domain_name+"api/posts/update-profile";

    public static String URL_UpdateProfile= domain_name+"api/posts/update-profile";

    public static String URL_edit_Profile_API= domain_name+"api/posts/edit-profile";

    // Print
    public static String URL_PRINTBILL = "http://frenzybet.com/prescribe/print-pres/";


    public static String URL_DRUGSLIST = "http://216.10.240.90/~easyprescribe/index.php/Apicontroller/index/view_drugs";
}
