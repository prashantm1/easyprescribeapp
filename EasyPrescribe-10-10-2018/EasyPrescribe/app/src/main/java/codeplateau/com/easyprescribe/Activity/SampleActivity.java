package codeplateau.com.easyprescribe.Activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import codeplateau.com.easyprescribe.Adapter.MediciNameAdapter;
import codeplateau.com.easyprescribe.Model.Drugs_Model;
import codeplateau.com.easyprescribe.Model.Medicine_NameModel;
import codeplateau.com.easyprescribe.R;
import codeplateau.com.easyprescribe.Services.RestInterface;
import codeplateau.com.easyprescribe.Services.Utils;

public class SampleActivity extends AppCompatActivity {


    private static View view;
    private String TAG = Utils.Login_Fragment;

    private ProgressDialog pDialog;
    private static FragmentManager fragmentManager;
    private static String private_key;
    private static SharedPreferences pref;
    private SharedPreferences sharedPreferences;
    LinearLayout container;
    int medicine_count = 0;
    private EditText ed_direction;
    public int item_count = 0;
    public static String page,drug_type;


    private static Button btn_add_medicine,btn_submit,btn_cancel,btn_cancel1;
    private static EditText txt_weight;
    private static RadioButton radioDaysButton;
  //  private static AutoCompleteTextView txt_item_name;

    ArrayList<String> Item_Names_list = new ArrayList<String>();
    ArrayList<String> Quantity_list = new ArrayList<String>();


    ArrayList<Medicine_NameModel> medicine_nameModelArrayList;
    List<String> responseList = new ArrayList<String>();


    ArrayList<String> Morning_List = new ArrayList<String>();
    ArrayList<String> Afternoon_List = new ArrayList<String>();
    ArrayList<String> Evening_List = new ArrayList<String>();

    List<AutoCompleteTextView> ed_txt_item_name = new ArrayList<AutoCompleteTextView>();
    List<EditText> ed_txt_quantity = new ArrayList<EditText>();
    List<EditText> ed_txt_days = new ArrayList<EditText>();

    List<Spinner> ed_spin_type = new ArrayList<Spinner>();
    ArrayList<String> TypeCategory_List = new ArrayList<String>();
    ArrayList<String> Days_List = new ArrayList<String>();

    List<CheckBox> ed_chk_morning = new ArrayList<CheckBox>();
    List<CheckBox> ed_chk_afternoon = new ArrayList<CheckBox>();
    List<CheckBox> ed_chk_evening = new ArrayList<CheckBox>();

    List<Integer> ed_btn_cancel = new ArrayList<Integer>();

    List<RadioGroup> ed_rd_days = new ArrayList<RadioGroup>();
    List<RadioGroup> ed_rd_days1 = new ArrayList<RadioGroup>();

    List<RadioGroup> ed_rd_days2 = new ArrayList<RadioGroup>();

    List<RadioGroup> ed_rd_days3 = new ArrayList<RadioGroup>();



    private static String TAG_MEDICINE_ID = "id";
    private String appointment_id, user_id,patient_id,id;
    private static String med_id, dose, unit, direction, instruction, duration,description,days;
    private static String TAG_MEDICINE_NAME = "name";
    private RadioButton radio_instructionButton;
    MediciNameAdapter mediciNameAdapter;

    private AutoCompleteTextView txt_item_name;
    private RadioGroup rd_dose,rd_Duration,rd_instruction,rd_days;

    private LinearLayout parentLinearLayout;

    @Override
    protected void onCreate (Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sample);
        initViews();
        setListeners();
    }

    private  void initViews(){


        btn_add_medicine=(Button)findViewById(R.id.btn_add_medicine);
        btn_submit = (Button) findViewById(R.id.btn_submit);
        btn_cancel=(Button) findViewById(R.id.btn_cancel);

        txt_item_name = (AutoCompleteTextView) findViewById(R.id.txt_item_name);
        rd_days=(RadioGroup)findViewById(R.id.rd_days);
        rd_dose=(RadioGroup )findViewById(R.id.rd_dose);
        rd_Duration=(RadioGroup )findViewById(R.id.rd_Duration);
        rd_instruction=(RadioGroup )findViewById(R.id.rd_instruction);
        ed_direction=(EditText )findViewById(R.id.ed_direction);

    }

    private void setListeners(){

        private_key = getString(R.string.privatekey);
        pref = getApplicationContext().getSharedPreferences("MyLogin", 0);

       // Add_Drugs();

        MedicineName_List_API();

        btn_add_medicine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //AddPrescriptionDetail();

                Add_Medicine_View();//Add_Prescrption
                Add_Medicine_Detail();

                txt_item_name.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                        for(int i=0; i < medicine_nameModelArrayList.size(); i++){

                            String item_name = txt_item_name.getText().toString();

                            if(item_name.equals(medicine_nameModelArrayList.get(i).getName())){

                                med_id = medicine_nameModelArrayList.get(i).getId();
                            }
                        }
                    }
                });
            }
        });

        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //SavePrescription();
                SaveMedicineDetail();
            }
        });

      /*  btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getContext());
                alertDialogBuilder.setMessage("Are you sure, You wanted to make decision. if you press yes all process is gone");
                alertDialogBuilder.setPositiveButton("yes",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface arg0, int arg1) {

                                if(page.equals("AppointmentList_Fragment")){

                                    AppointmentList_Fragment appointmentList_fragment = new AppointmentList_Fragment();
                                    Bundle args = new Bundle();
                                    appointmentList_fragment.setArguments(args);

                                    FragmentManager fragmentManager = ((AppCompatActivity)getContext()).getSupportFragmentManager();
                                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction()
                                            .setCustomAnimations(R.anim.left_enter, R.anim.right_out)
                                            .replace(R.id.frame_container, appointmentList_fragment);
                                    fragmentTransaction.addToBackStack(null);
                                    fragmentTransaction.commit();

                                } else if(page.equals("ViewPrescription_Fragment")) {

                                    ViewPrescription_Fragment viewPrescription_fragment = new ViewPrescription_Fragment();
                                    Bundle args = new Bundle();
                                    viewPrescription_fragment.setArguments(args);

                                    FragmentManager fragmentManager = ((AppCompatActivity)getContext()).getSupportFragmentManager();
                                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction()
                                            .setCustomAnimations(R.anim.left_enter, R.anim.right_out)
                                            .replace(R.id.frame_container, viewPrescription_fragment);
                                    fragmentTransaction.addToBackStack(null);
                                    fragmentTransaction.commit();
                                }
                            }
                        });

                alertDialogBuilder.setNegativeButton("No",new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {


                    }
                });

                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            }
        });*/
    }

    private void SaveMedicineDetail(){

      /*  if(txt_weight.getText().toString().equals("")){
            Toast.makeText(getApplicationContext(), "Enter weight", Toast.LENGTH_SHORT).show();
        } else {*/

            if(ed_txt_item_name.size() > 0){

               // weight = txt_weight.getText().toString().trim();

                for (int i=0;i<ed_txt_item_name.size();i++){

                    String item = ed_txt_item_name.get(i).getText().toString();

                    if(item.equals("")){

                        //Toast.makeText(getContext(), "Enter Items name", Toast.LENGTH_SHORT).show();
                        item_count = item_count + 1;

                    } else {

                        /*for (int j=0;j<ed_txt_quantity.size();j++){

                            Quantity_list.add(ed_txt_quantity.get(j).getText().toString().trim());
                        }

                        for (int k=0;k<ed_spin_type.size();k++){

                            String typecat = ed_spin_type.get(k).getSelectedItem().toString();
                            TypeCategory_List.add(typecat);
                        }*/
                    }
                }

                if(item_count > 0){

                    Toast.makeText(getApplicationContext(), "Enter Items name", Toast.LENGTH_SHORT).show();
                    item_count = 0;

                } else {

                    for (int z=0;z<ed_txt_item_name.size();z++){

                        Item_Names_list.add(ed_txt_item_name.get(z).getText().toString().trim());
                    }

                    for (int k=0;k<ed_spin_type.size();k++){

                        String typecat = ed_spin_type.get(k).getSelectedItem().toString();
                        TypeCategory_List.add(typecat);
                    }

                    AddPrescriptionCode();
                }

            } else {

                Toast.makeText(getApplicationContext(), "Enter Items name", Toast.LENGTH_SHORT).show();
            }

        }



    private void AddPrescriptionCode() {

        showpDialog();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                RestInterface.URL_AddPrescription, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Login Response: " + response);
                hidepDialog();
                try {
                    JSONObject jObj = new JSONObject(response);
                    String success = jObj.getString("success");

                    if (success.equals("1")) {

                        String message = jObj.getString("message");
                        Dialog_Success(message);

                        Intent intent_MedicineList = new Intent(getApplicationContext(), Write_Prescription.class);

                        intent_MedicineList.putExtra("appointment_id", appointment_id);
                        intent_MedicineList.putExtra("patient_id",patient_id);
                        intent_MedicineList.putExtra("user_id",user_id);
                        intent_MedicineList.putExtra("med_id",med_id);
                        intent_MedicineList.putExtra("id",id);
                        startActivity(intent_MedicineList);

                    } else {

                        String message = jObj.getString("message");
                        Dialog_Failed(message);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Error: " + error.getMessage());
                hidepDialog();
            }
        }) {

            @Override
            protected java.util.Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("med_id", med_id);
                params.put("instruction", String.valueOf(rd_instruction));
                params.put("dose", String.valueOf(rd_dose));
                params.put("unit", unit);
                params.put("direction", direction);
                params.put("duration", String.valueOf(rd_Duration));
                params.put("user_id",user_id);
                params.put("appointment_id", appointment_id);
                params.put("patient_id", patient_id);
                params.put("days", String.valueOf(rd_days));
                params.put("description",description);
                return params;
            }
        };

        strReq.setRetryPolicy(new DefaultRetryPolicy(
                5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // Adding request to request queue
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        strReq.setShouldCache(false);
        requestQueue.add(strReq);
    }


    private void Add_Medicine_Detail(){

        LayoutInflater layoutInflater = (LayoutInflater) getApplicationContext().getSystemService(getApplicationContext().LAYOUT_INFLATER_SERVICE);
        final View addView = layoutInflater.inflate(R.layout.field, null);

        txt_item_name = (AutoCompleteTextView) addView.findViewById(R.id.txt_item_name);
       // final EditText txt_quantity = (EditText)addView.findViewById(R.id.txt_quantity);
        //spin_type = (Spinner) addView.findViewById(R.id.spin_type);
        final Button btn_cancel = (Button) addView.findViewById(R.id.btn_cancel1);

       /* final Double doubleWeight;
        weight = txt_weight.getText().toString().trim();

        if(!weight.equals("")){
            doubleWeight = Double.valueOf(weight);
        }else {
            doubleWeight = 0.0;
        }*/

        List<String> type = new ArrayList<String>();
        type.add("Tab");
        type.add("Syrup");
        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_item, type);
        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // attaching data adapter to spinner
        //spin_type.setAdapter(dataAdapter);

        if(!medicine_nameModelArrayList.isEmpty()){

            txt_item_name.setThreshold(1);
            //txt_item_name.setAdapter(MediciNameAdapter);
        }

        txt_item_name.requestFocus();
        InputMethodManager imm = (InputMethodManager) getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(txt_item_name, InputMethodManager.SHOW_IMPLICIT);

        ed_txt_item_name.add(txt_item_name);
        //ed_spin_type.add(spin_type);
      //  ed_txt_quantity.add(txt_quantity);


        // spin type
       /* spin_type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                // On selecting a spinner item
                String item = parent.getItemAtPosition(position).toString();

                Double dblWeight = doubleWeight;

                if(item.equals("Tab")){

                    if(dblWeight > 0 && dblWeight <= 2){
                        txt_quantity.setText("1");
                    } else if(dblWeight > 2 && dblWeight <= 4.5){
                        txt_quantity.setText("2");
                    } else if(dblWeight > 4 && dblWeight <= 6.5){
                        txt_quantity.setText("3");
                    } else if(dblWeight > 6 && dblWeight <= 8.5){
                        txt_quantity.setText("4");
                    } else if(dblWeight > 8){
                        txt_quantity.setText("5");
                    } else {
                        txt_weight.setText("0");
                    }
                } else {

                    if(dblWeight > 0 && dblWeight <= 2){
                        txt_quantity.setText("5 ml");
                    } else if(dblWeight > 2 && dblWeight <= 4.5){
                        txt_quantity.setText("7.5 ml");
                    } else if(dblWeight > 4 && dblWeight <= 6.5){
                        txt_quantity.setText("10 ml");
                    } else if(dblWeight > 6 && dblWeight <= 8.5){
                        txt_quantity.setText("12.5 ml");
                    } else if(dblWeight > 8.5){
                        txt_quantity.setText("15 ml");
                    } else {
                        txt_weight.setText("0");
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });*/

        btn_add_medicine.setText("Add Medicine");

        btn_cancel1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ed_txt_item_name.remove(txt_item_name);
             //   ed_spin_type.remove(spin_type);
               // ed_txt_quantity.remove(txt_quantity);

                //((LinearLayout)addView.getParent()).removeView(addView);
                container.removeView(addView);
            }
        });
        container.addView(addView);
        //addView.show();
    }


    private void Add_Medicine_View(){

        // Print the number of edittext
        for (int i = medicine_count; i <= medicine_count; i++) {

            LayoutInflater layoutInflater = (LayoutInflater) getApplicationContext().getSystemService(getApplicationContext().LAYOUT_INFLATER_SERVICE);
            final View addView = layoutInflater.inflate(R.layout.field, null);

            txt_item_name = (AutoCompleteTextView) addView.findViewById(R.id.txt_item_name);
            final EditText txt_quantity = (EditText)addView.findViewById(R.id.txt_quantity);
            /*final EditText txt_days = (EditText)addView.findViewById(R.id.txt_days);

            radioDaysGroup = (RadioGroup)addView.findViewById(R.id.rd_days);*/

           // spin_type = (Spinner) addView.findViewById(R.id.spin_type);

            /*final CheckBox chk_morning = (CheckBox) addView.findViewById(R.id.chk_morning);
            final CheckBox chk_afternoon = (CheckBox) addView.findViewById(R.id.chk_afternoon);
            final CheckBox chk_evening = (CheckBox) addView.findViewById(R.id.chk_evening);*/

         //   Typeface font = Typeface.createFromAsset(getApplicationContext().getAssets(), "mangal.ttf");
            /*chk_morning.setTypeface(font);
            chk_afternoon.setTypeface(font);
            chk_evening.setTypeface(font);*/
          //  txt_item_name.setTypeface(font);

            final Button btn_cancel1 = (Button) addView.findViewById(R.id.btn_cancel1);

          /*  final Double doubleWeight;
            weight = txt_weight.getText().toString().trim();

            if(!weight.equals("")){
                doubleWeight = Double.valueOf(weight);
            }else {
                doubleWeight = 0.0;
            }*/

            // Spinner Drop down elements
            List<String> type = new ArrayList<String>();
            type.add("Tab");
            type.add("Syrup");
            // Creating adapter for spinner
            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_item, type);
            // Drop down layout style - list view with radio button
            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            // attaching data adapter to spinner
           // spin_type.setAdapter(dataAdapter);

            txt_item_name.setId(i);
           // txt_quantity.setId(i);
            //txt_days.setId(i);

           // spin_type.setId(i);

            /*chk_morning.setId(i);
            chk_afternoon.setId(i);
            chk_evening.setId(i);*/

            btn_cancel1.setId(i);

           /* if(!drugs_modelArrayList.isEmpty()){

                txt_item_name.setThreshold(1);
                txt_item_name.setAdapter(drugs_adapter);
            }*/

            // spin type
            /*spin_type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    // On selecting a spinner item
                    String item = parent.getItemAtPosition(position).toString();

                    Double dblWeight = doubleWeight;

                    if(item.equals("Tab")){

                        if(dblWeight > 0 && dblWeight <= 2){
                            txt_quantity.setText("1");
                        } else if(dblWeight > 2 && dblWeight <= 4.5){
                            txt_quantity.setText("2");
                        } else if(dblWeight > 4 && dblWeight <= 6.5){
                            txt_quantity.setText("3");
                        } else if(dblWeight > 6 && dblWeight <= 8.5){
                            txt_quantity.setText("4");
                        } else if(dblWeight > 8){
                            txt_quantity.setText("5");
                        } else {
                            txt_weight.setText("0");
                        }
                    } else {

                        if(dblWeight > 0 && dblWeight <= 2){
                            txt_quantity.setText("5 ml");
                        } else if(dblWeight > 2 && dblWeight <= 4.5){
                            txt_quantity.setText("7.5 ml");
                        } else if(dblWeight > 4 && dblWeight <= 6.5){
                            txt_quantity.setText("10 ml");
                        } else if(dblWeight > 6 && dblWeight <= 8.5){
                            txt_quantity.setText("12.5 ml");
                        } else if(dblWeight > 8.5){
                            txt_quantity.setText("15 ml");
                        } else {
                            txt_weight.setText("0");
                        }
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
*/
            ed_btn_cancel.add(medicine_count);

            ed_txt_item_name.add(txt_item_name);
            ed_txt_quantity.add(txt_quantity);
            //ed_txt_days.add(txt_days);

            //ed_spin_type.add(spin_type);

            /*ed_chk_morning.add(chk_morning);
            ed_chk_afternoon.add(chk_afternoon);
            ed_chk_evening.add(chk_evening);*/

            ed_rd_days.add(rd_days);

            btn_add_medicine.setText("Add Medicine");

            btn_cancel1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    int id = btn_cancel1.getId();

                    if(id == 0){

                        ed_txt_item_name.remove(id);
                        ed_spin_type.remove(id);
                        ed_rd_days.remove(id);
                        ed_txt_quantity.remove(id);
                        ed_txt_days.remove(id);
                        ed_chk_morning.remove(id);
                        ed_chk_afternoon.remove(id);
                        ed_chk_evening.remove(id);

                        medicine_count = 0;

                    } else {

                        if(!ed_txt_item_name.isEmpty()){
                            ed_txt_item_name.remove(id);
                        }

                        if(ed_spin_type.isEmpty()){
                            ed_spin_type.remove(id);
                        }

                        if(ed_rd_days.isEmpty()){
                            ed_rd_days.remove(id);
                        }

                        if(!ed_txt_quantity.isEmpty()){
                            ed_txt_quantity.remove(id);
                        }

                        if(!ed_txt_days.isEmpty()){
                            ed_txt_days.remove(id);
                        }

                        if(!ed_chk_morning.isEmpty()){
                            ed_chk_morning.remove(id);
                        }

                        if(!ed_chk_afternoon.isEmpty()){
                            ed_chk_afternoon.remove(id);
                        }

                        if(!ed_chk_evening.isEmpty()){
                            ed_chk_evening.remove(id);
                        }
                    }
                    container.removeView(addView);
                }
            });
            container.addView(addView);
            //addView.show();
        }
        medicine_count = medicine_count + 1;
    }


    private void MedicineName_List_API() {
        // Tag used to cancel the request
        showpDialog();
        StringRequest strReq = new StringRequest(Request.Method.POST,
                RestInterface.URL_MedicineName, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Add Medicine Response: " + response);
                hidepDialog();
                try {
                    JSONObject jObj = new JSONObject(response);
                    String success = jObj.getString("success");

                    if (success.equals("1")) {

                        JSONArray requestarray = jObj.getJSONArray("data");

                        if (requestarray.equals("[]")) {

                            Toast.makeText(SampleActivity.this, "Not Any Medicine Name Found!", Toast.LENGTH_SHORT).show();

                        } else {

                            medicine_nameModelArrayList = new ArrayList<Medicine_NameModel>();
                            medicine_nameModelArrayList.clear();

                            responseList = new ArrayList<String>();

                            for (int i = 0; i < requestarray.length(); i++) {

                                Medicine_NameModel medicine_nameModels = new Medicine_NameModel();
                                JSONObject req = requestarray.getJSONObject(i);

                                medicine_nameModels.setId(req.optString(TAG_MEDICINE_ID));
                                medicine_nameModels.setName(req.optString(TAG_MEDICINE_NAME));

                                medicine_nameModelArrayList.add(medicine_nameModels);
                                responseList.add(req.optString(TAG_MEDICINE_NAME));
                            }

                            ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_dropdown_item_1line, responseList);
                            txt_item_name.setThreshold(1);
                            txt_item_name.setAdapter(adapter);

                            txt_item_name.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                @Override
                                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                                    for (int i = 0; i < medicine_nameModelArrayList.size(); i++) {

                                        String item_name = txt_item_name.getText().toString();

                                        if (item_name.equals(medicine_nameModelArrayList.get(i).getName())) {

                                            med_id = medicine_nameModelArrayList.get(i).getId();
                                        }
                                    }
                                }
                            });
                        }

                    } else {
                        hidepDialog();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
                hidepDialog();
            }
        }) {

            @Override
            protected java.util.Map<String, String> getParams() {
                // Posting params to register url
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", user_id);
                return params;
            }
        };

        strReq.setRetryPolicy(new DefaultRetryPolicy(
                5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // Adding request to request queue
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        strReq.setShouldCache(false);
        requestQueue.add(strReq);
    }


    private void Dialog_Success(String value) {

        // dailog_alert dialog
        final Dialog dialog = new Dialog(SampleActivity.this);
        dialog.setContentView(R.layout.dailog_box);

        // set the dailog_alert dialog components - text, image and button
        ImageView image = (ImageView) dialog.findViewById(R.id.image);
        image.setImageDrawable(getResources().getDrawable(R.drawable.donemark));
        image.setMaxHeight(50);
        image.setMaxWidth(50);

        TextView tv_title = (TextView) dialog.findViewById(R.id.tv_title);
        tv_title.setText("Add Medicine");

        TextView text = (TextView) dialog.findViewById(R.id.tv_content);
        text.setText(value);

        Button dialogButton = (Button) dialog.findViewById(R.id.dialogButtonOK);
        // if button is clicked, close the dailog_alert dialog
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();

                Intent intent_Write_Prescription = new Intent(getApplicationContext(), Write_Prescription.class);
                intent_Write_Prescription.putExtra("appointment_id", appointment_id);
                startActivity(intent_Write_Prescription);
            }
        });

        dialog.show();
    }

    private void Dialog_Failed(String value) {

        // dailog_alert dialog
        final Dialog dialog = new Dialog(SampleActivity.this);
        dialog.setContentView(R.layout.dailog_box);

        // set the dailog_alert dialog components - text, image and button
        ImageView image = (ImageView) dialog.findViewById(R.id.image);
        image.setImageDrawable(getResources().getDrawable(R.drawable.ic_error_red_24dp));
        image.setMaxHeight(50);
        image.setMaxWidth(50);

        TextView tv_title = (TextView) dialog.findViewById(R.id.tv_title);
        tv_title.setText("Add Medicine");

        TextView text = (TextView) dialog.findViewById(R.id.tv_content);
        text.setText(value);

        Button dialogButton = (Button) dialog.findViewById(R.id.dialogButtonOK);
        // if button is clicked, close the dailog_alert dialog
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });

        dialog.show();
    }



    private void showpDialog() {

        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Please wait...");
        pDialog.setCancelable(false);

        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hidepDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
}
