package codeplateau.com.easyprescribe.Fragment;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import codeplateau.com.easyprescribe.Activity.DrawerLayoutActivity;
import codeplateau.com.easyprescribe.Activity.Login;
import codeplateau.com.easyprescribe.R;
import codeplateau.com.easyprescribe.Services.RestInterface;

/**
 * A simple {@link Fragment} subclass.
 */
public class Logout_Fragment extends Fragment {

    SharedPreferences pref;

    public Logout_Fragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        pref = getContext().getSharedPreferences(RestInterface.pref, Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = pref.edit();
        editor.clear();
        editor.apply();
        editor.commit(); // commit changes

        Intent intent_Login = new Intent(getContext(), Login.class);
        startActivity(intent_Login);

        return inflater.inflate(R.layout.fragment_logout, container, false);
    }

}
