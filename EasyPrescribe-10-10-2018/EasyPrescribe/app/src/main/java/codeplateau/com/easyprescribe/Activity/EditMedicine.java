package codeplateau.com.easyprescribe.Activity;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import codeplateau.com.easyprescribe.Model.Medicine_NameModel;
import codeplateau.com.easyprescribe.R;
import codeplateau.com.easyprescribe.Services.RestInterface;
import codeplateau.com.easyprescribe.Services.Utils;

public class EditMedicine extends AppCompatActivity {

    private static AutoCompleteTextView txt_item_name;
    private static RadioGroup rd_dose, rd_Duration, rd_instruction, rd_days;
    private Button btn_add_medicine, btn_submit, btn_cancel, btn_cancel1;
    private static EditText txt_distribution;
    LinearLayout container;
    private String med_id, appointment_id, user_id;
    Toolbar toolbar;
    SharedPreferences pref;
    int medicine_count = 0;
    private ProgressDialog pDialog;
    private String TAG = Utils.Login_Fragment;
    private StringBuilder strdays = new StringBuilder();
    private StringBuilder strdose = new StringBuilder();
    private StringBuilder strduration = new StringBuilder();
    private StringBuilder strinstruction = new StringBuilder();
    private StringBuilder strdistribution = new StringBuilder();
    public int i;

    ArrayAdapter adapter;

    static List<AutoCompleteTextView> ed_txt_item_name = new ArrayList<AutoCompleteTextView>();

    static List<EditText> ed_distribution = new ArrayList<EditText>();

    static List<String> rd_days1 = new ArrayList<String>();

    static List<String> rd_dose1 = new ArrayList<String>();

    static List<String> rd_duration1 = new ArrayList<String>();

    static List<String> rd_instruction1 = new ArrayList<String>();


    private static RadioButton radio_Duration, radio_dose, radio_instruction, radio_days;

    private static String TAG_MEDICINE_ID = "id";
    private static String TAG_MEDICINE_NAME = "name";
    public int item_count = 0;

    final static ArrayList<Medicine_NameModel> medicine_nameModelArrayList = new ArrayList<Medicine_NameModel>();
    List<String> responseList = new ArrayList<String>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_medicine);


        initViews();
        setListeners();
    }

    private void initViews() {

        container = (LinearLayout) findViewById(R.id.container);

        btn_add_medicine = (Button) findViewById(R.id.btn_add_medicine);
        btn_submit = (Button) findViewById(R.id.btn_submit);
        btn_cancel = (Button) findViewById(R.id.btn_cancel);
        btn_cancel1 = (Button) findViewById(R.id.btn_cancel1);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        pref = getApplicationContext().getSharedPreferences(RestInterface.pref, Context.MODE_PRIVATE);
        user_id = pref.getString("user_id", "");

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            appointment_id = extras.getString("appointment_id");
        }
    }

    private void setListeners() {

        MedicineName_List_API();
        Add_Medicine_Detail();

        btn_add_medicine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Add_Medicine_Detail();
            }
        });

        btn_cancel1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SaveMedicineDetail();
            }
        });
    }

    private void SaveMedicineDetail() {

        int count = ed_txt_item_name.size();

        System.out.println("Count: " + count);

        StringBuilder strMed = new StringBuilder();

        for (int i = 0; i < ed_txt_item_name.size(); i++) {

            strMed.append(ed_txt_item_name.get(i).getText().toString().trim());
            strMed.append(",");
        }

        for (int i = 0; i < ed_distribution.size(); i++) {

            strdistribution.append(ed_distribution.get(i).getText().toString().trim());
            strdistribution.append(",");

        }

        for (int i = 0; i < rd_days1.size(); i++) {

            strdays.append(rd_days1.get(i));
            strdays.append(",");
        }

        for (int i = 0; i < rd_dose1.size(); i++) {

            strdose.append(rd_dose1.get(i));
            strdose.append(",");
        }

        for (int i = 0; i < rd_duration1.size(); i++) {

            strduration.append(rd_duration1.get(i));
            strduration.append(",");
        }

        for (int i = 0; i < rd_instruction1.size(); i++) {

            strinstruction.append(rd_instruction1.get(i));
            strinstruction.append(",");
        }

        AddPrescriptionCode();
    }


    private void AddPrescriptionCode() {

        showpDialog();

        StringRequest strReq = new StringRequest(Request.Method.POST, RestInterface.URL_AddPrescription, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Login Response: " + response);
                hidepDialog();
                try {
                    JSONObject jObj = new JSONObject(response);
                    String success = jObj.getString("success");

                    if (success.equals("1")) {

                        String message = jObj.getString("message");
                        Dialog_Success(message);

                        Intent intent_MedicineList = new Intent(getApplicationContext(), PrintActivity.class);
                        intent_MedicineList.putExtra("appointment_id", appointment_id);
                        intent_MedicineList.putExtra("user_id", user_id);
                        intent_MedicineList.putExtra("id", "2");
                        startActivity(intent_MedicineList);

                    } else {

                        String message = jObj.getString("message");
                        Dialog_Failed(message);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Error: " + error.getMessage());
                hidepDialog();
            }
        }) {
            @Override
            protected java.util.Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("med_id", med_id);
                params.put("instruction", strinstruction.toString());
                params.put("dose", strdose.toString());
                params.put("unit", "");
                params.put("direction", "");
                params.put("duration", strduration.toString());
                params.put("user_id", user_id);
                params.put("appointment_id", appointment_id);
                params.put("patient_id", user_id);
                params.put("days", strdays.toString());
                params.put("description", strdistribution.toString());
                return params;
            }
        };

        strReq.setRetryPolicy(new DefaultRetryPolicy(5000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // Adding request to request queue
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        strReq.setShouldCache(false);
        requestQueue.add(strReq);
    }

    private void Add_Medicine_Detail() {

        for (i = medicine_count; i <= medicine_count; i++) {

            LayoutInflater layoutInflater = (LayoutInflater) getApplicationContext().getSystemService(getApplicationContext().LAYOUT_INFLATER_SERVICE);
            final View addView = layoutInflater.inflate(R.layout.field, null);

            txt_item_name = (AutoCompleteTextView) addView.findViewById(R.id.txt_item_name);

            final Button btn_cancel1 = (Button) addView.findViewById(R.id.btn_cancel1);
            rd_Duration = (RadioGroup) addView.findViewById(R.id.rd_Duration);
            rd_dose = (RadioGroup) addView.findViewById(R.id.rd_dose);
            rd_instruction = (RadioGroup) addView.findViewById(R.id.rd_instruction);
            rd_days = (RadioGroup) addView.findViewById(R.id.rd_days);
            txt_distribution = (EditText) addView.findViewById(R.id.txt_distribution);

            txt_item_name.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                    for (int i = 0; i < medicine_nameModelArrayList.size(); i++) {

                        String item_name = txt_item_name.getText().toString();

                        if (item_name.equals(medicine_nameModelArrayList.get(i).getName())) {

                            med_id = medicine_nameModelArrayList.get(i).getId();

                        }
                    }
                }
            });

            rd_dose1.add("Tablet");

            rd_dose.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {

                    int selectedIddose = rd_dose.getCheckedRadioButtonId();
                    radio_dose = (RadioButton) addView.findViewById(selectedIddose);
                    String dose = radio_dose.getText().toString();

                    rd_dose1.add(dose);
                }
            });

            //rd_days1.add("1");

            rd_days.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {

                    int selectedIddays = rd_days.getCheckedRadioButtonId();
                    radio_days = (RadioButton) addView.findViewById(selectedIddays);
                    String days = radio_days.getText().toString();

                    rd_days1.add(days);
                }
            });

            rd_duration1.add("1 times");

            rd_Duration.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {

                    int selectedIdduration = rd_Duration.getCheckedRadioButtonId();
                    radio_Duration = (RadioButton) addView.findViewById(selectedIdduration);
                    String duration = radio_Duration.getText().toString();

                    rd_duration1.add(duration);
                }
            });


            rd_instruction1.add("After food");

            rd_instruction.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {

                    int selectedIdinstruction = rd_instruction.getCheckedRadioButtonId();
                    radio_instruction = (RadioButton) addView.findViewById(selectedIdinstruction);
                    String instruction = radio_instruction.getText().toString();

                    rd_instruction1.add(instruction);
                }
            });

            txt_item_name.setId(i);
            txt_distribution.setId(i);
            rd_Duration.setId(i);
            rd_instruction.setId(i);
            rd_days.setId(i);
            rd_dose.setId(i);

            if (!medicine_nameModelArrayList.isEmpty()) {

                txt_item_name.setThreshold(1);
                txt_item_name.setAdapter(adapter);
            }

            txt_item_name.requestFocus();
            InputMethodManager imm = (InputMethodManager) getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            ed_txt_item_name.add(txt_item_name);
            ed_distribution.add(txt_distribution);

            container.addView(addView);
        }
        medicine_count = medicine_count + 1;
    }


    private void MedicineName_List_API() {
        // Tag used to cancel the request
        showpDialog();
        StringRequest strReq = new StringRequest(Request.Method.POST, RestInterface.URL_MedicineName, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Add Medicine Response: " + response);
                hidepDialog();
                try {
                    JSONObject jObj = new JSONObject(response);
                    String success = jObj.getString("success");

                    if (success.equals("1")) {

                        JSONArray requestarray = jObj.getJSONArray("data");

                        if (requestarray.equals("[]")) {

                            Toast.makeText(EditMedicine.this, "Not Any Medicine Name Found!", Toast.LENGTH_SHORT).show();

                        } else {

                            // medicine_nameModelArrayList = new ArrayList <Medicine_NameModel>();
                            medicine_nameModelArrayList.clear();

                            responseList = new ArrayList<String>();

                            for (int i = 0; i < requestarray.length(); i++) {

                                Medicine_NameModel medicine_nameModels = new Medicine_NameModel();
                                JSONObject req = requestarray.getJSONObject(i);

                                medicine_nameModels.setId(req.optString(TAG_MEDICINE_ID));
                                medicine_nameModels.setName(req.optString(TAG_MEDICINE_NAME));

                                medicine_nameModelArrayList.add(medicine_nameModels);
                                responseList.add(req.optString(TAG_MEDICINE_NAME));
                            }

                            adapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_dropdown_item_1line, responseList);
                            txt_item_name.setThreshold(1);
                            txt_item_name.setAdapter(adapter);

                        }

                    } else {
                        hidepDialog();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
                hidepDialog();
            }
        }) {

            @Override
            protected java.util.Map<String, String> getParams() {
                // Posting params to register url
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", user_id);
                return params;
            }
        };

        strReq.setRetryPolicy(new DefaultRetryPolicy(5000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // Adding request to request queue
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        strReq.setShouldCache(false);
        requestQueue.add(strReq);
    }


    private void Dialog_Success(String value) {

        // dailog_alert dialog
        final Dialog dialog = new Dialog(EditMedicine.this);
        dialog.setContentView(R.layout.dailog_box);

        // set the dailog_alert dialog components - text, image and button
        ImageView image = (ImageView) dialog.findViewById(R.id.image);
        image.setImageDrawable(getResources().getDrawable(R.drawable.donemark));
        image.setMaxHeight(50);
        image.setMaxWidth(50);

        TextView tv_title = (TextView) dialog.findViewById(R.id.tv_title);
        tv_title.setText("Add Medicine");

        TextView text = (TextView) dialog.findViewById(R.id.tv_content);
        text.setText(value);

        Button dialogButton = (Button) dialog.findViewById(R.id.dialogButtonOK);
        // if button is clicked, close the dailog_alert dialog
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();

                Intent intent_Write_Prescription = new Intent(getApplicationContext(), Write_Prescription.class);
                intent_Write_Prescription.putExtra("appointment_id", appointment_id);
                startActivity(intent_Write_Prescription);
            }
        });

        dialog.show();
    }

    private void Dialog_Failed(String value) {
        // dailog_alert dialog
        final Dialog dialog = new Dialog(EditMedicine.this);
        dialog.setContentView(R.layout.dailog_box);

        // set the dailog_alert dialog components - text, image and button
        ImageView image = (ImageView) dialog.findViewById(R.id.image);
        image.setImageDrawable(getResources().getDrawable(R.drawable.ic_error_red_24dp));
        image.setMaxHeight(50);
        image.setMaxWidth(50);

        TextView tv_title = (TextView) dialog.findViewById(R.id.tv_title);
        tv_title.setText("Add Medicine");

        TextView text = (TextView) dialog.findViewById(R.id.tv_content);
        text.setText(value);

        Button dialogButton = (Button) dialog.findViewById(R.id.dialogButtonOK);
        // if button is clicked, close the dailog_alert dialog
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });

        dialog.show();
    }


    private void showpDialog() {

        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Please wait...");
        pDialog.setCancelable(false);

        if (!pDialog.isShowing()) pDialog.show();
    }

    private void hidepDialog() {
        if (pDialog.isShowing()) pDialog.dismiss();
    }


    @Override
    public void onBackPressed() {

        Intent intent_Write_Prescription = new Intent(getApplicationContext(), Write_Prescription.class);
        intent_Write_Prescription.putExtra("appointment_id", appointment_id);
        startActivity(intent_Write_Prescription);
    }
}
