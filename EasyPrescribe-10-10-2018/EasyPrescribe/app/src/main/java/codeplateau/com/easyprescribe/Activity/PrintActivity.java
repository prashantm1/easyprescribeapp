package codeplateau.com.easyprescribe.Activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.print.PrintAttributes;
import android.print.PrintDocumentAdapter;
import android.print.PrintManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;

import codeplateau.com.easyprescribe.R;
import codeplateau.com.easyprescribe.Services.RestInterface;

import static codeplateau.com.easyprescribe.Services.RestInterface.URL_PRINTBILL;

public class PrintActivity extends AppCompatActivity {


    private static View view;
    private static WebView print;
    private String  appointment_id;
    Button btn_home,btn_print;

    SharedPreferences pref;
    private String print_url;

    @Override
    protected void onCreate (Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_print);
        initViews();
        SetListener();
    }

    private void initViews(){

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            appointment_id = extras.getString("appointment_id");
        }

        pref = getApplicationContext().getSharedPreferences(RestInterface.pref, Context.MODE_PRIVATE);

        print = (WebView) findViewById(R.id.print);

        print.getSettings().setJavaScriptEnabled(true);
        print.setWebChromeClient(new WebChromeClient());

        btn_print = (Button)findViewById(R.id.btn_print);
        btn_home = (Button)findViewById(R.id.btn_home);

        print_url = RestInterface.URL_PRINTBILL+appointment_id;
        print.loadUrl(print_url);

        doWebViewPrint();

        btn_print.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                doWebViewPrint();
            }
        });

    }

    private void doWebViewPrint() {
        // Create a WebView object specifically for printing
        final WebView webView = new WebView(getApplicationContext());
        webView.setWebViewClient(new WebViewClient() {

            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return false;
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                Log.i("Print", "page finished loading " + url);
                createWebPrintJob(view);
            }
        });

        print_url = RestInterface.URL_PRINTBILL+appointment_id;
        webView.loadUrl(print_url);
        print = webView;
    }

    private void createWebPrintJob(WebView webView) {

        // Get a PrintManager instance
        PrintManager printManager = (PrintManager) PrintActivity.this.getSystemService(PrintActivity.this.PRINT_SERVICE);

        // Get a print adapter instance
        PrintDocumentAdapter printAdapter = webView.createPrintDocumentAdapter();

        // Create a print job with name and adapter instance
        String jobName = getString(R.string.app_name) + " Document";
        printManager.print(jobName, printAdapter, new PrintAttributes.Builder().build());
    }


    private void SetListener(){

    }

    @Override
    public void onBackPressed() {
        Intent intent_DrawerLayoutActivity = new Intent(getApplicationContext(), DrawerLayoutActivity.class);
        startActivity(intent_DrawerLayoutActivity);
    }
}
