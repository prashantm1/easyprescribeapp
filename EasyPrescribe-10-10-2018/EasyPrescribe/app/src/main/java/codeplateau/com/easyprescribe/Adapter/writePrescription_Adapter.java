package codeplateau.com.easyprescribe.Adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import codeplateau.com.easyprescribe.Activity.Edit_MedicineActivity;
import codeplateau.com.easyprescribe.Activity.Write_Prescription;
import codeplateau.com.easyprescribe.Model.Medicine_ListModel;
import codeplateau.com.easyprescribe.R;
import codeplateau.com.easyprescribe.Services.RestInterface;

import static com.android.volley.VolleyLog.TAG;

public class writePrescription_Adapter extends RecyclerView.Adapter<writePrescription_Adapter.ViewHolder> {

    private ArrayList<Medicine_ListModel> medicine_listModelArrayList;
    private Context context;
    private ProgressDialog pDialog;
    private String id, appointment_id;

    public writePrescription_Adapter(Context context, ArrayList<Medicine_ListModel> medicine_listModels) {
        this.medicine_listModelArrayList = medicine_listModels;
        this.context = context;
    }

    @Override
    public writePrescription_Adapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {

        View view;
        view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_write_prescription, viewGroup, false);

        return new writePrescription_Adapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final writePrescription_Adapter.ViewHolder viewHolder, final int i) {

        String iconurl = "https://www.frenzybet.com/uploads/icons/" + medicine_listModelArrayList.get(i).getMedicine();


        viewHolder.txt_medicinename.setText(medicine_listModelArrayList.get(i).getMedicine());

        String dose = medicine_listModelArrayList.get(i).getDose();
        String duration = medicine_listModelArrayList.get(i).getDuration();

        viewHolder.tv_description.setText(dose + " " + duration + "per day");

        viewHolder.img_edit.setOnClickListener(new View.OnClickListener() {

            String id = medicine_listModelArrayList.get(i).getId();
            String med_id = medicine_listModelArrayList.get(i).getMed_id();
            String patient_id = medicine_listModelArrayList.get(i).getPatient_id();
            String appointment_id = medicine_listModelArrayList.get(i).getAppointment_id();
            String dose = medicine_listModelArrayList.get(i).getDose();
            String instruction = medicine_listModelArrayList.get(i).getInstruction();
            String duration = medicine_listModelArrayList.get(i).getDuration();
            String directions = medicine_listModelArrayList.get(i).getDirections();
            String medicine = medicine_listModelArrayList.get(i).getMedicine();
            String user_id = medicine_listModelArrayList.get(i).getId();


            @Override
            public void onClick(View v) {

                Intent intent_MedicineList = new Intent(context.getApplicationContext(), Edit_MedicineActivity.class);
                intent_MedicineList.putExtra("id", id);
                intent_MedicineList.putExtra("med_id", med_id);
                intent_MedicineList.putExtra("patient_id", patient_id);
                intent_MedicineList.putExtra("appointment_id", appointment_id);
                intent_MedicineList.putExtra("dose", dose);
                intent_MedicineList.putExtra("instruction", instruction);
                intent_MedicineList.putExtra("duration", duration);
                intent_MedicineList.putExtra("directions", directions);
                intent_MedicineList.putExtra("medicine", medicine);
                intent_MedicineList.putExtra("user_id", user_id);
                context.startActivity(intent_MedicineList);
            }
        });

        viewHolder.img_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                id = medicine_listModelArrayList.get(i).getId();
                appointment_id = medicine_listModelArrayList.get(i).getAppointment_id();
                Delete_PrescriptionAPI(id);
            }
        });


    }

    @Override
    public int getItemCount() {
        return medicine_listModelArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView txt_medicinename, tv_description;
        ImageView img_edit, img_delete;
        LinearLayout ll_write_prescription;

        public ViewHolder(View view) {
            super(view);

            txt_medicinename = (TextView) view.findViewById(R.id.tv_medicinename);
            tv_description = (TextView) view.findViewById(R.id.tv_description);
            img_edit = (ImageView) view.findViewById(R.id.img_edit);
            img_delete = (ImageView) view.findViewById(R.id.img_delete);
            ll_write_prescription = (LinearLayout) view.findViewById(R.id.ll_write_prescription);
        }
    }

    private void Delete_PrescriptionAPI(final String id) {
        showpDialog();
        StringRequest strReq = new StringRequest(Request.Method.POST,
                RestInterface.URL_DeletePrescription, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Login Response: " + response);
                //hideProgressDialog();
                hidepDialog();
                try {
                    JSONObject jObj = new JSONObject(response);
                    String success = jObj.getString("success");

                    if (success.equals("1")) {

                        //      JSONObject jsonObject = jObj.getJSONObject("data");

                        String message = jObj.getString("message");
                        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();

                        Intent intent_Notifications = new Intent(context, Write_Prescription.class);
                        context.startActivity(intent_Notifications);


                    } else {

                        String message = jObj.getString("message");
                        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();

                        hidepDialog();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Error: " + error.getMessage());
                //Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
                hidepDialog();
            }
        }) {

            @Override
            protected java.util.Map<String, String> getParams() {
                // Posting params to register url
                Map<String, String> params = new HashMap<String, String>();

                params.put("id", id);

                return params;
            }
        };

        strReq.setRetryPolicy(new DefaultRetryPolicy(
                5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // Adding request to request queue
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        strReq.setShouldCache(false);
        requestQueue.add(strReq);
    }

    private void showpDialog() {

        // Progress Dialog
        pDialog = new ProgressDialog(this.context);
        pDialog.setMessage("Please wait...");
        pDialog.setCancelable(false);

        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hidepDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
}




