package codeplateau.com.easyprescribe.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import codeplateau.com.easyprescribe.Services.RestInterface;
import codeplateau.com.easyprescribe.R;


public class Login extends AppCompatActivity {

    private static EditText txt_username, txt_password;
    private static Button btn_login;

    private ProgressDialog pDialog;
    private TextInputLayout input_layout_username, input_layout_password;

    private static final String TAG = Login.class.getSimpleName();

    public static String userid,name,email,phone;
    SharedPreferences pref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        initViews();
        setListeners();
    }

    private void initViews() {

        txt_username = (EditText) findViewById(R.id.txt_username);
        txt_password = (EditText) findViewById(R.id.txt_password);

        btn_login = (Button) findViewById(R.id.btn_login);

        input_layout_username = (TextInputLayout) findViewById(R.id.input_layout_username);
        input_layout_password = (TextInputLayout) findViewById(R.id.input_layout_password);
    }

    private void setListeners() {

        pref = getApplicationContext().getSharedPreferences(RestInterface.pref, Context.MODE_PRIVATE);

        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (txt_username.getText().toString().equals("")) {
                    Toast.makeText(getApplicationContext(), "Enter Username", Toast.LENGTH_LONG).show();
                } else if (txt_password.getText().toString().equals("")) {
                    Toast.makeText(getApplicationContext(), "Enter Password", Toast.LENGTH_LONG).show();
                } else {
                    Login_API();
                }
            }
        });

    }

    private void Login_API() {

        showpDialog();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                RestInterface.URL_Login, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Login Response: " + response);

                hidepDialog();

                try {
                    JSONObject jObj = new JSONObject(response);
                    String success = jObj.getString("success");

                    if (success.equals("1")) {

                        JSONArray jsonArray = jObj.getJSONArray("data");

                        for (int i = 0; i < jsonArray.length(); i++){

                            JSONObject jsonObject = jsonArray.getJSONObject(i);

                            userid = jsonObject.optString("id");
                            name = jsonObject.optString("name");
                            email = jsonObject.optString("email");
                            phone = jsonObject.optString("phone");

                            pref = getApplicationContext().getSharedPreferences(RestInterface.pref, Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor = pref.edit();
                            editor.putString("user_id", userid);
                            editor.putString("name", name);
                            editor.putString("email", email);
                            editor.putString("phone", phone);
                            editor.apply();
                            editor.commit();
                        }

                        txt_username.setText("");
                        txt_password.setText("");

                        String data = jObj.getString("message");
                        Toast.makeText(getApplicationContext(), data, Toast.LENGTH_SHORT).show();

                        Intent intent_DrawerLayoutActivity = new Intent(getApplicationContext(), DrawerLayoutActivity.class);
                        startActivity(intent_DrawerLayoutActivity);

                    } else {

                        String message = jObj.getString("message");
                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();

                        hidepDialog();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Error: " + error.getMessage());
                //Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
                hidepDialog();
            }
        }) {

            @Override
            protected java.util.Map<String, String> getParams() {
                // Posting params to register url
                Map<String, String> params = new HashMap<String, String>();
                params.put("email", txt_username.getText().toString().trim());
                params.put("password", txt_password.getText().toString().trim());

                return params;
            }
        };

        strReq.setRetryPolicy(new DefaultRetryPolicy(
                5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // Adding request to request queue
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        strReq.setShouldCache(false);
        requestQueue.add(strReq);
    }

    private void showpDialog() {

        // Progress Dialog
        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Please wait...");
        pDialog.setCancelable(false);

        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hidepDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    private static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    private boolean validateEmail() {
        String email = txt_username.getText().toString().trim();

        if (email.isEmpty() || !isValidEmail(email)) {
            input_layout_username.setError(getString(R.string.err_msg_email));
            requestFocus(txt_username);
            return false;
        } else {
            input_layout_username.setErrorEnabled(false);
        }
        return true;
    }

    private boolean validatePassword() {
        if (txt_password.getText().toString().trim().isEmpty()) {
            input_layout_password.setError(getString(R.string.err_msg_password));
            requestFocus(input_layout_password);
            return false;
        } else {
            input_layout_password.setErrorEnabled(false);
        }

        return true;
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();

        Intent intent_Login = new Intent(getApplicationContext(), MainScreen.class);
        startActivity(intent_Login);
    }
}
