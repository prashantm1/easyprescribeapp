package codeplateau.com.striskfinalproject.Services;

public class MarshmallowIntentId {

    public static final int WRITE_EXTERNAL_STORAGE_PERMISSION = 1;
    public static final int READ_SMS_INTENT_ID = 2;
    public static final int ACCESS_FINE_LOCATION_INTENT_ID = 3;
    public static final int ACCESS_CONTACTS_INTENT_ID = 4;
    public static final int ACCESS_CAMERA_PERMISSION = 5;
}
