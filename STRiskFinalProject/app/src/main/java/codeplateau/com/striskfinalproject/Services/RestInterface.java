package codeplateau.com.striskfinalproject.Services;

public class RestInterface {

    // Shared Preference
    public static String pref = "STRisk123456";

    // Domain Name
    public static String domain_name = "http://216.10.240.90/~striskconsulting/";

    // Login
    public static String url_login = domain_name + "api/users/login";

    // Customer List
    public static String url_customerlist = domain_name + "api/customers/getcustomerbyid";

    // Customer List By Userid
    public static String url_customerlist_by_userId = domain_name + "api/customers/searchcustomerbyid";

    // Add Customer Details
    public static String url_add_customerdetails = domain_name + "api/customers/update_customer";

    // Image Upload URL
    public static String url_imageupload = "http://216.10.240.90/~striskconsulting/fileUpload.php";
}
