package codeplateau.com.striskfinalproject.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import codeplateau.com.striskfinalproject.Activity.AddCustomerDetail;
import codeplateau.com.striskfinalproject.Model.CustomerDetail_Model;
import codeplateau.com.striskfinalproject.R;


public class CustomerDetail_Adapter extends RecyclerView.Adapter<CustomerDetail_Adapter.ViewHolder> {


    private ArrayList<CustomerDetail_Model> customerDetail_modelArrayList;
    private Context context;

    public CustomerDetail_Adapter(Context context, ArrayList<CustomerDetail_Model> taskList_models, String s, String s1, String s2) {
        this.customerDetail_modelArrayList = taskList_models;
        this.context = context;
    }

    @Override
    public CustomerDetail_Adapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {

        View view;
        view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_customerdetails, viewGroup, false);

        return new CustomerDetail_Adapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final CustomerDetail_Adapter.ViewHolder viewHolder, final int i) {

        viewHolder.tv_cino.setText(customerDetail_modelArrayList.get(i).getCino());
        viewHolder.tv_name.setText(customerDetail_modelArrayList.get(i).getCustomer_name());
        viewHolder.tv_address.setText(customerDetail_modelArrayList.get(i).getAddress());
        viewHolder.tv_mobileno.setText(customerDetail_modelArrayList.get(i).getCustomer_mobileno());

        viewHolder.layout_customerdetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String id = customerDetail_modelArrayList.get(i).getId();
                String cino = customerDetail_modelArrayList.get(i).getCino();
                String customer_name = customerDetail_modelArrayList.get(i).getCustomer_name();
                String customer_mobileno = customerDetail_modelArrayList.get(i).getCustomer_mobileno();
                String address = customerDetail_modelArrayList.get(i).getAddress();

                Intent intent_CustomerDetail = new Intent(context.getApplicationContext(), AddCustomerDetail.class);
                intent_CustomerDetail.putExtra("id", id);
                intent_CustomerDetail.putExtra("cino", cino);
                intent_CustomerDetail.putExtra("customer_name", customer_name);
                intent_CustomerDetail.putExtra("customer_mobileno", customer_mobileno);
                intent_CustomerDetail.putExtra("address", address);
                intent_CustomerDetail.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent_CustomerDetail);
            }
        });

    }

    @Override
    public int getItemCount() {

        return customerDetail_modelArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tv_cino, tv_name, tv_address, tv_mobileno;
        LinearLayout layout_customerdetail;

        public ViewHolder(View view) {
            super(view);

            tv_cino = view.findViewById(R.id.tv_cino);
            tv_name = view.findViewById(R.id.tv_name);
            tv_address = view.findViewById(R.id.tv_address);
            tv_mobileno = view.findViewById(R.id.tv_mobileno);
            layout_customerdetail = view.findViewById(R.id.layout_customerdetail);
        }
    }
}
