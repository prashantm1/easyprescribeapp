package codeplateau.com.striskfinalproject.Activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.database.Cursor;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NavUtils;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import net.gotev.uploadservice.MultipartUploadRequest;
import net.gotev.uploadservice.UploadNotificationConfig;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;

import codeplateau.com.striskfinalproject.R;
import codeplateau.com.striskfinalproject.Services.GPSTracker;
import codeplateau.com.striskfinalproject.Services.MarshMallowPermission;
import codeplateau.com.striskfinalproject.Services.MarshmallowIntentId;
import codeplateau.com.striskfinalproject.Services.RestInterface;

import static codeplateau.com.striskfinalproject.Services.RestInterface.url_imageupload;

public class AddCustomerDetail extends AppCompatActivity implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {
    private final static int SELECT_PHOTO_1 = 101;
    private final static int SELECT_PHOTO_2 = 102;
    private final static int SELECT_PHOTO_3 = 103;
    private final static int SELECT_PHOTO_4 = 104;
    private final static int SELECT_PHOTO_5 = 105;
    private final static int SELECT_PHOTO_6 = 106;
    private static final String TAG = Login.class.getSimpleName();
    //Define a request code to send to Google Play services
    private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
    private static String email;
    private static int PICK_IMAGE_REQUEST = 1;
    private final int PICK_IMAGE_CAMERA_ONE = 1, PICK_IMAGE_GALLERY_TWO = 2;
    private final int PICK_IMAGE_CAMERA_THREE = 3, PICK_IMAGE_GALLERY_FOUR = 4;
    private final int PICK_IMAGE_CAMERA_FIVE = 5, PICK_IMAGE_GALLERY_SIX = 6;
    private final int PICK_IMAGE_CAMERA_SEVEN = 7, PICK_IMAGE_GALLERY_EIGHT = 8;
    private final int PICK_IMAGE_CAMERA_NINE = 9, PICK_IMAGE_GALLERY_TEN = 10;
    private final int PICK_IMAGE_CAMERA_ELEVEN = 11, PICK_IMAGE_GALLERY_TWELVE = 12;
    public String imageiconselect = "0", imageiconselect2 = "0", imageiconselect3 = "0", imageiconselect4 = "0", imageiconselect5 = "0", imageiconselect6 = "0";
    public String remarks_salary_slip, remarks_form_sixteen, remarks_itreturns, business_activity_verification, salary_slip_verification, form_sixteen_verification, itreturns_verification, remarks_pancard_number, pancard_number_verification, remarks_adharnumber, adharnumber_verification, user_id, cino, customer_name, customer_mobileno, address, residence_address_verification, remarks_residence_address, business_address_verification, remarks_business_address, business_phone_verification, remarks_business_phone, residence_phone_verification, remarks_residence_phone, income_verification, remarks_income, bank_statements_verification, remarks_bank_statements, employee_employers_business_activity_verification, remarks_employee_employers_business_activity, document, location;
    public String stringLatitude, stringLongitude;
    public String imagename, imagename2, imagename3, imagename4, imagename5, imagename6;
    Uri imageuri, imageuri2, imageuri3, imageuri4, imageuri5, imageuri6;
    SharedPreferences pref;
    String mId;
    private TextView lbl_name, lbl_phone, lbl_address, lbl_cino;
    private ProgressDialog pDialog;
    private Button btn_submit;
    private ImageView img_upload;
    private String strImage1 = "", strImage2 = "", strImage3 = "", strImage4 = "", strImage5 = "", strImage6 = "";
    private RadioGroup radio_itreturns_verification, radio_salary_slip_verification, radio_form_sixteen_verification, radio_residence_address_verify, radio_business_address, radio_business_phone, radio_residence_phone_verify, radio_income_verification, radio_bank_statement, radio_Employee, radio_name_of_applicant, radio_Adhar_no, radio_pancard;
    private RadioButton rd_residence_yes, rd_residence_no, rd_business_yes, rd_business_no, rd_business_phone_yes, rd_business_phone_no, rd_adhar_no_yes, rd_adhar_no, rd_pancard_yes, rd_pancard_no;
    private RadioButton rd_itreturns_verification_yes, rd_itreturns_verification_no, rd_sixteen_yes, rd_sixteen_no, rd_salary_slip_verification_yes, rd_salary_slip_verification_no, rd_residence_phone_Yes, rd_residence_phone_no, rd_income_yes, rd_income_no, rd_bank_yes, rd_bank_no, rd_employee_yes, rd_employee_no;
    private EditText txt_itreturns_verification, txt_form_sixteen_verification, txt_salary_slip_verification, txt_Income_Verification, txt_residence_address, txt_business_address, txt_business_phone, txt_residence_phone_verify, txt_verification_address, txt_bank_statement, txt_employee_address, txt_Adhar_no, txt_Pan_Card;
    private ImageView ivShopPhoto1, ivShopPhoto2, ivShopPhoto3, ivShopPhoto4, ivShopPhoto5, ivShopPhoto6;
    private RadioButton radio_itreturns_verificationButton, radio_form_sixteen_verificationButton, radio_salary_slip_verificationButton, radioAdharCardButton, radioPanCardButton, radioResidenceButton, radio_businessaddress_Button, radio_business_phoneButton, radio_residence_phone_verify_Button, radio_income_verification_Button, radio_bank_statement_Button, radio_Employee_Button;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private double currentLatitude;
    private double currentLongitude;
    // Upload Code
    private MarshMallowPermission marshMallowPermission;
    //String url = "http://216.10.240.90/~striskconsulting/api/customer";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_customerdetails);

        initViews();
        setListeners();
    }

    private void initViews() {

        Bundle receiveBundle = this.getIntent().getExtras();
        mId = receiveBundle.getString("id");

        marshMallowPermission = new MarshMallowPermission(AddCustomerDetail.this);

        if (!marshMallowPermission.checkPermissionForExternalStorage()) {
            marshMallowPermission.requestPermissionForExternalStorage(MarshMallowPermission.EXTERNAL_STORAGE_PERMISSION_REQUEST_CODE_BY_LOAD_PROFILE);
        }

        // Back button
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // Add Title
        getSupportActionBar().setTitle("Add Customer Detail");

        // Google map code
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                // The next two lines tell the new client that “this” current class will handle connection stuff
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                //fourth line adds the LocationServices API endpoint from GooglePlayServices
                .addApi(LocationServices.API)
                .build();

        // Create the LocationRequest object
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(10 * 1000)        // 10 seconds, in milliseconds
                .setFastestInterval(1 * 1000); // 1 second, in milliseconds

        pref = getSharedPreferences(RestInterface.pref, Context.MODE_PRIVATE);
        email = pref.getString("email", "");
        user_id = pref.getString("id", "");

        Bundle extras = getIntent().getExtras();
        if (extras != null) {

            cino = extras.getString("cino");
            customer_name = extras.getString("customer_name");
            customer_mobileno = extras.getString("customer_mobileno");
            address = extras.getString("address");
        }

        //TextView
        lbl_name = findViewById(R.id.lbl_name);
        lbl_phone = findViewById(R.id.lbl_phone);
        lbl_address = findViewById(R.id.lbl_address);
        lbl_cino = findViewById(R.id.lbl_cino);

        //Button
        btn_submit = findViewById(R.id.btn_submit);

        //Radio Group
        radio_residence_address_verify = findViewById(R.id.radio_residence_address_verify);
        radio_business_address = findViewById(R.id.radio_business_address);
        radio_business_phone = findViewById(R.id.radio_business_phone);
        radio_residence_phone_verify = findViewById(R.id.radio_residence_phone_verify);
        radio_income_verification = findViewById(R.id.radio_income_verification);
        radio_bank_statement = findViewById(R.id.radio_bank_statement);
        radio_Employee = findViewById(R.id.radio_Employee);
        radio_Adhar_no = findViewById(R.id.radio_Adhar_no);
        radio_pancard = findViewById(R.id.radio_pancard);
        radio_form_sixteen_verification = findViewById(R.id.radio_form_sixteen_verification);
        radio_salary_slip_verification = findViewById(R.id.radio_salary_slip_verification);
        radio_itreturns_verification = findViewById(R.id.radio_itreturns_verification);

        rd_adhar_no_yes = findViewById(R.id.rd_adhar_no_yes);
        rd_adhar_no = findViewById(R.id.rd_adhar_no);

        rd_pancard_yes = findViewById(R.id.rd_pancard_yes);
        rd_pancard_no = findViewById(R.id.rd_pancard_no);

        rd_residence_yes = findViewById(R.id.rd_residence_yes);
        rd_residence_no = findViewById(R.id.rd_residence_no);

        rd_business_yes = findViewById(R.id.rd_business_yes);
        rd_business_no = findViewById(R.id.rd_business_no);

        rd_business_phone_yes = findViewById(R.id.rd_business_phone_yes);
        rd_business_phone_no = findViewById(R.id.rd_business_phone_no);

        rd_residence_phone_Yes = findViewById(R.id.rd_residence_phone_Yes);
        rd_residence_phone_no = findViewById(R.id.rd_residence_phone_no);

        rd_income_yes = findViewById(R.id.rd_income_yes);
        rd_income_no = findViewById(R.id.rd_income_no);

        rd_bank_yes = findViewById(R.id.rd_bank_yes);
        rd_bank_no = findViewById(R.id.rd_bank_no);

        rd_employee_yes = findViewById(R.id.rd_employee_yes);
        rd_employee_no = findViewById(R.id.rd_employee_no);

        rd_salary_slip_verification_no = findViewById(R.id.rd_salary_slip_verification_no);
        rd_salary_slip_verification_yes = findViewById(R.id.rd_salary_slip_verification_yes);

        rd_sixteen_no = findViewById(R.id.rd_sixteen_no);
        rd_sixteen_yes = findViewById(R.id.rd_sixteen_yes);

        rd_itreturns_verification_no = findViewById(R.id.rd_itreturns_verification_no);
        rd_itreturns_verification_yes = findViewById(R.id.rd_itreturns_verification_yes);


        txt_Adhar_no = findViewById(R.id.txt_Adhar_no);
        txt_Pan_Card = findViewById(R.id.txt_Pan_Card);

        txt_residence_address = findViewById(R.id.txt_residence_address);
        txt_business_address = findViewById(R.id.txt_business_address);
        txt_business_phone = findViewById(R.id.txt_business_phone);
        txt_residence_phone_verify = findViewById(R.id.txt_residence_phone_verify);
        txt_verification_address = findViewById(R.id.txt_verification_address);
        txt_bank_statement = findViewById(R.id.txt_bank_statement);
        txt_employee_address = findViewById(R.id.txt_employee_address);
        txt_salary_slip_verification = findViewById(R.id.txt_salary_slip_verification);
        txt_form_sixteen_verification = findViewById(R.id.txt_form_sixteen_verification);
        txt_itreturns_verification = findViewById(R.id.txt_itreturns_verification);

        lbl_name.setText(customer_name);
        lbl_phone.setText(customer_mobileno);
        lbl_address.setText(address);
        lbl_cino.setText(cino);

        // check if GPS enabled
        GPSTracker gpsTracker = new GPSTracker(this);

        if (gpsTracker.getIsGPSTrackingEnabled()) {
            stringLatitude = String.valueOf(gpsTracker.latitude);
            stringLongitude = String.valueOf(gpsTracker.longitude);
        } else {
            // can't get location
            // GPS or Network is not enabled
            // Ask user to enable GPS/network in settings
            gpsTracker.showSettingsAlert();
        }

        ivShopPhoto1 = findViewById(R.id.image);
        ivShopPhoto2 = findViewById(R.id.image2);
        ivShopPhoto3 = findViewById(R.id.image3);
        ivShopPhoto4 = findViewById(R.id.image4);
        ivShopPhoto5 = findViewById(R.id.image5);
        ivShopPhoto6 = findViewById(R.id.image6);

    }

    private void setListeners() {

        ivShopPhoto1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                /*Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                photoPickerIntent.setType("image/*");
                startActivityForResult(photoPickerIntent, SELECT_PHOTO_1);*/

                SelectImage_ONE();
            }
        });

        ivShopPhoto2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                /*Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                photoPickerIntent.setType("image/*");
                startActivityForResult(photoPickerIntent, SELECT_PHOTO_2);*/

                SelectImage_TWO();
            }
        });

        ivShopPhoto3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                /*Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                photoPickerIntent.setType("image/*");
                startActivityForResult(photoPickerIntent, SELECT_PHOTO_3);*/

                SelectImage_THREE();
            }
        });

        ivShopPhoto4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                /*Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                photoPickerIntent.setType("image/*");
                startActivityForResult(photoPickerIntent, SELECT_PHOTO_4);*/

                SelectImage_FOUR();
            }
        });

        ivShopPhoto5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                /*Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                photoPickerIntent.setType("image/*");
                startActivityForResult(photoPickerIntent, SELECT_PHOTO_5);*/

                SelectImage_FIVE();
            }
        });

        ivShopPhoto6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                /*Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                photoPickerIntent.setType("image/*");
                startActivityForResult(photoPickerIntent, SELECT_PHOTO_6);*/

                SelectImage_SIX();
            }
        });

        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int selected_salary_slip_verificationButton = radio_salary_slip_verification.getCheckedRadioButtonId();
                radio_salary_slip_verificationButton = findViewById(selected_salary_slip_verificationButton);
                salary_slip_verification = radio_salary_slip_verificationButton.getText().toString();

                int selected_form_sixteen_verificationButton = radio_form_sixteen_verification.getCheckedRadioButtonId();
                radio_form_sixteen_verificationButton = findViewById(selected_form_sixteen_verificationButton);
                form_sixteen_verification = radio_form_sixteen_verificationButton.getText().toString();

                int selected_itreturns_verificationnButton = radio_itreturns_verification.getCheckedRadioButtonId();
                radio_itreturns_verificationButton = findViewById(selected_itreturns_verificationnButton);
                itreturns_verification = radio_itreturns_verificationButton.getText().toString();

                int selectedAdhar_no = radio_Adhar_no.getCheckedRadioButtonId();
                radioAdharCardButton = findViewById(selectedAdhar_no);
                adharnumber_verification = radioAdharCardButton.getText().toString();

                int selectedpancard = radio_pancard.getCheckedRadioButtonId();
                radioPanCardButton = findViewById(selectedpancard);
                pancard_number_verification = radioPanCardButton.getText().toString();

                int selectedId = radio_residence_address_verify.getCheckedRadioButtonId();
                radioResidenceButton = findViewById(selectedId);
                residence_address_verification = radioResidenceButton.getText().toString();

                int selectedbusiness_address = radio_business_address.getCheckedRadioButtonId();
                radio_businessaddress_Button = findViewById(selectedbusiness_address);
                business_address_verification = radio_businessaddress_Button.getText().toString();

                int selectedbusiness_phone = radio_business_phone.getCheckedRadioButtonId();
                radio_business_phoneButton = findViewById(selectedbusiness_phone);
                business_phone_verification = radio_business_phoneButton.getText().toString();

                int selectedresidence_phone_verify = radio_residence_phone_verify.getCheckedRadioButtonId();
                radio_residence_phone_verify_Button = findViewById(selectedresidence_phone_verify);
                residence_phone_verification = radio_residence_phone_verify_Button.getText().toString();

                int selectedincome_verification = radio_income_verification.getCheckedRadioButtonId();
                radio_income_verification_Button = findViewById(selectedincome_verification);
                income_verification = radio_income_verification_Button.getText().toString();

                int selectedbank_statement = radio_bank_statement.getCheckedRadioButtonId();
                radio_bank_statement_Button = findViewById(selectedbank_statement);
                bank_statements_verification = radio_bank_statement_Button.getText().toString();

                int selectedEmployee = radio_Employee.getCheckedRadioButtonId();
                radio_Employee_Button = findViewById(selectedEmployee);
                business_activity_verification = radio_Employee_Button.getText().toString();

                String str_Adhar = adharnumber_verification;
                if (str_Adhar.equals("Yes")) {
                    radio_Adhar_no.check(R.id.rd_adhar_no_yes);
                } else if (str_Adhar.equals("No")) {
                    radio_Adhar_no.check(R.id.rd_adhar_no);
                }

                String str_Pan = pancard_number_verification;
                if (str_Pan.equals("Yes")) {
                    radio_pancard.check(R.id.rd_pancard_yes);
                } else if (str_Pan.equals("No")) {
                    radio_pancard.check(R.id.rd_pancard_no);
                }

                String str_ResiAddress = residence_address_verification;
                if (str_ResiAddress.equals("Yes")) {
                    radio_residence_address_verify.check(R.id.rd_residence_yes);
                } else if (str_ResiAddress.equals("No")) {
                    radio_residence_address_verify.check(R.id.rd_residence_no);
                }

                String str_businessAddress = business_address_verification;
                if (str_businessAddress.equals("Yes")) {
                    radio_business_address.check(R.id.rd_business_yes);
                } else if (str_businessAddress.equals("No")) {
                    radio_business_address.check(R.id.rd_business_no);
                }


                String str_phone_verification = business_phone_verification;
                if (str_phone_verification.equals("Yes")) {
                    radio_business_phone.check(R.id.rd_business_phone_yes);
                } else if (str_phone_verification.equals("No")) {
                    radio_business_phone.check(R.id.rd_business_phone_no);
                }

                String str_residence_phone_verifify = residence_phone_verification;
                if (str_residence_phone_verifify.equals("Yes")) {
                    radio_residence_phone_verify.check(R.id.rd_residence_phone_Yes);
                } else if (str_residence_phone_verifify.equals("No")) {
                    radio_residence_phone_verify.check(R.id.rd_residence_phone_no);
                }

                String str_income_verification = income_verification;
                if (str_income_verification.equals("Yes")) {
                    radio_income_verification.check(R.id.rd_income_yes);
                } else if (str_income_verification.equals("No")) {
                    radio_income_verification.check(R.id.rd_income_no);
                }

                String str_salary_slip = salary_slip_verification;
                if (str_salary_slip.equals("Yes")) {
                    radio_salary_slip_verification.check(R.id.rd_salary_slip_verification_yes);
                } else if (str_salary_slip.equals("No")) {
                    radio_salary_slip_verification.check(R.id.rd_salary_slip_verification_no);
                }

                String str_form_sixteen_verification = form_sixteen_verification;
                if (str_form_sixteen_verification.equals("Yes")) {
                    radio_form_sixteen_verification.check(R.id.rd_sixteen_yes);
                } else if (str_form_sixteen_verification.equals("No")) {
                    radio_form_sixteen_verification.check(R.id.rd_sixteen_no);
                }

                String str_itreturns_verification = itreturns_verification;
                if (str_itreturns_verification.equals("Yes")) {
                    radio_itreturns_verification.check(R.id.rd_itreturns_verification_yes);
                } else if (str_itreturns_verification.equals("No")) {
                    radio_itreturns_verification.check(R.id.rd_itreturns_verification_no);
                }

                String str_bank_statements_verification = bank_statements_verification;
                if (str_bank_statements_verification.equals("Yes")) {
                    radio_bank_statement.check(R.id.rd_bank_yes);
                } else if (str_bank_statements_verification.equals("No")) {
                    radio_bank_statement.check(R.id.rd_bank_no);
                }

                String str_business_activity_verification = business_activity_verification;
                if (str_business_activity_verification.equals("Yes")) {
                    radio_Employee.check(R.id.rd_employee_yes);
                } else if (str_business_activity_verification.equals("No")) {
                    radio_Employee.check(R.id.rd_employee_no);
                }

                CustomerDetail_API();
            }
        });


        radio_Adhar_no.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                if (checkedId == R.id.rd_adhar_no_yes) {
                    txt_Adhar_no.setVisibility(View.GONE);
                } else if (checkedId == R.id.rd_adhar_no) {
                    txt_Adhar_no.setVisibility(View.VISIBLE);

                }
            }
        });

        radio_pancard.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                if (checkedId == R.id.rd_pancard_yes) {
                    txt_Pan_Card.setVisibility(View.GONE);
                } else if (checkedId == R.id.rd_pancard_no) {
                    txt_Pan_Card.setVisibility(View.VISIBLE);

                }
            }
        });

        radio_residence_address_verify.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                if (checkedId == R.id.rd_residence_yes) {
                    txt_residence_address.setVisibility(View.GONE);
                } else if (checkedId == R.id.rd_residence_no) {

                    txt_residence_address.setVisibility(View.VISIBLE);

                }
            }
        });

        radio_business_address.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                if (checkedId == R.id.rd_business_yes) {
                    txt_business_address.setVisibility(View.GONE);
                } else if (checkedId == R.id.rd_business_no) {
                    txt_business_address.setVisibility(View.VISIBLE);

                }

            }
        });
        radio_salary_slip_verification.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                if (checkedId == R.id.rd_salary_slip_verification_yes) {
                    txt_salary_slip_verification.setVisibility(View.GONE);
                } else if (checkedId == R.id.rd_salary_slip_verification_no) {
                    txt_salary_slip_verification.setVisibility(View.VISIBLE);

                }

            }
        });

        radio_form_sixteen_verification.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                if (checkedId == R.id.rd_sixteen_yes) {
                    txt_form_sixteen_verification.setVisibility(View.GONE);
                } else if (checkedId == R.id.rd_sixteen_no) {
                    txt_form_sixteen_verification.setVisibility(View.VISIBLE);

                }

            }
        });

        radio_itreturns_verification.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                if (checkedId == R.id.rd_itreturns_verification_yes) {
                    txt_itreturns_verification.setVisibility(View.GONE);
                } else if (checkedId == R.id.rd_itreturns_verification_no) {
                    txt_itreturns_verification.setVisibility(View.VISIBLE);

                }

            }
        });


        radio_business_phone.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                if (checkedId == R.id.rd_business_phone_yes) {
                    txt_business_phone.setVisibility(View.GONE);
                } else if (checkedId == R.id.rd_business_phone_no) {
                    txt_business_phone.setVisibility(View.VISIBLE);

                }

            }
        });


        radio_residence_phone_verify.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                if (checkedId == R.id.rd_residence_phone_Yes) {
                    txt_residence_phone_verify.setVisibility(View.GONE);
                } else if (checkedId == R.id.rd_residence_phone_no) {
                    txt_residence_phone_verify.setVisibility(View.VISIBLE);

                }

            }
        });

        radio_income_verification.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                if (checkedId == R.id.rd_income_yes) {
                    txt_verification_address.setVisibility(View.GONE);
                } else if (checkedId == R.id.rd_income_no) {
                    txt_verification_address.setVisibility(View.VISIBLE);

                }

            }
        });

        radio_bank_statement.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                if (checkedId == R.id.rd_bank_yes) {
                    txt_bank_statement.setVisibility(View.GONE);
                } else if (checkedId == R.id.rd_bank_no) {
                    txt_bank_statement.setVisibility(View.VISIBLE);

                }
            }
        });

        radio_Employee.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                if (checkedId == R.id.rd_employee_yes) {
                    txt_employee_address.setVisibility(View.GONE);
                } else if (checkedId == R.id.rd_employee_no) {
                    txt_employee_address.setVisibility(View.VISIBLE);

                }
            }
        });

    }

    private void SelectImage_ONE() {

        final CharSequence[] options = {"Take Photo", "Choose from Gallery", "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(AddCustomerDetail.this);

        builder.setTitle("Add Photo!");

        builder.setItems(options, new DialogInterface.OnClickListener() {

            @Override

            public void onClick(DialogInterface dialog, int item) {

                if (options[item].equals("Take Photo")) {
                    Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(takePicture, PICK_IMAGE_CAMERA_ONE);//zero can be replaced with any action code
                } else if (options[item].equals("Choose from Gallery")) {
                    Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(pickPhoto, PICK_IMAGE_GALLERY_TWO);//one can be replaced with any action code
                } else if (options[item].equals("Cancel")) {

                    dialog.dismiss();

                }
            }

        });

        builder.show();

    }

    private void SelectImage_TWO() {

        final CharSequence[] options = {"Take Photo", "Choose from Gallery", "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(AddCustomerDetail.this);

        builder.setTitle("Add Photo!");

        builder.setItems(options, new DialogInterface.OnClickListener() {

            @Override

            public void onClick(DialogInterface dialog, int item) {

                if (options[item].equals("Take Photo")) {
                    Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(takePicture, PICK_IMAGE_CAMERA_THREE);//zero can be replaced with any action code
                } else if (options[item].equals("Choose from Gallery")) {
                    Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(pickPhoto, PICK_IMAGE_GALLERY_FOUR);//one can be replaced with any action code
                } else if (options[item].equals("Cancel")) {

                    dialog.dismiss();

                }
            }

        });

        builder.show();

    }

    private void SelectImage_THREE() {

        final CharSequence[] options = {"Take Photo", "Choose from Gallery", "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(AddCustomerDetail.this);

        builder.setTitle("Add Photo!");

        builder.setItems(options, new DialogInterface.OnClickListener() {

            @Override

            public void onClick(DialogInterface dialog, int item) {

                if (options[item].equals("Take Photo")) {
                    Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(takePicture, PICK_IMAGE_CAMERA_FIVE);//zero can be replaced with any action code
                } else if (options[item].equals("Choose from Gallery")) {
                    Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(pickPhoto, PICK_IMAGE_GALLERY_SIX);//one can be replaced with any action code
                } else if (options[item].equals("Cancel")) {

                    dialog.dismiss();

                }
            }

        });

        builder.show();

    }

    private void SelectImage_FOUR() {

        final CharSequence[] options = {"Take Photo", "Choose from Gallery", "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(AddCustomerDetail.this);

        builder.setTitle("Add Photo!");

        builder.setItems(options, new DialogInterface.OnClickListener() {

            @Override

            public void onClick(DialogInterface dialog, int item) {

                if (options[item].equals("Take Photo")) {
                    Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(takePicture, PICK_IMAGE_CAMERA_SEVEN);//zero can be replaced with any action code
                } else if (options[item].equals("Choose from Gallery")) {
                    Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(pickPhoto, PICK_IMAGE_GALLERY_EIGHT);//one can be replaced with any action code
                } else if (options[item].equals("Cancel")) {

                    dialog.dismiss();

                }
            }

        });

        builder.show();

    }

    private void SelectImage_FIVE() {

        final CharSequence[] options = {"Take Photo", "Choose from Gallery", "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(AddCustomerDetail.this);

        builder.setTitle("Add Photo!");

        builder.setItems(options, new DialogInterface.OnClickListener() {

            @Override

            public void onClick(DialogInterface dialog, int item) {

                if (options[item].equals("Take Photo")) {
                    Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(takePicture, PICK_IMAGE_CAMERA_NINE);//zero can be replaced with any action code
                } else if (options[item].equals("Choose from Gallery")) {
                    Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(pickPhoto, PICK_IMAGE_GALLERY_TEN);//one can be replaced with any action code
                } else if (options[item].equals("Cancel")) {

                    dialog.dismiss();

                }
            }

        });

        builder.show();

    }

    private void SelectImage_SIX() {

        final CharSequence[] options = {"Take Photo", "Choose from Gallery", "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(AddCustomerDetail.this);

        builder.setTitle("Add Photo!");

        builder.setItems(options, new DialogInterface.OnClickListener() {

            @Override

            public void onClick(DialogInterface dialog, int item) {

                if (options[item].equals("Take Photo")) {
                    Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(takePicture, PICK_IMAGE_CAMERA_ELEVEN);//zero can be replaced with any action code
                } else if (options[item].equals("Choose from Gallery")) {
                    Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(pickPhoto, PICK_IMAGE_GALLERY_TWELVE);//one can be replaced with any action code
                } else if (options[item].equals("Cancel")) {

                    dialog.dismiss();

                }
            }

        });

        builder.show();

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case MarshmallowIntentId.ACCESS_FINE_LOCATION_INTENT_ID:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // task you need to do.
                    Toast.makeText(AddCustomerDetail.this, "Location Permission granted.", Toast.LENGTH_SHORT).show();

                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Toast.makeText(AddCustomerDetail.this, "Location Permission denied.", Toast.LENGTH_SHORT).show();
                }
                break;
            case MarshmallowIntentId.WRITE_EXTERNAL_STORAGE_PERMISSION:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // task you need to do.
                    Toast.makeText(AddCustomerDetail.this, "Storage Permission granted.", Toast.LENGTH_SHORT).show();

                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Toast.makeText(AddCustomerDetail.this, "Storage Permission denied.", Toast.LENGTH_SHORT).show();
                }
                break;

            case MarshmallowIntentId.ACCESS_CAMERA_PERMISSION:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // task you need to do.
                    Toast.makeText(AddCustomerDetail.this, "Camera Permission granted.", Toast.LENGTH_SHORT).show();

                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Toast.makeText(AddCustomerDetail.this, "Camera Permission denied.", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);

        Bitmap bitmap;

        // Image 1
        if (requestCode == PICK_IMAGE_CAMERA_ONE) {

            try {

                //imageuri = imageReturnedIntent.getData();
                bitmap = (Bitmap) imageReturnedIntent.getExtras().get("data");
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 50, bytes);
                ivShopPhoto1.setImageBitmap(bitmap);
                imageiconselect = "1";

                imageuri = getImageUri(getApplicationContext(), bitmap);

            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (requestCode == PICK_IMAGE_GALLERY_TWO) {

            final InputStream imageStream1;

            try {

                imageuri = imageReturnedIntent.getData();
                imageStream1 = getApplicationContext().getContentResolver().openInputStream(imageuri);
                final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream1);
                ivShopPhoto1.setImageBitmap(selectedImage);
                imageiconselect = "1";

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        // Image 2
        if (requestCode == PICK_IMAGE_CAMERA_THREE) {

            try {

                //imageuri2 = imageReturnedIntent.getData();
                bitmap = (Bitmap) imageReturnedIntent.getExtras().get("data");
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 50, bytes);
                ivShopPhoto2.setImageBitmap(bitmap);
                imageiconselect2 = "1";

                imageuri2 = getImageUri(getApplicationContext(), bitmap);

            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (requestCode == PICK_IMAGE_GALLERY_FOUR) {

            final InputStream imageStream2;

            try {

                imageuri2 = imageReturnedIntent.getData();
                imageStream2 = getApplicationContext().getContentResolver().openInputStream(imageuri2);
                final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream2);
                ivShopPhoto2.setImageBitmap(selectedImage);
                imageiconselect2 = "1";

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        // Image 3
        if (requestCode == PICK_IMAGE_CAMERA_FIVE) {

            try {

                imageuri3 = imageReturnedIntent.getData();
                bitmap = (Bitmap) imageReturnedIntent.getExtras().get("data");
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 50, bytes);
                ivShopPhoto3.setImageBitmap(bitmap);
                imageiconselect3 = "1";

                imageuri3 = getImageUri(getApplicationContext(), bitmap);

            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (requestCode == PICK_IMAGE_GALLERY_SIX) {

            final InputStream imageStream3;

            try {

                imageuri3 = imageReturnedIntent.getData();
                imageStream3 = getApplicationContext().getContentResolver().openInputStream(imageuri3);
                final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream3);
                ivShopPhoto3.setImageBitmap(selectedImage);
                imageiconselect3 = "1";

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        // Image 4
        if (requestCode == PICK_IMAGE_CAMERA_SEVEN) {

            try {

                imageuri4 = imageReturnedIntent.getData();
                bitmap = (Bitmap) imageReturnedIntent.getExtras().get("data");
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 50, bytes);
                ivShopPhoto4.setImageBitmap(bitmap);
                imageiconselect4 = "1";

                imageuri4 = getImageUri(getApplicationContext(), bitmap);

            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (requestCode == PICK_IMAGE_GALLERY_EIGHT) {

            final InputStream imageStream4;

            try {

                imageuri4 = imageReturnedIntent.getData();
                imageStream4 = getApplicationContext().getContentResolver().openInputStream(imageuri4);
                final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream4);
                ivShopPhoto4.setImageBitmap(selectedImage);
                imageiconselect4 = "1";

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        // Image 5
        if (requestCode == PICK_IMAGE_CAMERA_NINE) {

            try {

                imageuri5 = imageReturnedIntent.getData();
                bitmap = (Bitmap) imageReturnedIntent.getExtras().get("data");
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 50, bytes);
                ivShopPhoto5.setImageBitmap(bitmap);
                imageiconselect5 = "1";

                imageuri5 = getImageUri(getApplicationContext(), bitmap);

            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (requestCode == PICK_IMAGE_GALLERY_TEN) {

            final InputStream imageStream5;

            try {

                imageuri5 = imageReturnedIntent.getData();
                imageStream5 = getApplicationContext().getContentResolver().openInputStream(imageuri5);
                final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream5);
                ivShopPhoto5.setImageBitmap(selectedImage);
                imageiconselect5 = "1";

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        // Image 6
        if (requestCode == PICK_IMAGE_CAMERA_ELEVEN) {

            try {

                imageuri6 = imageReturnedIntent.getData();
                bitmap = (Bitmap) imageReturnedIntent.getExtras().get("data");
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 50, bytes);
                ivShopPhoto6.setImageBitmap(bitmap);
                imageiconselect6 = "1";

            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (requestCode == PICK_IMAGE_GALLERY_TWELVE) {

            final InputStream imageStream6;

            try {

                imageuri6 = imageReturnedIntent.getData();
                imageStream6 = getApplicationContext().getContentResolver().openInputStream(imageuri6);
                final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream6);
                ivShopPhoto6.setImageBitmap(selectedImage);
                imageiconselect6 = "1";

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        /*if (resultCode == RESULT_OK && null != imageReturnedIntent) {

            switch (requestCode) {
                case SELECT_PHOTO_1:

                    if (resultCode == RESULT_OK) {
                        //for 1st image
                        final InputStream imageStream;
                        try {
                            imageuri = imageReturnedIntent.getData();
                            imageStream = getApplicationContext().getContentResolver().openInputStream(imageuri);
                            final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                            ivShopPhoto1.setImageBitmap(selectedImage);
                            imageiconselect = "1";

                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        }
                    }

                    *//*if (requestCode == 1) {

                        Uri selectedImage = imageReturnedIntent.getData();
                        imageStream = getApplicationContext().getContentResolver().openInputStream(selectedImage);
                        final Bitmap selectedImage1 = BitmapFactory.decodeStream(imageStream);
                        ivShopPhoto1.setImageBitmap(selectedImage1);
                        imageiconselect = "1";

                    } else if (requestCode == 2) {

                        Uri selectedImage = imageReturnedIntent.getData();
                        imageview.setImageURI(selectedImage);
                    }*//*


                    break;
                case SELECT_PHOTO_2:
                    if (resultCode == RESULT_OK) {
                        try {
                            //for 2nd image
                            imageuri2 = imageReturnedIntent.getData();
                            final InputStream imageStream2 = getApplicationContext().getContentResolver().openInputStream(imageuri2);
                            final Bitmap selectedImage2 = BitmapFactory.decodeStream(imageStream2);
                            ivShopPhoto2.setImageBitmap(selectedImage2);
                            imageiconselect2 = "1";

                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                    }
                    break;
                case SELECT_PHOTO_3:
                    if (resultCode == RESULT_OK) {
                        try {
                            //for 3rd image
                            imageuri3 = imageReturnedIntent.getData();
                            final InputStream imageStream3 = getApplicationContext().getContentResolver().openInputStream(imageuri3);
                            final Bitmap selectedImage3 = BitmapFactory.decodeStream(imageStream3);
                            ivShopPhoto3.setImageBitmap(selectedImage3);
                            imageiconselect3 = "1";

                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                    }
                    break;
                case SELECT_PHOTO_4:
                    if (resultCode == RESULT_OK) {
                        try {

                            //for 4th image
                            imageuri4 = imageReturnedIntent.getData();
                            final InputStream imageStream4 = getApplicationContext().getContentResolver().openInputStream(imageuri4);
                            final Bitmap selectedImage4 = BitmapFactory.decodeStream(imageStream4);
                            ivShopPhoto4.setImageBitmap(selectedImage4);
                            imageiconselect4 = "1";

                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                    }
                    break;
                case SELECT_PHOTO_5:
                    if (resultCode == RESULT_OK) {
                        try {
                            //for 5th image
                            imageuri5 = imageReturnedIntent.getData();
                            final InputStream imageStream5 = getApplicationContext().getContentResolver().openInputStream(imageuri5);
                            final Bitmap selectedImage5 = BitmapFactory.decodeStream(imageStream5);
                            ivShopPhoto5.setImageBitmap(selectedImage5);
                            imageiconselect5 = "1";
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                    }
                    break;
                case SELECT_PHOTO_6:
                    if (resultCode == RESULT_OK) {
                        try {
                            //for 6th image
                            imageuri6 = imageReturnedIntent.getData();
                            final InputStream imageStream6 = getApplicationContext().getContentResolver().openInputStream(imageuri6);
                            final Bitmap selectedImage6 = BitmapFactory.decodeStream(imageStream6);
                            ivShopPhoto6.setImageBitmap(selectedImage6);
                            imageiconselect6 = "1";
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                    }
                    break;
            }*/
//            } else {
//                if (bytes < 1024) {
//                    filesizeMB = bytes;
//                    System.out.println("image size =" + filesizeMB + "bytes");
//                    fileErrorDialoge(" Image size is more than 5MB. Please upload image less that 5MB in size.");
//
//                } else if (bytes < 1048576) {
//                    filesizeMB = (bytes / 1024);
//                    //+ " KB";
//                    System.out.println("image size =" + filesizeMB + "KB");
//                    fileErrorDialoge(" Image size is more than 5MB. Please upload image less that 5MB in size.");
//                } else if (bytes < 1073741824) {
//                    filesizeMB = (bytes / 1048576);
//                    //+ " MB";
//                    System.out.println("image size =" + filesizeMB + "MB");
//                    fileErrorDialoge(" Image size is more than 5MB. Please upload image less that 5MB in size.");
//                } else {
//                    filesizeMB = (bytes / 1073741824);
//                    //" GB";
//                    System.out.println("image size =" + filesizeMB + "GB");
//                    fileErrorDialoge(" Image size is more than 5MB. Please upload image less that 5MB in size.");
//                }

    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public String getRealPathFromURI(Uri uri) {
        String path = "";
        if (getContentResolver() != null) {
            Cursor cursor = getContentResolver().query(uri, null, null, null, null);
            if (cursor != null) {
                cursor.moveToFirst();
                int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
                path = cursor.getString(idx);
                cursor.close();
            }
        }
        return path;
    }

    public Uri getPickImageResultUri(Intent data) {
        boolean isCamera = true;
        if (data != null) {
            String action = data.getAction();
            isCamera = action != null && action.equals(MediaStore.ACTION_IMAGE_CAPTURE);
        }

        return isCamera ? getCaptureImageOutputUri() : data.getData();
    }

    private Uri getCaptureImageOutputUri() {
        Uri outputFileUri = null;
        File getImage = getExternalCacheDir();
        if (getImage != null) {
            outputFileUri = Uri.fromFile(new File(getImage.getPath()));
        }
        return outputFileUri;
    }

    private void CustomerDetail_API() {
        showpDialog();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                RestInterface.url_add_customerdetails, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Response: " + response);
                hidepDialog();
                try {
                    JSONObject jObj = new JSONObject(response);
                    String status = jObj.getString("status");

                    if (status.equals("true")) {

                        String Message = jObj.getString("Message");
                        Toast.makeText(getApplicationContext(), Message, Toast.LENGTH_LONG).show();

                        if (imageiconselect.equals("1")) {
                            uploadMultipart();
                        } else {
                            imageiconselect = "";
                        }

                        if (imageiconselect2.equals("1")) {
                            uploadMultipart2();
                        } else {
                            imageiconselect2 = "";
                        }

                        if (imageiconselect3.equals("1")) {
                            uploadMultipart3();
                        } else {
                            imageiconselect3 = "";
                        }

                        if (imageiconselect4.equals("1")) {
                            uploadMultipart4();
                        } else {
                            imageiconselect4 = "";
                        }

                        if (imageiconselect5.equals("1")) {
                            uploadMultipart5();
                        } else {
                            imageiconselect5 = "";
                        }

                        if (imageiconselect6.equals("1")) {
                            uploadMultipart6();
                        } else {
                            imageiconselect6 = "";
                        }

                        Intent intent_MedicineList = new Intent(getApplicationContext(), UserListining.class);
                        startActivity(intent_MedicineList);

                    } else {
                        String Message = jObj.getString("Error Message");
                        Toast.makeText(getApplicationContext(), Message, Toast.LENGTH_SHORT).show();

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Error: " + error.getMessage());
                hidepDialog();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                // Posting params to register url
                Map<String, String> params = new HashMap<String, String>();
                params.put("id", mId);
                params.put("user_id", user_id);
                params.put("cino", cino);
                params.put("customer_name", customer_name);
                params.put("customer_mobileno", customer_mobileno);
                params.put("address", address);

                // Adhar Card
                params.put("adharnumber_verification", adharnumber_verification);
                params.put("remarks_adharnumber", txt_Adhar_no.getText().toString());

                // pancard
                params.put("pancard_number_verification", pancard_number_verification);
                params.put("remarks_pancard_number", txt_Pan_Card.getText().toString());

                // residence address
                params.put("residence_address_verification", residence_address_verification);
                params.put("remarks_residence_address", txt_residence_address.getText().toString());

                // bussiness address
                params.put("business_address_verification", business_address_verification);
                params.put("remarks_business_address", txt_business_address.getText().toString());

                // bussiness phone
                params.put("business_phone_verification", business_phone_verification);
                params.put("remarks_business_phone", txt_business_phone.getText().toString());

                // residence phone
                params.put("residence_phone_verification", residence_phone_verification);
                params.put("remarks_residence_phone", txt_residence_phone_verify.getText().toString());

                // income
                params.put("income_verification", income_verification);
                params.put("remarks_income", txt_verification_address.getText().toString());

                // salary slip
                params.put("salary_slip_verification", salary_slip_verification);
                params.put("remarks_salary_slip", txt_salary_slip_verification.getText().toString());

                // form 16
                params.put("form_sixteen_verification", form_sixteen_verification);
                params.put("remarks_form_sixteen", txt_form_sixteen_verification.getText().toString());

                // it returns
                params.put("itreturns_verification", itreturns_verification);
                params.put("remarks_itreturns", txt_itreturns_verification.getText().toString());

                // bank statement
                params.put("bank_statements_verification", bank_statements_verification);
                params.put("remarks_bank_statements", txt_bank_statement.getText().toString());

                // bussiness activity
                params.put("business_activity_verification", business_activity_verification);
                params.put("remarks_business_activity", txt_employee_address.getText().toString());

                StringBuilder str = new StringBuilder();

                if (imageiconselect.equals("1")) {
                    imagename = getFileName(imageuri);
                    imagename = cino + "_1_" + imagename + ",";
                    //imagename = cino + "_1" + ",";
                    str.append(imagename);
                }
                if (imageiconselect2.equals("1")) {
                    imagename2 = getFileName(imageuri2);
                    imagename2 = cino + "_2_" + imagename2 + ",";
                    //str.append(imagename2);
                    //imagename2 = cino + "_2" + ",";
                    str.append(imagename2);
                }
                if (imageiconselect3.equals("1")) {
                    imagename3 = getFileName(imageuri3);
                    imagename3 = cino + "_3_" + imagename3 + ",";
                    //str.append(imagename3);
                    //imagename3 = cino + "_3" + ",";
                    str.append(imagename3);
                }
                if (imageiconselect4.equals("1")) {
                    imagename4 = getFileName(imageuri4);
                    imagename4 = cino + "_4_" + imagename4 + ",";
                    //str.append(imagename4);
                    //imagename4 = cino + "_4" + ",";
                    str.append(imagename4);
                }
                if (imageiconselect5.equals("1")) {
                    imagename5 = getFileName(imageuri5);
                    imagename5 = cino + "_5_" + imagename5 + ",";
                    //str.append(imagename5);
                    //imagename5 = cino + "_5" + ",";
                    str.append(imagename5);
                }
                if (imageiconselect6.equals("1")) {
                    imagename6 = getFileName(imageuri6);
                    imagename6 = cino + "_6_" + imagename6 + ",";
                    //str.append(a6.toString());
                    //imagename6 = cino + "_6" + ",";
                    str.append(imagename6);
                }

                String str1 = str.toString();
                str1 = str1.substring(0, str1.lastIndexOf(","));
                params.put("documents", str1);

                String location = currentLatitude + "," + currentLongitude;
                params.put("location", location);

                return params;
            }
        };

        strReq.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // Adding request to request queue
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        strReq.setShouldCache(false);
        requestQueue.add(strReq);
    }

    /*private void CustomerDetail_API1() {

        StringRequest putRequest = new StringRequest(Request.Method.PUT, RestInterface.url_add_customerdetails,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response);
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        //Log.d("Error.Response", error);
                    }
                }
        ) {

            @Override
            protected Map<String, String> getParams()
            {
                // Posting params to register url
                Map <String, String> params = new HashMap<String, String>();
                params.put("user_id", user_id);
                params.put("cino", cino);
                params.put("customer_name", customer_name);
                params.put("customer_mobileno", customer_mobileno);
                params.put("address", address);

                // Adhar Card
                params.put("adharnumber_verification", adharnumber_verification);
                params.put("remarks_adharnumber", txt_Adhar_no.getText().toString());

                // pancard
                params.put("pancard_number_verification", pancard_number_verification);
                params.put("remarks_pancard_number", txt_Pan_Card.getText().toString());

                // residence address
                params.put("residence_address_verification", residence_address_verification);
                params.put("remarks_residence_address", txt_residence_address.getText().toString());

                // bussiness address
                params.put("business_address_verification", business_address_verification);
                params.put("remarks_business_address", txt_business_address.getText().toString());

                // bussiness phone
                params.put("business_phone_verification", business_phone_verification);
                params.put("remarks_business_phone", txt_business_phone.getText().toString());

                // residence phone
                params.put("residence_phone_verification", residence_phone_verification);
                params.put("remarks_residence_phone", txt_residence_phone_verify.getText().toString());

                // income
                params.put("income_verification", income_verification);
                params.put("remarks_income", txt_verification_address.getText().toString());

                // salary slip
                params.put("salary_slip_verification", salary_slip_verification);
                params.put("remarks_salary_slip", txt_salary_slip_verification.getText().toString());

                // form 16
                params.put("form_sixteen_verification", form_sixteen_verification);
                params.put("remarks_form_sixteen", txt_form_sixteen_verification.getText().toString());

                // it returns
                params.put("itreturns_verification", itreturns_verification);
                params.put("remarks_itreturns", txt_itreturns_verification.getText().toString());

                // bank statement
                params.put("bank_statements_verification", bank_statements_verification);
                params.put("remarks_bank_statements", txt_bank_statement.getText().toString());

                // bussiness activity
                params.put("business_activity_verification", business_activity_verification);
                params.put("remarks_business_activity", txt_employee_address.getText().toString());

                StringBuilder str = new StringBuilder();

                if (imageiconselect.equals("1")) {
                    imagename = getFileName(imageuri);
                    String a1 = imagename + ",";
                    str.append(a1.toString());
                }
                if (imageiconselect2.equals("1")) {
                    imagename2 = getFileName(imageuri2);
                    String a2 = imagename2 + ",";
                    str.append(a2.toString());
                }
                if (imageiconselect3.equals("1")) {
                    imagename3 = getFileName(imageuri3);
                    String a3 = imagename3 + ",";
                    str.append(a3.toString());
                }
                if (imageiconselect4.equals("1")) {
                    imagename4 = getFileName(imageuri4);
                    String a4 = imagename4 + ",";
                    str.append(a4.toString());
                }
                if (imageiconselect5.equals("1")) {
                    imagename5 = getFileName(imageuri5);
                    String a5 = imagename5 + ",";
                    str.append(a5.toString());
                }
                if (imageiconselect6.equals("1")) {
                    imagename6 = getFileName(imageuri6);
                    String a6 = imagename6 + ",";
                    str.append(a6.toString());
                }
                params.put("documents", str.toString());

                String location = currentLatitude + "," + currentLongitude;
                params.put("location", location);

                return params;
            }

        };

        putRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // Adding request to request queue
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        putRequest.setShouldCache(false);
        requestQueue.add(putRequest);
    }*/

    public String getFileName(Uri uri) {
        String result = null;
        if (uri.getScheme().equals("content")) {
            Cursor cursor = getApplicationContext().getContentResolver().query(uri, null, null, null, null);
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                }
            } finally {
                cursor.close();
            }
        }
        if (result == null) {
            result = uri.getPath();
            int cut = result.lastIndexOf('/');
            if (cut != -1) {
                result = result.substring(cut + 1);
            }
        }
        return result;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (getParentActivityIntent() == null) {
                    Log.i(TAG, "You have forgotten to specify the parentActivityName in the AndroidManifest!");
                    onBackPressed();
                } else {
                    NavUtils.navigateUpFromSameTask(this);
                }
                return true;
            case R.id.menu_off:

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
                alertDialogBuilder.setMessage("Are you sure, you wanted logout?");
                alertDialogBuilder.setPositiveButton("Yes",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface arg0, int arg1) {

                                pref = getApplicationContext().getSharedPreferences(RestInterface.pref, Context.MODE_PRIVATE);

                                SharedPreferences.Editor editor = pref.edit();
                                editor.clear();
                                editor.apply();
                                editor.commit(); // commit changes

                                Intent intent_Login = new Intent(getApplicationContext(), Login.class);
                                startActivity(intent_Login);
                            }
                        });

                alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //finish();
                    }
                });

                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void uploadMultipart() {

        //getting the actual path of the image
        String path = getPath(imageuri);

        //Uploading code
        try {
            String uploadId = UUID.randomUUID().toString();

            imagename = imagename.replaceAll(",$", "");

            //Creating a multi part request
            new MultipartUploadRequest(getApplicationContext(), uploadId, url_imageupload)
                    .addFileToUpload(path, "image", imagename) //Adding file
                    .setNotificationConfig(new UploadNotificationConfig())
                    .setMaxRetries(2)
                    .startUpload(); //Starting the upload

            //Toast.makeText(this, "Documents Uploded Successfully.", Toast.LENGTH_SHORT).show();

        } catch (Exception exc) {
            Toast.makeText(getApplicationContext(), exc.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    public void uploadMultipart2() {

        //getting the actual path of the image
        String path = getPath(imageuri2);

        //Uploading code
        try {
            String uploadId = UUID.randomUUID().toString();

            imagename2 = imagename2.replaceAll(",$", "");

            //Creating a multi part request
            new MultipartUploadRequest(getApplicationContext(), uploadId, url_imageupload)
                    .addFileToUpload(path, "image", imagename2) //Adding file
                    .setNotificationConfig(new UploadNotificationConfig())
                    .setMaxRetries(2)
                    .startUpload(); //Starting the upload

            //Toast.makeText(this, "Documents Uploded Successfully.", Toast.LENGTH_SHORT).show();

        } catch (Exception exc) {
            Toast.makeText(getApplicationContext(), exc.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    public void uploadMultipart3() {

        //getting the actual path of the image
        String path = getPath(imageuri3);

        //Uploading code
        try {
            String uploadId = UUID.randomUUID().toString();

            imagename3 = imagename3.replaceAll(",$", "");

            //Creating a multi part request
            new MultipartUploadRequest(getApplicationContext(), uploadId, url_imageupload)
                    .addFileToUpload(path, "image", imagename3) //Adding file
                    .setNotificationConfig(new UploadNotificationConfig())
                    .setMaxRetries(2)
                    .startUpload(); //Starting the upload

            //Toast.makeText(this, "Documents Uploded Successfully.", Toast.LENGTH_SHORT).show();

        } catch (Exception exc) {
            Toast.makeText(getApplicationContext(), exc.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    public void uploadMultipart4() {

        //getting the actual path of the image
        String path = getPath(imageuri4);

        //Uploading code
        try {
            String uploadId = UUID.randomUUID().toString();

            imagename4 = imagename4.replaceAll(",$", "");

            //Creating a multi part request
            new MultipartUploadRequest(getApplicationContext(), uploadId, url_imageupload)
                    .addFileToUpload(path, "image", imagename4) //Adding file
                    .setNotificationConfig(new UploadNotificationConfig())
                    .setMaxRetries(2)
                    .startUpload(); //Starting the upload

            //Toast.makeText(this, "Documents Uploded Successfully.", Toast.LENGTH_SHORT).show();

        } catch (Exception exc) {
            Toast.makeText(getApplicationContext(), exc.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    public void uploadMultipart5() {

        //getting the actual path of the image
        String path = getPath(imageuri5);

        //Uploading code
        try {
            String uploadId = UUID.randomUUID().toString();

            imagename5 = imagename5.replaceAll(",$", "");

            //Creating a multi part request
            new MultipartUploadRequest(getApplicationContext(), uploadId, url_imageupload)
                    .addFileToUpload(path, "image", imagename5) //Adding file
                    .setNotificationConfig(new UploadNotificationConfig())
                    .setMaxRetries(2)
                    .startUpload(); //Starting the upload

            //Toast.makeText(this, "Documents Uploded Successfully.", Toast.LENGTH_SHORT).show();

        } catch (Exception exc) {
            Toast.makeText(getApplicationContext(), exc.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    public void uploadMultipart6() {

        //getting the actual path of the image
        String path = getPath(imageuri6);

        //Uploading code
        try {
            String uploadId = UUID.randomUUID().toString();

            imagename6 = imagename6.replaceAll(",$", "");

            //Creating a multi part request
            new MultipartUploadRequest(getApplicationContext(), uploadId, url_imageupload)
                    .addFileToUpload(path, "image", imagename6) //Adding file
                    .setNotificationConfig(new UploadNotificationConfig())
                    .setMaxRetries(2)
                    .startUpload(); //Starting the upload

            //Toast.makeText(this, "Documents Uploded Successfully.", Toast.LENGTH_SHORT).show();

        } catch (Exception exc) {
            Toast.makeText(getApplicationContext(), exc.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    //method to get the file path from uri
    public String getPath(Uri uri) {
        Cursor cursor = getApplicationContext().getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        String document_id = cursor.getString(0);
        document_id = document_id.substring(document_id.lastIndexOf(":") + 1);
        cursor.close();

        cursor = getApplicationContext().getContentResolver().query(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                null, MediaStore.Images.Media._ID + " = ? ", new String[]{document_id}, null);
        cursor.moveToFirst();
        String path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
        cursor.close();

        return path;
    }

    private void showpDialog() {
        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Please wait...");
        pDialog.setCancelable(false);

        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hidepDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    @Override
    protected void onResume() {
        super.onResume();
        //Now lets connect to the API
        mGoogleApiClient.connect();
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.v(this.getClass().getSimpleName(), "onPause()");

        //Disconnect from API onPause()
        if (mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
            mGoogleApiClient.disconnect();
        }
    }

    /**
     * If connected get lat and long
     */
    @Override
    public void onConnected(Bundle bundle) {
        Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

        if (location == null) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);

        } else {
            //If everything went fine lets get latitude and longitude
            currentLatitude = location.getLatitude();
            currentLongitude = location.getLongitude();

            //Toast.makeText(this, currentLatitude + " WORKS " + currentLongitude + "", Toast.LENGTH_LONG).show();
        }
    }


    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        /*
         * Google Play services can resolve some errors it detects.
         * If the error has a resolution, try sending an Intent to
         * start a Google Play services activity that can resolve
         * error.
         */
        if (connectionResult.hasResolution()) {
            try {
                // Start an Activity that tries to resolve the error
                connectionResult.startResolutionForResult(this, CONNECTION_FAILURE_RESOLUTION_REQUEST);
                /*
                 * Thrown if Google Play services canceled the original
                 * PendingIntent
                 */
            } catch (IntentSender.SendIntentException e) {
                // Log the error
                e.printStackTrace();
            }
        } else {
            /*
             * If no resolution is available, display a dialog to the
             * user with the error.
             */
            Log.e("Error", "Location services connection failed with code " + connectionResult.getErrorCode());
        }
    }

    /**
     * If locationChanges change lat and long
     *
     * @param location
     */
    @Override
    public void onLocationChanged(Location location) {
        currentLatitude = location.getLatitude();
        currentLongitude = location.getLongitude();

        //Toast.makeText(this, currentLatitude + " WORKS " + currentLongitude + "", Toast.LENGTH_LONG).show();
    }


    @Override
    public void onBackPressed() {
        Intent intent_DrawerLayoutActivity = new Intent(getApplicationContext(), UserListining.class);
        startActivity(intent_DrawerLayoutActivity);
    }

}
