package codeplateau.com.striskfinalproject.Activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.app.NavUtils;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import codeplateau.com.striskfinalproject.Adapter.CustomerDetail_Adapter;
import codeplateau.com.striskfinalproject.Model.CustomerDetail_Model;
import codeplateau.com.striskfinalproject.R;
import codeplateau.com.striskfinalproject.Services.AppController;
import codeplateau.com.striskfinalproject.Services.RestInterface;

import static codeplateau.com.striskfinalproject.Services.RestInterface.url_customerlist;

public class UserListining extends AppCompatActivity {

    private static String TAG = UserListining.class.getSimpleName();
    private static RecyclerView rc_cino_list;
    private static LinearLayout ll_cino_no_record;
    private static LinearLayoutManager mLayoutManager_cinolist;
    private static Button btn_search;
    private static EditText ed_cino;
    private static String mUserId;
    private static SwipeRefreshLayout rc_cino_list_refresh;
    ProgressDialog pDialog;
    SharedPreferences pref;
    ArrayList<CustomerDetail_Model> customerDetail_modelArrayList;
    CustomerDetail_Adapter customerDetail_adapter;
    private String jsonResponse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.customer_list);

        initViews();
        setListeners();
    }

    private void initViews() {

        // Back button
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);

        // Add Title
        getSupportActionBar().setTitle("Customer List");

        pref = getSharedPreferences(RestInterface.pref, Context.MODE_PRIVATE);
        mUserId = pref.getString("id", "");

        ed_cino = findViewById(R.id.ed_cino);
        rc_cino_list = findViewById(R.id.rc_cino_list);
        rc_cino_list_refresh = findViewById(R.id.pullToRefresh);
        ll_cino_no_record = findViewById(R.id.ll_cino_no_record);
    }

    private void setListeners() {

        CustomerList_API(mUserId);

        mLayoutManager_cinolist = new LinearLayoutManager(getApplicationContext());
        rc_cino_list.setHasFixedSize(false);
        mLayoutManager_cinolist.setOrientation(LinearLayoutManager.VERTICAL);
        rc_cino_list.setLayoutManager(mLayoutManager_cinolist);

        ed_cino.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence CINO, int start, int before, int count) {

                // TODO Auto-generated method stub
                CustomerListByCINO_API(mUserId, CINO.toString());
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {

                // TODO Auto-generated method stub
            }
        });

        rc_cino_list_refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                CustomerList_API(mUserId); // your code
                rc_cino_list_refresh.setRefreshing(true);
            }
        });
    }

    // Search By UserId
    private void CustomerList_API(final String mUserId) {

        showpDialog();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                RestInterface.url_customerlist, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Leads Response: " + response);

                hidepDialog();

                try {

                    customerDetail_modelArrayList = new ArrayList<CustomerDetail_Model>();
                    customerDetail_modelArrayList.clear();

                    JSONArray jsonArray_today = new JSONArray(response);

                    if (jsonArray_today.equals("[]")) {

                        ll_cino_no_record.setVisibility(View.VISIBLE);
                        rc_cino_list.setVisibility(View.GONE);

                    } else {

                        for (int i = 0; i < jsonArray_today.length(); i++) {

                            JSONObject person = jsonArray_today.getJSONObject(i);

                            CustomerDetail_Model customerDetail_model = new CustomerDetail_Model();

                            customerDetail_model.setId(person.optString("id"));
                            customerDetail_model.setCino(person.optString("cino"));
                            customerDetail_model.setCustomer_name(person.optString("customer_name"));
                            customerDetail_model.setCustomer_mobileno(person.optString("customer_mobileno"));
                            customerDetail_model.setAddress(person.optString("address"));
                            customerDetail_model.setCity(person.optString("city"));

                            customerDetail_modelArrayList.add(customerDetail_model);
                        }

                        if (!customerDetail_modelArrayList.isEmpty()) {

                            rc_cino_list.setVisibility(View.VISIBLE);
                            ll_cino_no_record.setVisibility(View.GONE);

                            customerDetail_adapter = new CustomerDetail_Adapter(getApplicationContext(), customerDetail_modelArrayList, "", "", "");
                            rc_cino_list.setAdapter(customerDetail_adapter);

                        } else {

                            rc_cino_list.setVisibility(View.GONE);
                            ll_cino_no_record.setVisibility(View.VISIBLE);
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Error: " + error.getMessage());
                //Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
                hidepDialog();
            }
        }) {

            @Override
            protected java.util.Map<String, String> getParams() {
                // Posting params to register url
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", mUserId);
                return params;
            }
        };

        strReq.setRetryPolicy(new DefaultRetryPolicy(
                5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // Adding request to request queue
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        strReq.setShouldCache(false);
        requestQueue.add(strReq);
    }

    // Search By CINO
    private void CustomerListByCINO_API(final String mUserId, final String CINO) {

        showpDialog();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                RestInterface.url_customerlist_by_userId, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Response: " + response);

                hidepDialog();

                try {
                    customerDetail_modelArrayList = new ArrayList<CustomerDetail_Model>();
                    customerDetail_modelArrayList.clear();

                    JSONArray jsonArray_today = new JSONArray(response);

                    if (jsonArray_today.equals("[]")) {

                        ll_cino_no_record.setVisibility(View.VISIBLE);
                        rc_cino_list.setVisibility(View.GONE);

                    } else {

                        for (int i = 0; i < jsonArray_today.length(); i++) {

                            JSONObject person = jsonArray_today.getJSONObject(i);

                            CustomerDetail_Model customerDetail_model = new CustomerDetail_Model();

                            customerDetail_model.setId(person.optString("id"));
                            customerDetail_model.setCino(person.optString("cino"));
                            customerDetail_model.setCustomer_name(person.optString("customer_name"));
                            customerDetail_model.setCustomer_mobileno(person.optString("customer_mobileno"));
                            customerDetail_model.setAddress(person.optString("address"));
                            customerDetail_model.setCity(person.optString("city"));

                            customerDetail_modelArrayList.add(customerDetail_model);
                        }

                        if (!customerDetail_modelArrayList.isEmpty()) {

                            rc_cino_list.setVisibility(View.VISIBLE);
                            ll_cino_no_record.setVisibility(View.GONE);

                            customerDetail_adapter = new CustomerDetail_Adapter(getApplicationContext(), customerDetail_modelArrayList, "", "", "");
                            rc_cino_list.setAdapter(customerDetail_adapter);

                        } else {

                            rc_cino_list.setVisibility(View.GONE);
                            ll_cino_no_record.setVisibility(View.VISIBLE);
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Error: " + error.getMessage());
                //Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
                hidepDialog();
            }
        }) {

            @Override
            protected java.util.Map<String, String> getParams() {
                // Posting params to register url
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", mUserId);
                params.put("cino", CINO);
                return params;
            }
        };

        strReq.setRetryPolicy(new DefaultRetryPolicy(
                5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // Adding request to request queue
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        strReq.setShouldCache(false);
        requestQueue.add(strReq);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (getParentActivityIntent() == null) {
                    Log.i(TAG, "You have forgotten to specify the parentActivityName in the AndroidManifest!");
                    onBackPressed();
                } else {
                    NavUtils.navigateUpFromSameTask(this);
                }
                return true;
            case R.id.menu_off:

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
                alertDialogBuilder.setMessage("Are you sure, you wanted logout?");
                alertDialogBuilder.setPositiveButton("Yes",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface arg0, int arg1) {

                                pref = getApplicationContext().getSharedPreferences(RestInterface.pref, Context.MODE_PRIVATE);

                                SharedPreferences.Editor editor = pref.edit();
                                editor.clear();
                                editor.apply();
                                editor.commit(); // commit changes

                                Intent intent_Login = new Intent(getApplicationContext(), Login.class);
                                startActivity(intent_Login);
                            }
                        });

                alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //finish();
                    }
                });

                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void showpDialog() {

        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Please wait...");
        pDialog.hide();
        pDialog.setCancelable(true);

        //if (!pDialog.isShowing()) pDialog.show();
    }

    private void hidepDialog() {
        if (pDialog.isShowing()) pDialog.dismiss();
    }

    @Override
    public void onBackPressed() {
        Intent intent_DrawerLayoutActivity = new Intent(getApplicationContext(), UserListining.class);
        startActivity(intent_DrawerLayoutActivity);
    }
}
