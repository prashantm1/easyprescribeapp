package codeplateau.com.striskfinalproject.Activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;


import codeplateau.com.striskfinalproject.R;
import codeplateau.com.striskfinalproject.Services.RestInterface;

public class SplashScreen extends AppCompatActivity {

    SharedPreferences sharedPreferences;
    String email = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen);

        sharedPreferences = getApplicationContext().getSharedPreferences(RestInterface.pref, Context.MODE_PRIVATE);
        email = sharedPreferences.getString("email", "");

        new Handler().postDelayed(new Runnable() {
            /*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */
            @Override
            public void run() {
                // This method will be executed once the timer is over
                // Start your app main activity

                if (email.equals("")) {

                    Intent intent_MainScreen = new Intent(SplashScreen.this, Login.class);
                    startActivity(intent_MainScreen);

                } else {

                    Intent intent_DrawerLayoutActivity = new Intent(SplashScreen.this, UserListining.class);
                    startActivity(intent_DrawerLayoutActivity);
                }
                // close this activity

                finish();
            }
        }, 3000);

    }
}
