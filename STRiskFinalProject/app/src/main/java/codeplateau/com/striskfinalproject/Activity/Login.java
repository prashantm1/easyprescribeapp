package codeplateau.com.striskfinalproject.Activity;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import codeplateau.com.striskfinalproject.R;
import codeplateau.com.striskfinalproject.Services.RestInterface;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.Manifest.permission.ACCESS_NETWORK_STATE;
import static android.Manifest.permission.INTERNET;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static android.Manifest.permission.CAMERA;


public class Login extends AppCompatActivity {
    private static final String TAG = Login.class.getSimpleName();
    private static final int REQUEST_PERMISSION = 12;
    private static String email, id, firstname, lastname, email_verified_at, password, mobileno, user_type, remember_token, created_at, updated_at;
    private static String mEmail, mPassword;
    SharedPreferences pref;
    private EditText txt_username, txt_password;
    private Button btn_login;
    private ProgressDialog pDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);

        initViews();
        setListeners();
    }

    private void initViews() {

        // App Permission
        /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions();
        }*/

        if (shouldAskPermissions()) {
            askPermissions();
        }

        txt_username = findViewById(R.id.txt_username);
        txt_password = findViewById(R.id.txt_password);

        btn_login = findViewById(R.id.btn_login);
    }

    private void setListeners() {

        pref = getApplicationContext().getSharedPreferences(RestInterface.pref, Context.MODE_PRIVATE);

        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (txt_username.getText().toString().equals("")) {
                    Toast.makeText(getApplicationContext(), "Enter Username", Toast.LENGTH_LONG).show();
                } else if (txt_password.getText().toString().equals("")) {
                    Toast.makeText(getApplicationContext(), "Enter Password", Toast.LENGTH_LONG).show();
                } else {

                    mEmail = txt_username.getText().toString().trim();
                    mPassword = txt_password.getText().toString().trim();

                    Login_API(mEmail, mPassword);
                }
            }
        });
    }

    private void Login_API(final String mEmail, final String mPassword) {

        showpDialog();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                RestInterface.url_login, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Response: " + response);

                hidepDialog();

                try {

                    JSONObject jObj = new JSONObject(response);
                    boolean result = jObj.getBoolean("success");

                    if (result) {

                        JSONArray jsonArray = jObj.getJSONArray("data");

                        for (int i = 0; i < jsonArray.length(); i++) {

                            JSONObject jsonObject = jsonArray.getJSONObject(i);

                            id = jsonObject.optString("id");
                            email = jsonObject.optString("email");
                            firstname = jsonObject.optString("firstname");
                            lastname = jsonObject.optString("lastname");
                            email_verified_at = jsonObject.optString("email_verified_at");
                            password = jsonObject.optString("password");
                            mobileno = jsonObject.optString("mobileno");
                            user_type = jsonObject.optString("user_type");
                            remember_token = jsonObject.optString("remember_token");
                            created_at = jsonObject.optString("created_at");
                            updated_at = jsonObject.optString("updated_at");
                        }

                        SharedPreferences.Editor editor = pref.edit();

                        editor.putString("id", id);
                        editor.putString("email", email);
                        editor.putString("firstname", firstname);
                        editor.putString("lastname", lastname);
                        editor.putString("password", password);
                        editor.putString("mobileno", mobileno);

                        editor.apply();
                        editor.commit();

                        //String data = jObj.getString("message");
                        //Toast.makeText(getApplicationContext(), data, Toast.LENGTH_SHORT).show();

                        Intent intent_DrawerLayoutActivity = new Intent(getApplicationContext(), UserListining.class);
                        startActivity(intent_DrawerLayoutActivity);

                    } else {

                        String message = jObj.getString("message");
                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();

                        hidepDialog();
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Error: " + error.getMessage());
                //Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
                hidepDialog();
            }
        }) {

            @Override
            protected java.util.Map<String, String> getParams() {
                // Posting params to register url
                Map<String, String> params = new HashMap<String, String>();
                params.put("email", mEmail);
                params.put("password", mPassword);

                return params;
            }
        };

        strReq.setRetryPolicy(new DefaultRetryPolicy(
                5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // Adding request to request queue
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        strReq.setShouldCache(false);
        requestQueue.add(strReq);
    }

    /*private void requestPermissions() {
        ActivityCompat.requestPermissions(Login.this, new String[]{
                  INTERNET
                , ACCESS_FINE_LOCATION
                , ACCESS_NETWORK_STATE
                , WRITE_EXTERNAL_STORAGE
                , READ_EXTERNAL_STORAGE
                , CAMERA
        }, REQUEST_PERMISSION);
    }*/


    protected boolean shouldAskPermissions() {
        return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1);
    }

    protected void askPermissions() {
        String[] permissions = {
                "android.permission.INTERNET",
                "android.permission.CAMERA",
                "android.permission.READ_EXTERNAL_STORAGE",
                "android.permission.WRITE_EXTERNAL_STORAGE",
                "android.permission.ACCESS_FINE_LOCATION",
                "android.permission.ACCESS_NETWORK_STATE"
        };
        int requestCode = 200;
        requestPermissions(permissions, requestCode);
    }

    private void showpDialog() {
        // Progress Dialog
        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Please wait...");
        pDialog.setCancelable(false);

        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hidepDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    @Override
    public void onBackPressed() {
        Intent intent_Login = new Intent(getApplicationContext(), Login.class);
        startActivity(intent_Login);
    }
}
